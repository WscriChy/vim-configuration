if exists("b:did_ftplugin") | finish | endif

if version > 600
  runtime! ftplugin/c.vim
endif

let b:did_ftplugin = 1
