hi! clear

set background=dark
if version > 580
    " no guarantees for version 5.8 and below, but this makes it stop
    " complaining
    hi! clear
    if exists("syntax_on")
    syntax reset
    endif
endif
let g:colors_name="darkspectrum"

hi! Normal           guifg=#EFEFEF guibg=#282828

" highlight groups
hi! nCursor          guifg=NONE    guibg=#000000 gui=underline,bold
hi! iCursor          guifg=NONE    guibg=#CC0000
hi! CursorLine                     guibg=#000000
hi! CursorColumn                   guibg=#000000

hi! DiffAdd          guifg=#FFCC7F guibg=#A67429 gui=none
hi! DiffChange       guifg=#7FBDFF guibg=#425C78 gui=none
hi! DiffText         guifg=#8AE234 guibg=#4E9A06 gui=none
hi! DiffDelete       guifg=#000000 guibg=#000000 gui=none

"hi! Number           guifg=#fce94f
"hi! Number           guifg=#FCAF3E
"hi! String           guifg=#CC5555
hi! Constant         guifg=#CC5555               gui=none
"       *Constant        any constant
"        String          a string constant: "this is a string"
"        Character       a character constant: 'c', '\n'
"        Number          a number constant: 234, 0xff
"        Boolean         a boolean constant: TRUE, false
"        Float           a floating point constant: 2.3e10

hi! Folded           guifg=#FFFFFF guibg=#000000 gui=bold
hi! vimFold          guifg=#FFFFFF guibg=#000000 gui=bold
hi! FoldColumn       guifg=#FFFFFF guibg=#000000 gui=bold

hi! LineNr           guifg=#535353 guibg=#202020
hi! NonText          guifg=#535353 guibg=#202020
hi! Folded           guifg=#535353 guibg=#202020 gui=bold
hi! FoldeColumn      guifg=#535353 guibg=#202020 gui=bold

hi! VertSplit        guifg=#000000 guibg=#000000 gui=none

hi! StatusLine       guifg=#EE8888 guibg=#882222 gui=none
hi! StatusLineNC     guifg=#CCCCCC guibg=#000000 gui=none

hi! ModeMsg          guifg=#FCE94F
hi! MoreMsg          guifg=#FCE94F
hi! Visual           guifg=#FFFFFF guibg=#3465A4 gui=none
hi! VisualNOS        guifg=#FFFFFF guibg=#204A87 gui=none
hi! IncSearch        guifg=#EF5939 guibg=#FFFFFF
hi! Search           guifg=NONE    guibg=#282828 gui=underline,bold
hi! SpecialKey       guifg=#8AE234

hi! Title            guifg=#EF5939
hi! WarningMsg       guifg=#EFEFEF guibg=#282828
hi! ErrorMsg         guifg=#4B4B4B guibg=#282828

hi! MatchParen       guifg=#FFFFFF guibg=#ad7fa8

hi! Comment          guifg=#8A8A8A
"       *Comment         any comment

hi! Identifier       guifg=#729FCF
hi! Function         guifg=#729FCF gui=bold
"       *Identifier      any variable name
"        Function        function name (also: methods for classes)
hi! Statement        guifg=#FF33CC               gui=bold
hi! Operator         guifg=#CC33FF               gui=bold
"       *Statement       any statement
"        Conditional     if, then, else, endif, switch, etc.
"        Repeat          for, do, while, etc.
"        Label           case, default, etc.
"        Operator        "sizeof", "+", "*", etc.
"        Keyword         any other keyword
"        Exception       try, catch, throw
hi! Type             guifg=#8AE234               gui=italic
"       *Type            int, long, char, etc.
"        StorageClass    static, register, volatile, etc.
"        Structure       struct, union, enum, etc.
"        Typedef         A typedef
hi! Special          guifg=#E9B96E
"       *Special         any special symbol
"        SpecialChar     special character in a constant
"        Tag             you can use CTRL-] on this
"        Delimiter       character that needs attention
"        SpecialComment  special things inside a comment
"        Debug           debugging statements

hi! link PreProc Special
"       *PreProc         generic Preprocessor
"        Include         preprocessor #include
"        Define          preprocessor #define
"        Macro           same as Define
"        PreCondit       preprocessor #if, #else, #endif, etc.

hi! Underlined       guifg=#AD7FA8               gui=underline
hi! Directory        guifg=#729FCF
hi! Ignore           guifg=#555753
hi! Todo             guifg=#FFFFFF guibg=#EF5939 gui=bold

" Modified
" Added
hi! Namespace        guifg=#00BFFF
hi! Class            guifg=#00BFFF gui=bold

hi! WildMenu         guifg=#FFFFFF guibg=#3465a4 gui=none

hi! Pmenu            guibg=#000000 guifg=#C0C0C0
hi! PmenuSel         guibg=#3465A4 guifg=#FFFFFF
hi! PmenuSbar        guibg=#444444 guifg=#444444
hi! PmenuThumb       guibg=#888888 guifg=#888888

hi! cppSTLType       guifg=#729FCF gui=bold

hi! spellBad         guisp=#FCAF3E
hi! spellCap         guisp=#73D216
hi! spellRare        guisp=#AD7FA8
hi! spellLocal       guisp=#729FCF

" tabs (non gui)
hi! TabLine       guifg=#A3A3A3 guibg=#202020 gui=none
hi! TabLineFill   guifg=#535353 guibg=#202020 gui=none
hi! TabLineSel    guifg=#FFFFFF gui=bold

" Custom
hi! ActiveItem guifg=#FF6633 gui=bold

hi! def link MyOperator0 Operator
