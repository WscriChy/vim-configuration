#!/usr/bin/env python
# encoding: utf-8

import os
import vim
import platform

import vim
from Configurator import *

_platform = platform.system()

# The updatetime option is the number of milliseconds that have to pass before
# Vim's CursorHold (see :h CursorHold) event fires.
setOption("updatetime", 800)

setOption("previewheight", 30)

# Splits {{{
vim.command("set splitbelow")
vim.command("set splitright")
# Splits }}}

# Cmd {{{
# Cmd }}}

vim.command("set wildignore+="
            "*.pyc,*.pyo,*.dll,*.so,*.lib,*.a,*.obj,"
            "*.o,*.d,"
            "*.pdf,*.djvu,*.djv,"
            "*.mp3,*.ogg,*.flv,"
            "*.avi,*.mp4,*.mkv,"
            "*.ttf,"
            "*.7z,*.zip,*.tar,*.bz,*.bz2,*.gz,*.xz,*.rar"
            "*.db,"
            "*.out,*.log,*.swp,*.build"
            "*/.git/*,*/.hg/*,"
            "*//.git//*,*//.hg//*,"
            "*.png,*.jpg,*.jpeg,*.ico,*.tiff,*.gif,*.svg"
            )

if _platform == "Windows":
  setOption("shell", os.environ['COMSPEC'])

if _platform == "Windows":
  setOption("guifont", "Powerline\\ Consolas:h13")
elif vim.eval("has('gui_gtk2')") == "1":
  # setOption("guifont", "Inconsolata\\ 11")
  # setOption("guifont", "Inconsolata-g\\ 10")
  # setOption("guifont", "Menlo\ Regular\ 11")
  # setOption("guifont", "Monaco\ 10")
  setOption("guifont", "Inconsolata-dz\\ for\\ Powerline\\ 10")
  # setOption("guifont", "IBM\ 3270\ 12")
else:
  setOption("guifont", "Powerline\\ Consolas\\ 13")


# Helpful commands
# 1. let g:incsearch#auto_nohlsearch=0 --- disables autoremove of highlighting
# 2. IndentGuidesToggle --- disables indent guides from showing
# 3. set list           --- shows whitespace characters
