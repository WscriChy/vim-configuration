function! PathToString(path)
	return substitute(a:path, '[\\]\+', '\\\\', "g")
endfunction

let home            = expand("<sfile>:h")
let bin             = g:home."/bin"
let runtime         = g:home."/bin/Runtime"
let runtimeUpgrages = g:home."/RuntimeUpgrades"
let stuff           = g:home."/Stuff"
let trash           = g:home."/Trash"
let plugins         = g:home."/Plugins"

py import sys, os, vim
py sys.path.append(os.path.join(vim.eval("expand(\"<sfile>:h\")")))
py import Config
