#!/usr/bin/env python
# encoding: utf-8

import sys, os, copy

sys.path.append(os.path.join(os.path.dirname(__file__), 'python'))

import System

from Object import Object

from YCMBuilder import YCMBuilder
from Mwm import Mwm
from Proc import Proc

rootPath = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

import platform as _plt

_platform = _plt.system()

if System.platform.Windows():
  libclang = "clang.dll"
  libclang = System.findInPath(libclang)

mainConfig = Object(ROOT                 = rootPath,
                    BUILD                = os.path.join(rootPath, "build"),
                    J                    = 8,

                    CXX_OPTIMIZATION_FLAGS = "-O3 -march=native -fomit-frame-pointer -funroll-loops",
                    LINKER_OPTIMIZATION_FLAGS = "-flto -fuse-linker-plugin",
                    STATIC_RUNTIME_FLAGS = "-static-libstdc++ -static-libgcc",
                    YCM = os.path.join(rootPath, "Plugins", "Ycm", "Plugin"),
                    LLVM_ROOT            = os.path.dirname(os.path.dirname(libclang))
                                           if _platform == "Windows" else
                                           "/usr/lib/llvm-3.6"
                                           if _platform == "Linux" else "",
                    EXTERNAL_LIBCLANG_PATH = libclang
                                           if _platform == "Windows" else
                                           ""
                                           if _platform == "Linux" else "",
                    PYTHON_INCLUDE       = "D:/python/2.7/include"
                                           if _platform == "Windows" else
                                           "/usr/include/python2.7"
                                           if _platform == "Linux" else "",
                    PYTHON_LIBRARY       = "D:/python/2.7/python27.dll"
                                           if _platform == "Windows" else
                                           "/usr/lib/x86_64-linux-gnu/libpython2.7.so"
                                           if _platform == "Linux" else "",

                    MWM = os.path.join(rootPath, "Plugins/Mwm/Plugin"),

                    PROC = os.path.join(rootPath, "Plugins/Proc/Plugin")
                    )

ycmBuilder = YCMBuilder(config = copy.deepcopy(mainConfig))
ycmBuilder.fullCycle()

vimMwmBuilder = Mwm(config = copy.deepcopy(mainConfig))
vimMwmBuilder.fullCycle()

vimProcBuilder = Proc(config = copy.deepcopy(mainConfig))
vimProcBuilder.fullCycle()

