#!/usr/bin/env python -OO
# encoding: utf-8


class Platform(object):
  def __init__(self):
    import platform
    _platform = platform.system()
    self._Windows = False
    self._Linux = False
    if _platform == "Windows":
      self._Windows = True
    elif _platform == "Linux":
      self._Linux = True
  def Windows(self):
    return self._Windows
  def Linux(self):
    return self._Linux

platform = Platform()

def findInPath(objectName):
  import os
  for path in os.environ["PATH"].split(os.pathsep):
    path = path.strip('"')
    objectPath = os.path.join(path, objectName)
    if os.path.exists(objectPath):
      return objectPath
  return None
