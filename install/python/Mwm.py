#!/usr/bin/env python
# encoding: utf-8

from BasicBuilder import BasicBuilder

import os
import re

import platform

_platform = platform.system()

def _configure(self):
    return ("cmake -G \"Ninja\" "
        "-DCMAKE_INSTALL_PREFIX:PATH=%s "
        "-DCMAKE_C_FLAGS=\"%s\" "
        "-DCMAKE_SHARED_LINKER_FLAGS=\"%s -static\" "
        "%s"
        ) % (os.path.join(self.config.MWM, "plugin"),
             self.config.CXX_OPTIMIZATION_FLAGS,
             self.config.STATIC_RUNTIME_FLAGS,
             self.config.MWM)

class Mwm(BasicBuilder):

  def __init__(self, *args, **argd):
    if _platform != "Windows": return
    self.builderName = "Mwm"
    super(Mwm, self).__init__(*args, **argd)
    self.init()
    pass

  def checkInstallation(self, path):
    if _platform != "Windows": return
    if not os.path.isdir(self.BIN_PATH):
      self.installed = False
      return
    files = [f for f in os.listdir(self.BIN_PATH) if os.path.isfile(os.path.join(self.BIN_PATH, f))]
    isFound = False
    for f in files:
      if not re.match(".*vim-mwm\.(dll|so)$", f, re.I) is None:
        isFound = True
    self.installed = isFound

  def configure(self):
    if _platform != "Windows": return
    self.configurator.commandGenerator = _configure
    self._configure()
    pass

  def build(self):
    if _platform != "Windows": return
    self.configurator.commandGenerator = lambda self: "ninja"
    self._build()
    pass

  def install(self):
    if _platform != "Windows": return
    self.configurator.commandGenerator = lambda self: "ninja install"
    self._install()
    pass
