#!/usr/bin/env python
# encoding: utf-8

from Object import Object

import os, shutil

import platform

_platform = platform.system()

if _platform == "Linux":
  _systemPathType = 1
elif _platform == "Windows":
  _systemPathType = 0
else:
  _systemPathType = -1

PathType = type('Enum', (), {"WindowsPath" : 0, "UnixPath" : 1, "System" : _systemPathType})

class Builder(Object):

  def __init__(self, *args, **argd):
    super(Builder, self).__init__(*args, **argd)
    self.RETURN_CODE = 0
    if not hasattr(self, "pathType"):
      self.pathType = PathType.UnixPath
    pass

  # Clean a directory {{{
  # -----------------------------------------------------------------------------
  def cleanFiles(self, *paths):
    for path in paths:
      if not os.path.exists(str(path)):
        print(path)
        return
      if os.path.isfile(path):
        os.remove(path)
      for root, dirs, files in os.walk(path):
        for file in files:
          os.remove(os.path.join(root, file))
        for directory in dirs:
          shutil.rmtree(os.path.join(root, directory))
    pass
  # -----------------------------------------------------------------------------
  # Clean a directory }}}

  # Check files in a directory {{{
  # -----------------------------------------------------------------------------
  def checkInstallation(self, path):
    self.installed = self.checkFiles(path)

  def checkFiles(self, path):
    if os.path.isfile(path):
      return True
    for root, dirs, files in os.walk(path):
      for file in files:
        return True
      for directory in dirs:
        return True
    return False
  # -----------------------------------------------------------------------------
  # Check files in a directory }}}

  def preparePath(self, *args, **argd):
    pathType = self.pathType
    if 'pathType' in argd:
      pathType = argd['pathType']
    path = os.path.join(*args)
    path = self.normalizePath(path, pathType)
    return path

  def buildPath(self, *args):
    path = self.preparePath(*args)
    if not os.path.exists(path):
      os.makedirs(path)
    return path

  def checkPath(self, *args):
    path = self.preparePath(*args)
    if not os.path.exists(path):
      raise Exception("Path does not exist:\n%s" % path)
    return path

  def normalizePath(self, path, pathType = PathType.System):
    if pathType is None:
      pathType = self.pathType
    if pathType == PathType.WindowsPath:
      return os.path.normpath(path)
    elif pathType == PathType.UnixPath:
      return os.path.normpath(path).replace("\\", "/")
    else:
      raise Exception("Unknown path type")

