#!/usr/bin/env python
# encoding: utf-8

from Object import Object
from Output import Output

import sys
import subprocess

class Process(Object):

  def __init__(self, *args, **argd):
    super(Process, self).__init__(*args, **argd)

  def run(self, command):
    if hasattr(self, 'env'):
      process = subprocess.Popen(command,
                                 cwd = self.cwd,
                                 env = self.env,
                                 stdout= Output(sys.__stdout__),
                                 stderr= Output(sys.__stderr__),
                                 stdin = sys.stdin,
                                 shell = True)
    else:
      process = subprocess.Popen(command,
                                 cwd = self.cwd,
                                 stdout= Output(sys.__stdout__),
                                 stderr= Output(sys.__stderr__),
                                 stdin = sys.stdin,
                                 shell = True)
    retcode = process.wait() #returns None while subprocess is running
    if(retcode is not None):
      return retcode
