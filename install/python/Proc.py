#!/usr/bin/env python
# encoding: utf-8

from BasicBuilder import BasicBuilder

import os
import re

import platform

_platform = platform.system()

class Proc(BasicBuilder):

  def __init__(self, *args, **argd):
    self.builderName = "Proc"
    super(Proc, self).__init__(*args, **argd)
    self.init()
    pass

  def checkInstallation(self, path):
    lookupPath = self.preparePath(self.INSTALL_PATH, "autoload")
    if not os.path.isdir(lookupPath):
      self.installed = False
      return
    files = [f for f in os.listdir(lookupPath) if os.path.isfile(os.path.join(lookupPath, f))]
    isFound = False
    for f in files:
      if not re.match(".*vimproc\w+\.(dll|so)$", f, re.I) is None:
        isFound = True
    self.installed = isFound

  def configure(self):
    self._configure()
    pass

  def build(self):
    makefile = ""
    if _platform == "Windows":
      makefile = "make_mingw64.mak"
    elif _platform == "Linux":
      makefile = "make_unix.mak"
    else:
      return
    self.configurator.cwd = lambda self: self.preparePath(self.config.PROC)
    self.configurator.commandGenerator = lambda self: ("make -f %s" %  makefile)
    self._build()
    pass

  def install(self):
    self._install()
    pass
