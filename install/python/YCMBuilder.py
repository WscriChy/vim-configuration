#!/usr/bin/env python -OO
# encoding: utf-8

from Builder import Builder
from Output  import log

from Process import Process

import shutil
import platform

_platform = platform.system()

class YCMBuilder(Builder):

  def __init__(self, *args, **argd):
    super(YCMBuilder, self).__init__(*args, **argd)
    self.installed = True
    self.libclang = self.preparePath(self.config.YCM,
        "third_party/ycmd",
        "libclang.dll"
        if _platform == "Windows" else
        "libclang.so"
        if _platform == "Linux" else "")
    self.ycmCore = self.preparePath(self.config.YCM,
        "third_party/ycmd",
        "ycm_core.pyd"
        if _platform == "Windows" else
        "ycm_core.so"
        if _platform == "Linux" else "")
    self.ycmClientSupport = self.preparePath(self.config.YCM,
        "third_party/ycmd",
        "ycm_client_support.pyd"
        if _platform == "Windows" else
        "ycm_client_support.so"
        if _platform == "Linux" else "")
    if not (self.checkFiles(self.libclang) and
        self.checkFiles(self.ycmCore) and
        self.checkFiles(self.ycmClientSupport)):
      self.cleanFiles(self.libclang, self.ycmCore, self.ycmClientSupport)
      self.installed = False
    pass

  def configure(self):
    if self.installed:
      return
    self.installPath = self.checkPath(self.config.YCM,
                                 "third_party/ycmd")
    self.sourcePath = self.checkPath(self.installPath, "cpp")
    self.buildPath = self.buildPath(self.config.BUILD, "Ycm")
    self.cleanFiles(self.buildPath)
    command = ""
    if _platform == "Linux":
      command = ("cmake -G \"Ninja\" "
          "-DUSE_CLANG_COMPLETER=ON "
          # "-DUSE_SYSTEM_LIBCLANG=ON "
          "-DCMAKE_CXX_FLAGS=\"%s\" "
          "-DCMAKE_C_FLAGS=\"%s\" "
          # "-DPATH_TO_LLVM_ROOT=\"%s\" "
          "%s"
          ) % (
               self.config.CXX_OPTIMIZATION_FLAGS,
               self.config.CXX_OPTIMIZATION_FLAGS,
               # self.config.LLVM_ROOT,
               self.sourcePath
               )
    elif _platform == "Windows":
      command = ("cmake -G \"Ninja\" "
          "-DUSE_CLANG_COMPLETER=ON "
          "-DUSE_SYSTEM_LIBCLANG=OFF "
          "-DCMAKE_CXX_FLAGS=\"%s\" "
          "-DCMAKE_C_FLAGS=\"%s\" "
          "-DCMAKE_SHARED_LINKER_FLAGS=\"%s -static\" "
          "-DPATH_TO_LLVM_ROOT=\"%s\" "
          "-DEXTERNAL_LIBCLANG_PATH=\"%s\" "
          #"-DPYTHON_INCLUDE_DIR=%s "
          #"-DPYTHON_LIBRARY=%s "
          "%s"
          ) % (
               self.config.CXX_OPTIMIZATION_FLAGS,
               self.config.CXX_OPTIMIZATION_FLAGS,
               self.config.STATIC_RUNTIME_FLAGS,
               self.config.LLVM_ROOT,
               self.config.EXTERNAL_LIBCLANG_PATH,
               #self.config.PYTHON_INCLUDE,
               #self.config.PYTHON_LIBRARY,
               self.sourcePath
               )
    else:
      command = ""

    log("Configuring %s with the command:\n%s" % ("YCM", command))

    process = Process(cwd = self.buildPath)
    returnCode = process.run(command)
    self.returnCode = returnCode
    pass

  def build(self):
    if self.installed:
      return
    if self.returnCode != 0:
      return
    command = "ninja"
    process = Process(cwd = self.buildPath)
    returnCode = process.run(command)
    self.returnCode = returnCode
    pass

  def install(self):
    if _platform != "Windows":
      return
    if (self.checkFiles(self.ycmCore) and
        self.checkFiles(self.ycmClientSupport)):
      shutil.copy(self.config.EXTERNAL_LIBCLANG_PATH, self.libclang)

  def fullCycle(self):
    self.configure()
    self.build()
    self.install()
    pass
