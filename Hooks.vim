" augroup my-OpenVimFilerOnEmptyStartUp
" autocmd VimEnter * if !argc() | VimFiler | endif
" augroup END

" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside an event handler
" (happens when dropping a file on gvim).
" Also don't do it when the mark is in the first line, that is the default
" position when opening a file.
autocmd BufReadPost *
  \ if line("'\"") > 1 && line("'\"") <= line("$") |
  \   exe "normal! g`\"" |
  \ endif
" augroup END

au BufRead,BufNewFile *.md  set filetype=markdown
au BufRead,BufNewFile *.qml set filetype=qml

" C++ style comment folding
" au BufNewFile,BufRead *.cpp,*.c,*.h,*.java set foldmethod=syntax
" au BufNewFile,BufRead *.cpp,*.c,*.h,*.hpp,*.java set foldtext=CxxFoldText()
" function CxxFoldText()
"   let line = getline(v:foldstart)
"   let sub = substitute(line, '^[\t ]*', '', '')
"   let nlines = v:foldend - v:foldstart + 1
"   if strlen(nlines) == 1
"     let nlines = " " . nlines
"   elseif strlen(nlines) == 2
"     let nlines = " " . nlines
"   endif
"   return "+-" . v:folddashes . nlines . ": " . sub
" endfunction
