let mapleader = " "

autocmd CmdwinEnter * noremap <buffer> <silent> q     :quit<CR>
autocmd CmdwinEnter * noremap <buffer> <silent> <A-`> :quit<CR>
autocmd CmdwinEnter * noremap <buffer> <silent> :     :quit<CR>

noremap  <Backspace> %

" LocaList & Quickfix Navigation
nnoremap <silent> [s     :lne<Cr>
nnoremap <silent> [d     :lpr<Cr>
nnoremap <silent> [e     :cp<Cr>
nnoremap <silent> [w     :cn<Cr>
nnoremap <silent> [c     :lcl<Cr>

" Others
function OnOffSpell()
	if(&spell)
		setlocal nospell
	else
		setlocal spell
	endif
endfunction
