#!/usr/bin/env python
# encoding: utf-8

# Then you can call it like this:
#
# e = Employee({"name": "abc", "age": 32})
# or like this:
#
# e = Employee(name="abc", age=32)
# or even like this:
#
# employee_template = {"role": "minion"}
# e = Employee(employee_template, name="abc", age=32)

class Object(object):

  def __init__(self, *args, **argd):
    for dictionary in args:
      for key in dictionary:
        setattr(self, key, dictionary[key])

    for key in argd:
        setattr(self, key, argd[key])

    pass

  def __getitem__(self, name):
    return getattr(self, name)

  def __setitem__(self, name, value):
    setattr(self, name, value)
  pass
