#!/usr/bin/env python
# encoding: utf-8

from Object import Object
from utils import normalize

import os
from os.path import isabs, join
import inspect
from glob import glob

import vim

class Commander(Object):

  def __init__(self, *args, **argd):
    self._hooks = []
    if "currentDir" in argd:
      self.currentDir(argd["currentDir"])
      argd.pop("currentDir")
    else:
      self.currentDir(os.path.dirname(os.path.realpath(
          os.path.abspath(inspect.getfile(inspect.currentframe())))))
    super(Commander, self).__init__(*args, **argd)
    pass

  def currentDir(self, *values):
    if len(values) >= 1:
      self._currentDir = normalize(*values)
    return self._currentDir

  def sourceFile(self, expr):
    if not isabs(expr):
      expr = join(self._currentDir, expr)
    list = glob(expr)
    for x in list:
      vim.command("source " + normalize(x))

  def set(self, name, value = ""):
    if value == "":
      vim.command("let {0}=1".format(name))
    else:
      vim.command("let {0}={1}".format(name, value))

  def setOption(self, name, value = ""):
    if value == "":
      vim.command("set {0}".format(name))
    else:
      vim.command("set {0}={1}".format(name, value))

  def setGlobal(self, name, value = ""):
    if value == "":
      vim.command("let g:{0}=1".format(name))
    else:
      vim.command("let g:{0}={1}".format(name, value))

  def callFunction(self, function):
    vim.command("call %s" % function)

  def setHook(self, event, function):
    number = len(self._hooks)
    self._hooks.append(function)
    command = ("autocmd {0} py import Configurator;"
              "Configurator.commander._hooks[{1}]()").format(event, number)
    vim.command(command)

