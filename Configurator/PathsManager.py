#!/usr/bin/env python
# encoding: utf-8

from Object import Object
from utils import normalize

import os
from os.path import isabs, join, isdir
import inspect
from glob import glob
from collections import OrderedDict

import vim

class PathsManager(Object):

  def __init__(self, *args, **argd):
    self._runtime = ""
    self.paths = OrderedDict()
    if "currentDir" in argd:
      self.currentDir(argd["currentDir"])
      argd.pop("currentDir")
    else:
      self.currentDir(os.path.dirname(os.path.realpath(
          os.path.abspath(inspect.getfile(inspect.currentframe())))))
    super(Object, self).__init__(*args, **argd)
    self.runtime(vim.eval("$VIMRUNTIME"));

  def currentDir(self, *values):
    if len(values) >= 1:
      self._currentDir = normalize(*values)
    return self._currentDir

  def runtime(self, path):
    if self._runtime: self.exclude(self._runtime)
    self._runtime = normalize(path)
    vim.command("let $VIMRUNTIME=\"%s\"" % self._runtime)

  def exclude(self, expr):
    if not isabs(expr):
      expr = join(self._currentDir, expr)
    list = glob(expr)
    for x in list:
      if isdir(x):
        self.paths.pop(normalize(x))

  def add(self, expr):
    if not isabs(expr):
      expr = join(self._currentDir, expr)
    list = glob(expr)
    result = OrderedDict()
    for x in list:
      if isdir(x):
        item = normalize(x)
        self.paths[item] = ""
        result[item] = ""
    return result

  def loadPaths(self):
    self.add(self._runtime)
    afters = []
    pathsString = ""
    aftersString = ""
    for x in self.paths:
      after = normalize(x, "after")
      if os.path.isdir(after):
        afters.append(after)
    if len(self.paths) > 0:
      pathsString = ', '.join(self.paths)
      if len(afters) > 0:
        pathsString += ', '
    aftersString = ', '.join(afters)
    command = "let &runtimepath='%s%s'" % (pathsString, aftersString)
    vim.command(command)
