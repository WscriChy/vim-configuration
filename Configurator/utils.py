#!/usr/bin/env python
# encoding: utf-8

import os

def normalize(*args, **argd):
  path = os.path.join(*args)
  if not os.path.isabs(path):
    if "currentDir" in argd:
      path = os.path.join(argd["currentDir"], path)
  path = os.path.normpath(path).replace("\\", "/")
  return path
