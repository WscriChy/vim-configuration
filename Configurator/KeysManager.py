#!/usr/bin/env python
# encoding: utf-8

from Object import Object

import re
import vim

class KeyAttribute(Object):

  def __init__(self, *args, **argd):
    self._type = -1
    super(KeyAttribute, self).__init__(*args, **argd)

  def equals(self, other):
     return self._type == other._type

  def __eq__(self, other):
    return self.equals(other)

  def updateCommand(self, command):
    if self._type == 0:
      command.Normal = True
    elif self._type == 1:
      command.Insert = True
    elif self._type == 2:
      command.CommandLine = True
    elif self._type == 3:
      command.Visual = True
    elif self._type == 4:
      command.Select = True
    elif self._type == 5:
      command.OperatorPending = True
    elif self._type == 6:
      command.Silent = True
    elif self._type == 7:
      command.Buffer = True
    elif self._type == 8:
      command.NoRemap = True

setattr(KeyAttribute, 'Normal',          KeyAttribute(_type = 0))
setattr(KeyAttribute, 'Insert',          KeyAttribute(_type = 1))
setattr(KeyAttribute, 'CommandLine',     KeyAttribute(_type = 2))
setattr(KeyAttribute, 'Visual',          KeyAttribute(_type = 3))
setattr(KeyAttribute, 'Select',          KeyAttribute(_type = 4))
setattr(KeyAttribute, 'OperatorPending', KeyAttribute(_type = 5))
setattr(KeyAttribute, 'Silent',          KeyAttribute(_type = 6))
setattr(KeyAttribute, 'Buffer',          KeyAttribute(_type = 7))
setattr(KeyAttribute, 'NoRemap',         KeyAttribute(_type = 8))

class Command(object):

  def __init__(self, *args, **argd):
    self.Normal = False
    self.Insert = False
    self.CommandLine = False
    self.Visual = False
    self.Select = False
    self.OperatorPending = False
    self.Silent = False
    self.Buffer = False
    self.NoRemap = False
    self.source = argd['source'] if 'source' in argd else ''
    self.destination = argd['destination'] if 'destination' in argd else ''
    for value in args:
     value.updateCommand(self)

  def _execute(self, command):
    cm = command + " " + self.source + " " + self.destination
    vim.command(cm)

  def execute(self):
    commandString = ""
    if self.NoRemap:
      commandString = "noremap"
    else:
      commandString = "map"
    if self.Silent:
      commandString += " <silent>"
    if self.Buffer:
      commandString += " <buffer>"
    if self.Normal:
      tempCommandString = "n" + commandString
      self._execute(tempCommandString)
    if self.Insert:
      tempCommandString = "i" + commandString
      self._execute(tempCommandString)
    if self.CommandLine:
      tempCommandString = "c" + commandString
      self._execute(tempCommandString)
    if self.Visual:
      tempCommandString = "x" + commandString
      self._execute(tempCommandString)
    if self.Select:
      tempCommandString = "s" + commandString
      self._execute(tempCommandString)
    if self.OperatorPending:
      tempCommandString = "o" + commandString
      self._execute(tempCommandString)

class KeysManager(Object):

  def __init__(self, *args, **argd):
    super(KeysManager, self).__init__(*args, **argd)

  def map(self, source, destination, *args):
    command = Command(*args, source = source, destination = destination)
    command.execute()

  def _getMap(self, source, mode):
     f = vim.Function('maparg')
     result = f(source, mode, 0, 1000)
     command = Command()
     if not result.has_key('rhs'):
       command.source = source
       command.destination = source
       return command
     if result['silent'] == 1: command.Silent = True
     if result['noremap'] == 1: command.NoRemap = True
     if result['buffer'] == 1: command.Buffer = True
     if result['buffer'] == 1: command.Buffer = True
     if result['expr'] == 1: command.Expression = True
     command.source = result['lhs']
     command.destination = result['rhs']
     return command

  def getNormalMap(self, source):
    command = self._getMap(source, 'n')
    command.Normal = True
    return command

  def getInsertMap(self, source):
    command = self._getMap(source, 'i')
    command.Insert = True
    return command

  def getCommandLineMap(self, source):
    command = self._getMap(source, 'c')
    command.CommandLine = True
    return command

  def getVisualMap(self, source):
    command = self._getMap(source, 'x')
    command.Visual = True
    return command

  def getSelectMap(self, source):
    command = self._getMap(source, 's')
    command.Select = True
    return command

  def getOperatorPendingMap(self, source):
    command = self._getMap(source, 'o')
    command.OperatorPending = True
    return command


