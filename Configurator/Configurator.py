#!/usr/bin/env python
# encoding: utf-8

from KeysManager import KeysManager
from KeysManager import KeyAttribute
from PathsManager import PathsManager
from Commander import Commander

import os
import imp
import vim
import inspect
import platform


_platform = platform.system()

_currentDir = os.path.dirname(os.path.realpath(
  os.path.abspath(inspect.getfile(inspect.currentframe()))))

_rootPath = os.path.realpath(os.path.join(_currentDir, ".."))

def getRootPath():
  return _rootPath

def setRootPath(path):
  _rootPath = path

keysManager = KeysManager()
pathsManager = PathsManager()
commander = Commander()

def sourceVimSettings(path, name):
  isAdded = False
  settings = os.path.join(path, "%s.vim" % name)
  if os.path.isfile(settings):
    source(settings)
    isAdded = True
  return isAdded

def sourcePythonScript(moduleName, scriptPath):
  imp.load_source(moduleName, scriptPath)

def sourcePythonSettings(path, moduleName, name):
  isAdded = False
  settingsPath = os.path.join(path, "{0}.py".format(name))
  if os.path.isfile(settingsPath):
    module = imp.load_source(moduleName, settingsPath)
    isAdded = True
  return isAdded

Normal = KeyAttribute.Normal
Insert = KeyAttribute.Insert
CommandLine = KeyAttribute.CommandLine
Visual = KeyAttribute.Visual
Select = KeyAttribute.Select
OperatorPending = KeyAttribute.OperatorPending
Silent = KeyAttribute.Silent
NoRemap = KeyAttribute.NoRemap
Buffer = KeyAttribute.Buffer

def source(filePath):
  commander.sourceFile(filePath)

def set(name, value = ""):
  commander.set(name, value)

def setOption(name, value = ""):
  commander.setOption(name, value)

def setGlobal(name, value = ""):
  commander.setGlobal(name,value)

def call(function):
  commander.callFunction(function)

def hook(event, function):
  commander.setHook(event, function)

def map(source, destination, *args):
  keysManager.map(source, destination, *args)
