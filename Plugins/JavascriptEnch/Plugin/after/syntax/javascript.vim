" syn match MyOperator0 "<<\|>>\|&&\|||\|++\|--\|->"
" syn match MyOperator0 "[.!~*&%<>^|=,+\-:;]"
" syn match MyOperator0 "/[^/*=]"me=e-1
" " Slash at the end of a line
" syn match MyOperator0 "/$"
" syn match MyOperator0 "[][]"

hi! def link javascriptObjectLabelColon MyOperator
hi! def link javaScriptParens MyOperator
hi! def link javascriptOpSymbol MyOperator
hi! def link javascriptOpSymbols MyOperator
hi! def link javascriptDotNotation MyOperator
hi! def link javascriptDotStyleNotation MyOperator
