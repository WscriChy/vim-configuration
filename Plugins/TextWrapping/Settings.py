#!/usr/bin/env python
# encoding: utf-8

import vim
from Configurator import *

# Set soft wrap

# This option changes how text is displayed.  It doesn't change the text in the
# buffer, see 'textwidth' for that. When on, lines longer than the width of the
# window will wrap and displaying continues on the next line.
setOption("wrap")
# If on Vim will wrap long lines at a character in 'breakat' rather than at the
# last character that fits on the screen.  Unlike 'wrapmargin' and 'textwidth',
# this does not insert <EOL>s in the file, it only affects the way the file is
# displayed, not its contents.
setOption("linebreak")
# Insert two spaces after a '.', '?' and '!' with a join command. When
# 'cpoptions' includes the 'j' flag, only do this after a '.'. Otherwise only
# one space is inserted.
setOption("nojoinspaces")

# let s:showbreak='»'
# set showbreak=↪
set("&showbreak", "'↪ '")
# Most probably need the next line for Windows
# set("&showbreak", "'>> '")

def setBufferOptions():
  buffer = vim.current.buffer
  bufferType = buffer.options['buftype']
  bufferFileType = buffer.options['filetype']
  conditions = [
    buffer.options['buflisted'] == True,
    bufferType != 'nofile',
    bufferType != 'quickfix',
    bufferType != 'help',
    bufferType != '' or buffer.name != '' or bufferFileType != '',
    ]
  for condition in conditions:
    if not condition:
      return
  vim.command("setlocal textwidth=90")
  vim.command("setlocal wrapmargin=0")
  # Disable autoformatting because conflicts with completion --- vim bug
  vim.command("setlocal formatoptions=roqwnj")

hook("BufRead,BufNewFile *", setBufferOptions)
