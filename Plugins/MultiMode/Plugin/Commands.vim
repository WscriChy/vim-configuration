function! s:ProtectString(a)
	return substitute(a:a, '"', '\\"', "g")
endfunction

command -nargs=+ R :py import GlobalReplace;GlobalReplace.globalReplace(<q-args>)
