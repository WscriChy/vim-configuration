#!/usr/bin/env python
# encoding: utf-8

import os
import sys
import re
import socket
import struct
import cPickle as pickle
import subprocess
import vim
from vim import buffers
import logging
import tempfile
import chardet
import inspect
import platform

_platform = platform.system()

class Logger(object):
  def __init__(self):
    self.fd, self._logFileName = tempfile.mkstemp(prefix = "VimGlobalFindLog")
    logging.basicConfig(
        filename = self._logFileName,
        level = logging.INFO,
        format = 'vim:%(levelname)s:%(message)s')

  def __del__(self):
    logging.shutdown()
    os.close(self.fd)
    os.remove(self._logFileName)

  def getFile(self):
    return self.fd

_logger = Logger()

def _openFile(filePath):
  f = open(filePath, "rb")
  try:
    data = f.read()
    detector = chardet.detect(data)
    #logging.info("{0} {1}".format(detector["encoding"],
    #  detector["confidence"]))
    if detector["encoding"] is None:
      logging.warn("Encoding is of the file {0} None that means "
          "the file is probaly not a text file".format(
            filePath))
      return None
    data = data.decode(detector["encoding"])
    return data
  except:
    logging.exception("Got exception from open the file {0}"
        .format(filePath))
  finally:
    f.close()

def _openFileInVim(filePath):
  bufnr = -1
  try:
    vim.command("set shortmess+=A")
    vim.command("edit " + filePath)
    vim.command("set shortmess-=A")
    bufnr = int(vim.eval("bufnr('{0}')".format(filePath)))
  except vim.error as e:
    logging.exception('Got exception from vim open command')
    return -1
  return bufnr

def _getBuffer(filePath):
  bufnr = int(vim.eval("bufnr('{0}')".format(filePath)))
  return bufnr

def _checkFilePath(pattern, filePath):
  filePathMatch = re.match(
      "^(?:[^.]*\.(?:py[odc]|exe|dll|so|lib|a|obj|o|d"
      "|pdf|djvu|djv"
      "|mp3|ogg"
      "|avi|mp4|mkv"
      "|7z|zip|tar.*|bz|gz|xz|out|log|rar|db"
      "|png|jpg|jpeg|ico|tiff|gif|svg))$", os.path.basename(filePath))
  if not filePathMatch is None:
    return False
  if not (pattern is None) and (pattern != ""):
    filePathMatch = re.match(pattern, filePath)
    if filePathMatch is None:
      return False
  return True

def globalFind(command, filesString):
  try:
     _globalFind(command, filesString)
  except:
     logging.exception('Got exception on main handler')
     raise

def globalReplace(command, filesString):
  try:
     _globalReplace(command, filesString)
  except:
     logging.exception('Got exception on main handler')
     raise

class _File(object):

  def __init__(self, filePath):
    self.filePath = filePath
    self.bufferNumber = _getBuffer(self.filePath)
    self.data = None

  def prepareData(self):
    if self.bufferNumber >= 0:
      buffer = vim.buffers[self.bufferNumber]
      self.data = buffer[0:len(buffer)]
      fileFormat = buffer.options["fileformat"]
      if fileFormat == "unix":
        self.data = '\n'.join(self.data)
      else:
        self.data = '\r\n'.join(self.data)

  def getData(self):
    if self.bufferNumber < 0:
      self.data = _openFile(self.filePath)
      return self.data
    else:
      return self.data

  def releaseData(self):
    self.data = None

  def openInVim(self):
    if self.bufferNumber < 0:
      self.bufferNumber = _openFileInVim(self.filePath)
    return self.bufferNumber

def recvall(sock, count):
  buf = b''
  while count:
      newbuf = sock.recv(count)
      if not newbuf: return None
      buf += newbuf
      count -= len(newbuf)
  return buf

def send_one_message(sock, data):
  length = len(data)
  sock.sendall(struct.pack('!q', length))
  sock.sendall(data)

def recv_one_message(sock):
  lengthbuf = recvall(sock, 8)
  length = struct.unpack('!q', lengthbuf)[0]
  if length == -1:
    return None
  return recvall(sock, length)

class _SearchScript(object):
  def __init__(self):
    pass

  def initialize(self):
    try:
      self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error as msg:
      logging.exception("Fail to allocate socket: {0}".format(msg))
    try:
      self.socket.bind(('localhost', 0))
      self.socket.listen(1)
    except socket.error as msg:
      logging.exception("Fail to bind socket: {0}".format(msg))
    currentDir = os.path.dirname(os.path.realpath(
      os.path.abspath(inspect.getfile(inspect.currentframe()))))
    clientScript = os.path.join(currentDir, "search.py")
    port = self.socket.getsockname()[1]
    self.process = subprocess.Popen(
        ["python",
          clientScript,
          str(port)],
        stdout = _logger.getFile(),
        stderr = _logger.getFile(),
        shell = True)
    timeout = socket.getdefaulttimeout()
    socket.setdefaulttimeout(5)
    self.connection, address = self.socket.accept()
    socket.setdefaulttimeout(timeout)

  def sendString(self, filePath):
    try:
      send_one_message(self.connection, filePath)
    except socket.error as msg:
      logging.exception("Fail to send string {0}: {1}"
          .format(filePath, msg))

  def sendEnd(self):
    try:
      self.connection.sendall(struct.pack('!q', -1))
    except socket.error as msg:
      logging.exception("Fail to send end: {0}".format(msg))

  def receiveEntity(self):
    try:
      data = recv_one_message(self.connection)
      if data is None: return None
      data = pickle.loads(data)
      return data
    except socket.error as msg:
      logging.exception("Fail to receive an entity: {0}".format(msg))

def _globalFind(command, filesString):
  m = re.search(
      "^((?:[^/]|(?<=\\\\)/)*)(?:(?<!\\\\)/((?:[^/]|(?<=\\\\)/)*)(?:(?<!\\\\)/)?)?$",
      command, re.M and re.S)
  if (m is None):
    vim.command("echo \"Wrong command {0}\"".format(command))
    return
  pattern = re.sub("\\\\/", "/", m.group(1))
  if pattern == "":
    vim.command("echo \"No search pattern\"")
    return
  #files = filesString.split(',')
  files = filesString
  entities = []
  sendFilePathes = []
  searchScript = _SearchScript()
  searchScript.initialize()
  searchScript.sendString(pattern)
  for tempPath in files:
    if os.path.isdir(tempPath):
      for tempRoot, tempDirs, tempFiles in os.walk(tempPath):
        for tempFile in tempFiles:
          tempFilePath = os.path.join(tempRoot, tempFile)
          if _checkFilePath(m.group(2), tempFilePath):
            searchScript.sendString(tempFilePath)
            sendFilePathes.append(tempFilePath)
    elif os.path.isfile(tempPath):
      if _checkFilePath(m.group(2), tempPath):
        searchScript.sendString(tempPath)
        sendFilePathes.append(tempPath)
  searchScript.sendEnd()
  files = None
  while True:
    entity = searchScript.receiveEntity()
    if entity is None: break
    fileNumber = entity["bufnr"]
    filePath = sendFilePathes[fileNumber]
    bufferNumber = _getBuffer(filePath)
    if bufferNumber < 0:
      bufferNumber = _openFileInVim(filePath)
    entity["bufnr"] = bufferNumber
    entities.append(entity)
  vim.Function('setqflist')(entities, 'r')
  vim.command("copen")

def _updateBuffer(data, buffer):
  views = []
  currentWindow = vim.current.window
  for window in vim.windows:
    if (window.buffer.number == buffer.number):
      vim.command('%swincmd w' % window.number)
      views.append((window, vim.eval('winsaveview()')))
  data = data.split('\n')
  if len(data) > 0:
    try:
      del buffer[:]
      buffer.append(data)
      del buffer[0]
    except vim.error as e:
      print(e.strerror)
  for window, view in views:
    vim.command('%swincmd w' % window.number)
    vim.eval('winrestview(%s)' % view)
  vim.command('%swincmd w' % currentWindow.number)

class _Replacement(object):
  def __init__(self,
      bufferNumber,
      lineCount,
      data,
      number,
      pattern,
      replacement):
    self.bufferNumber = bufferNumber
    self.lineCount = lineCount
    self.data = data
    self.number = number
    self.replacement = replacement
    self.pattern = pattern
    self.entities = []
    self.newLineIterator = re.finditer("\n", self.data)
    self.newLinePosition = 0
    if lineCount > 1: self.newLineMatch = self.newLineIterator.next()
    self.lineNumber = 1

  def __call__(self, match):
    columnNumber = 0
    while (self.lineCount > self.lineNumber and
           self.newLineMatch.start() < match.start()):
        self.lineNumber += 1
        self.newLinePosition = self.newLineMatch.start()
        if self.lineCount != self.lineNumber:
          self.newLineMatch = self.newLineIterator.next()
    columnNumber = match.start() - self.newLinePosition
    result = match.expand(self.replacement)
    entity = {
      "bufnr" : self.bufferNumber,
      "lnum" : self.lineNumber,
      "col" : columnNumber,
      "vcol" : 1,
      #"nr" : self.number,
      "text" : result
      }
    #print(entity['text'])
    self.number += 1
    self.entities.append(entity)
    return result

def _globalReplace(command, filesString):
  m = re.search(
      "(.*)(?<!\\\\)/(.*)(?<!\\\\)(?:/(.*)(?:(?<!\\\\)/)?)?",
      command, re.M and re.S)
  if (m is None):
    print("Wrong command")
    return
  pattern = re.sub("\\\\/", "/", m.group(1))
  replacement = re.sub("\\\\/", "/", m.group(2))
  files = filesString.split(',')
  number = 1
  entities = []
  for filePath in files:
    if not _checkFilePath(m.group(3), filePath): continue
    # Get data and initialize
    bufferNumber = _getBuffer(filePath)
    if bufferNumber == -1: continue
    buffer = buffers[bufferNumber]
    lineCount = len(buffer)
    data = buffer[0:lineCount]
    data = '\n'.join(data)
    # Start replace operation
    replacingObject = _Replacement(
        bufferNumber,
        lineCount,
        data,
        number,
        pattern,
        replacement)
    newData = re.sub(pattern, replacingObject, data)
    number = replacingObject.number
    #Update buffer
    if len(replacingObject.entities) > 0:
      entities.extend(replacingObject.entities)
      _updateBuffer(newData, buffer)
  vim.Function('setqflist')(entities, 'r')
  vim.command("copen")
