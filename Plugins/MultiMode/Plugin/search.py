#!/usr/bin/env python
# encoding: utf-8

import os
import sys
import re
import socket
import struct
import mmap
import cPickle as pickle
import multiprocessing
import logging
import tempfile
#import chardet

class Logger(object):
  def __init__(self):
    #self._fd, self._logFileName = tempfile.mkstemp(prefix = "SearchLog")
    #logging.basicConfig(filename = self._logFileName, level=logging.DEBUG)
    logging.basicConfig(
        level=logging.INFO,
        format='search.py:%(levelname)s:%(message)s')

  def __del__(self):
    logging.shutdown()
    #os.close(self._fd)
    #os.remove(self._logFileName)

_logger = Logger()

class _File(object):

  def __init__(self, filePath):
    self.f = None
    self.mm = None
    self.encoding = None
    try:
      if os.stat(filePath).st_size == 0: return
      self.f = open(filePath, "rb")
    except:
      logging.exception("Got exception from open the file {0}"
          .format(filePath))
      return
    try:
      self.mm = mmap.mmap(self.f.fileno(), 0, access = mmap.ACCESS_READ)
    except:
      logging.exception("Got exception from open the file {0}"
          .format(filePath))
      self.release()
      return
    #try:
    #  from chardet.universaldetector import UniversalDetector
    #  detector = UniversalDetector()
    #  for line in iter(self.mm.readline, ""):
    #      detector.feed(line)
    #      if detector.done: break
    #  detector.close()
    #  self.encoding = detector.result["encoding"]
    #  #logging.info("{0} {1}".format(detector["encoding"],
    #  #  detector["confidence"]))
    #  if self.encoding is None:
    #    logging.warn("Encoding is of the file {0} None that means "
    #        "the file is probaly not a text file".format(filePath))
    #    self.release()
    #    return
    #except:
    #  logging.exception("Got exception from open the file {0}"
    #      .format(filePath))
    #  self.release()
    #  return

  def release(self):
    if not self.mm is None:
      self.mm.close()
      self.mm = None
    if not self.f is None:
      self.f.close()
      self.f = None

  def isEmpty(self):
    return self.mm is None

  def getData(self):
    return self.mm

  def encodePattern(self, pattern):
    #if self.encoding is None:
    #  return None
    #upattern = pattern.decode("UTF-8")
    #try:
    #  newpattern = upattern.encode(self.encoding)
    #  return newpattern
    #except:
    #  logging.exception("Failed encode pattern to {0}".format(self.encoding))
    #  return None
    return pattern

def _findInText(filePath, originalPattern):
  f = _File(filePath)
  if f.isEmpty(): return []
  entities = []
  pattern = f.encodePattern(originalPattern)
  if pattern is None:
    return []
  try:
    newLineIterator = re.finditer("\n", f.getData())
    newLinePosition = 0
    newLineStart = 0
    try:
      newLineStart = newLineIterator.next().start()
    except StopIteration:
      newLineStart = sys.maxint
    lineNumber = 1
    for match in re.finditer(pattern, f.getData()):
      columnNumber = 0
      while newLineStart < match.start():
        lineNumber += 1
        newLinePosition = newLineStart + 1
        try:
          newLineStart = newLineIterator.next().start()
        except StopIteration:
          newLineStart = sys.maxint
      columnNumber = 1 + match.start() - newLinePosition
      entity = {
        "lnum" : lineNumber,
        "col"  : columnNumber,
        "vcol" : 1,
        "text" : match.group()
        }
      entities.append(entity)
  except:
    logging.exception("Failed to preforme a search in {0}".format(filePath))
    entities = []
  finally:
    f.release()
  return entities

def _findInTextParallel(args):
  return _findInText(*args)

def recvall(sock, count):
  buf = b''
  while count:
    newbuf = sock.recv(count)
    if not newbuf: return None
    buf += newbuf
    count -= len(newbuf)
  return buf

def send_one_message(sock, data):
  length = len(data)
  sock.sendall(struct.pack('!q', length))
  sock.sendall(data)

def recv_one_message(sock):
  lengthbuf = recvall(sock, 8)
  length = struct.unpack('!q', lengthbuf)[0]
  if length < 0: return None
  return recvall(sock, length)

def run(port, sock):
  try :
    sock.connect(('localhost', port))
  except socket.error as msg:
    logging.exception("Fail to bind socket: {0}".format(msg))
  logging.info("Connection established")
  try :
    pattern = recv_one_message(sock)
  except socket.error as msg:
    logging.exception("Fail to receive a pattern: {0}".format(msg))
  pattern = pattern.decode('UTF-8')
  poolOutputs = []
  poolSize = multiprocessing.cpu_count()
  pool = multiprocessing.Pool(processes = poolSize)
  while True:
    try :
      filePath = recv_one_message(sock)
    except socket.error as msg:
      logging.exception("Fail to receive a file path: {0}".format(msg))
    if filePath is None:
      break
    filePath = filePath.decode('UTF-8')
    poolOutputs.append(
        pool.apply_async(_findInTextParallel, ((filePath, pattern),)))
  pool.close()
  poolOutputSize = len(poolOutputs)
  isDone = False if poolOutputSize == 0 else True
  entities = []
  i = 0
  while isDone:
    if poolOutputs[i].ready():
      output = poolOutputs[i].get()
      for entity in output:
        entity["bufnr"] = i
        data = pickle.dumps(entity, -1)
        send_one_message(sock, data)
      i = i + 1
      if i == poolOutputSize:
        isDone = False
        break
  sock.sendall(struct.pack('!q', -1))
  pool.join()

if __name__ == '__main__':
  if (len(sys.argv) < 2):
    logging.error("No port was supplied")
    sys.exit(-1)
  port = int(sys.argv[1])
  try :
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  except socket.error as msg:
    logging.exception("Fail to create socket: {0}".format(msg))
    sys.exit(-1)
  try:
    run(port, sock)
  except:
    logging.exception('Got in run')
  finally:
    sock.close()

