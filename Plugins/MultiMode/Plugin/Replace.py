#!/usr/bin/env python
# encoding: utf-8

from Configurator import *

bindings = []
isOn = False

def turnOff():
  global isOn, bindings
  if not isOn:
    return
  for bind in bindings:
    bind.execute()
  bindings = []
  isOn = False

def turnOn():
  global isOn
  if isOn:
    return
  bindings.append(keysManager.getNormalMap("1"))
  bindings.append(keysManager.getVisualMap("1"))
  bindings.append(keysManager.getSelectMap("1"))
  bindings.append(keysManager.getNormalMap("2"))
  bindings.append(keysManager.getVisualMap("2"))
  bindings.append(keysManager.getSelectMap("2"))
  bindings.append(keysManager.getNormalMap("3"))
  bindings.append(keysManager.getVisualMap("3"))
  bindings.append(keysManager.getSelectMap("3"))
  bindings.append(keysManager.getNormalMap("4"))
  bindings.append(keysManager.getVisualMap("4"))
  bindings.append(keysManager.getSelectMap("4"))
  bindings.append(keysManager.getNormalMap("5"))
  bindings.append(keysManager.getVisualMap("5"))
  bindings.append(keysManager.getSelectMap("5"))
  bindings.append(keysManager.getNormalMap("6"))
  bindings.append(keysManager.getVisualMap("6"))
  bindings.append(keysManager.getSelectMap("6"))
  bindings.append(keysManager.getCommandLineMap("<Cr>"))
  map("1", "yiw:s/\\<<C-r>\"\\>//g<Left><Left>", Normal, NoRemap)
  map("1", "y:s/<C-r>\"//g<Left><Left>", Visual, Select, NoRemap)
  map("2", "yiw:s/\\<<C-r>\"\\>/<C-r>\"/g<Left><Left>", Normal, NoRemap)
  map("2", "y:s/<C-r>\"/<C-r>\"/g<Left><Left>", Visual, Select, NoRemap)
  map("3", ":s//<C-r>\"/g<Left><Left>", Normal, NoRemap)
  map("3", ":s//<C-r>\"/g<Left><Left>", Visual, Select, NoRemap)
  map("4", "yiw:%s/\<<C-r>\"\>//g<Left><Left>", Normal, NoRemap)
  map("4", "y:%s/<C-r>\"//g<Left><Left>", Visual, Select, NoRemap)
  map("5", "yiw:%s/\<<C-r>\"\>/<C-r>\"/g<Left><Left>", Normal, NoRemap)
  map("5", "y:%s/<C-r>\"/<C-r>\"/g<Left><Left>", Visual, Select, NoRemap)
  map("6", "yiw:%s//<C-r>\"/g<Left><Left>", Normal, NoRemap)
  map("6", "y:%s//<C-r>\"/g<Left><Left>", Visual, Select, NoRemap)
  map("<Cr>", "<Cr>:py import Replace;Replace.turnOff()<Cr>", CommandLine, NoRemap, Silent)
  isOn = True

def toggleReplaceMode():
  if isOn:
    turnOff()
  else:
    turnOn()
  pass
