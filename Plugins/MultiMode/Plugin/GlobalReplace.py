#!/usr/bin/env python
# encoding: utf-8

import Replace

from Configurator import *

import os
import re
import vim

bindings = []
isOn = False

def globalReplace(string):
  m = re.search(
      "/(.*)(?<!\\\\)/(.*)(?<!\\\\)/(.*)(?:(?<!\\\\)/)?",
      string, re.M and re.S)
  if (m is None):
    print "Wrong command"
    return
  currentDir = vim.eval("getcwd()")
  for root, dirs, files in os.walk(currentDir):
    for file in files:
      filePath = os.path.join(root, file)
      if not (m.group(3) is None) and (m.group(3) != ""):
        filePathMatch = re.match(m.group(3), filePath)
        if filePathMatch is None:
          continue
      bufnr = int(vim.eval("bufnr('%s')" % filePath))
      if (bufnr < 0):
        vim.command("edit " + filePath)
        bufnr = int(vim.eval("bufnr('$')"))
      else:
        vim.command("b %i" % bufnr)
      buffer = vim.buffers[bufnr]
      data = buffer[0:len(buffer)]
      replacePattern1 = re.sub("\\\\/", "/", m.group(1))
      replacePattern2 = re.sub("\\\\/", "/", m.group(2))
      data = re.sub(replacePattern1, replacePattern2, '\n'.join(data))
      #winnr = int(vim.eval("bufwinnr('%s')" % filePath))
      views = []
      currentWindow = vim.current.window
      for window in vim.windows:
        if (window.buffer.number == buffer.number):
          vim.command('%swincmd w' % window.number)
          views.append((window, vim.eval('winsaveview()')))
      vim.command("normal! ggVGx")
      data = data.split('\n')
      buffer.append(data)
      vim.command("normal! ggdd")
      for window, view in views:
        vim.command('%swincmd w' % window.number)
        vim.eval('winrestview(%s)' % view)
      vim.command('%swincmd w' % currentWindow.number)

def turnOff():
  global isOn, bindings
  if not isOn:
    return
  for bind in bindings:
    bind.execute()
  bindings = []
  isOn = False

def turnOn():
  global isOn
  if isOn:
    return
  Replace.turnOff()
  bindings.append(keysManager.getNormalMap("1"))
  bindings.append(keysManager.getVisualMap("1"))
  bindings.append(keysManager.getSelectMap("1"))
  bindings.append(keysManager.getNormalMap("2"))
  bindings.append(keysManager.getVisualMap("2"))
  bindings.append(keysManager.getSelectMap("2"))
  map("1", "yiw:R /<C-r>\"//<Left>", Normal, NoRemap)
  map("1", "y:R /<C-r>\"//<Left>", Visual, Select, NoRemap)
  map("2", "yiw:R /<C-r>\"/<C-r>\"/<Left>", Normal, NoRemap)
  map("2", "y:R /<C-r>\"/<C-r>\"/<Left>", Visual, Select, NoRemap)
  map("<Cr>", "<Cr>:py import GlobalReplace;GlobalReplace.turnOff()<Cr>",
      CommandLine, NoRemap, Silent)
  isOn = True

def toggleGlobalReplaceMode():
  if isOn:
    turnOff()
  else:
    turnOn()
  pass
