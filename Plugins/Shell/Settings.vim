let g:vimshell_prompt = ">>>"
let g:vimshell_user_prompt = 'fnamemodify(getcwd(), ":~")'
let g:vimshell_temporary_directory = g:trash."/vimshell"
let g:vimshell_enable_smart_case = 1
let g:vimshell_no_default_keymappings = 1

let s:vimshell_substitutedBufferNumber = -1
let s:vimshell_buffer = -1

function! Mychpwd(args, context)
  call vimshell#execute('ls')
endfunction
" Initialize execute file list.
let g:vimshell_execute_file_list = {}
call vimshell#set_execute_file('txt,md,markdown,vim,h,hxx,hii,cii,hpp,c,cxx,cpp,d,xml,java,tex', 'vim')
let g:vimshell_execute_file_list['rb'] = 'ruby'
let g:vimshell_execute_file_list['pl'] = 'perl'
let g:vimshell_execute_file_list['py'] = 'python'

autocmd FileType vimshell
      \ call vimshell#hook#add('chpwd', 'my_chpwd', 'g:Mychpwd')
      \| call s:SetupKeybindings()

autocmd FileType int-*
    \ call s:SetupInteractiveKeybindings()

function! s:SetupKeybindings()

  " setlocal nobuflisted
  setlocal buftype=nowrite
  " set nobuflisted
  " Normal mode key-mappings.
  " Execute command.
  nmap <buffer> <CR> <Plug>(vimshell_enter)
  " Hide vimshell.
  nmap <buffer> <F4> <Plug>(vimshell_hide)
  imap <buffer> <F4> <Plug>(vimshell_hide)
  nmap <buffer> q <Plug>(vimshell_hide)
  nmap <buffer> <A-q> <Plug>(vimshell_hide)
  nmap <buffer> <C-q> <Plug>(vimshell_hide)
  " Exit vimshell.
  nmap <buffer> Q <Plug>(vimshell_exit)
  nmap <buffer> 4 <Plug>(vimshell_previous_prompt)
  nmap <buffer> 3 <Plug>(vimshell_next_prompt)
  nmap <buffer> <C-p> <Plug>(vimshell_previous_prompt)
  nmap <buffer> <C-n> <Plug>(vimshell_next_prompt)
  " Paste this prompt.
  nmap <buffer> <C-y> <Plug>(vimshell_paste_prompt)
  " Search end argument.
  nmap <buffer> E <Plug>(vimshell_move_end_argument)
  " Change line.
  nmap <buffer> cc <Plug>(vimshell_change_line)
  " Delete line.
  nmap <buffer> dd <Plug>(vimshell_delete_line)
  " Start insert.
  nmap <buffer> I         <Plug>(vimshell_insert_head)
  nmap <buffer> A         <Plug>(vimshell_append_end)
  nmap <buffer> i         <Plug>(vimshell_insert_enter)
  nmap <buffer> a         <Plug>(vimshell_append_enter)
  "nmap <buffer> ^         <Plug>(vimshell_move_head)
  " Interrupt.
  nmap <buffer> <C-c> <Plug>(vimshell_interrupt)
  imap <buffer> <C-c> <Plug>(vimshell_interrupt)
  nmap <buffer> <C-k> <Plug>(vimshell_hangup)
  " Clear.
  nmap <buffer> <C-l> <Plug>(vimshell_clear)
  " Execute background.
  nmap <buffer> <C-z> <Plug>(vimshell_execute_by_background)

  " Insert mode key-mappings.
  " Execute command.
  inoremap <expr> <SID>(bs-ctrl-])
        \ getline('.')[col('.') - 2] ==# "\<C-]>" ? "\<BS>" : ''
  imap <buffer> <C-]>               <C-]><SID>(bs-ctrl-])
  imap <buffer> <CR>                <C-]><Plug>(vimshell_enter)

  " History completion.
  imap <buffer> <C-l> <Plug>(vimshell_history_unite)
  inoremap <buffer><expr> <C-p> pumvisible() ? "\<C-p>" :
        \ <SID>start_history_complete()
  inoremap <buffer><expr> <C-n> pumvisible() ? "\<C-n>" :
        \ <SID>start_history_complete()
  inoremap <buffer><expr> <Up> pumvisible() ? "\<C-p>" :
        \ <SID>start_history_complete()
  inoremap <buffer><expr> <Down> pumvisible() ? "\<C-n>" :
        \ <SID>start_history_complete()

  " Command completion.
  imap <buffer> <TAB>  <Plug>(vimshell_command_complete)
  " Move to Beginning of command.
  imap <buffer> <C-a> <Plug>(vimshell_move_head)
  " Delete all entered characters in the current line.
  imap <buffer> <C-u> <Plug>(vimshell_delete_backward_line)
  " Delete previous word characters in the current line.
  imap <buffer> <C-w> <Plug>(vimshell_delete_backward_word)
  " Push current line to stack.
  imap <silent><buffer><expr> <C-z> vimshell#mappings#smart_map(
        \ "\<Plug>(vimshell_push_current_line)",
        \ "\<Plug>(vimshell_execute_by_background)")
  " Insert last word.
  imap <buffer> <C-t> <Plug>(vimshell_insert_last_word)
  " Delete char.
  imap <buffer> <C-h>    <Plug>(vimshell_delete_backward_char)
  imap <buffer> <BS>     <Plug>(vimshell_delete_backward_char)
  " Delete line.
  imap <buffer> <C-k>     <Plug>(vimshell_delete_forward_line)
endfunction

function! s:SetupInteractiveKeybindings()
  " Normal mode key-mappings.
  nmap <buffer> <C-p>     <Plug>(vimshell_int_previous_prompt)
  nmap <buffer> <C-n>     <Plug>(vimshell_int_next_prompt)
  nmap <buffer> <CR>      <Plug>(vimshell_int_execute_line)
  nmap <buffer> <C-y>     <Plug>(vimshell_int_paste_prompt)
  nmap <buffer> <C-z>     <Plug>(vimshell_int_restart_command)
  nmap <buffer> <C-c>     <Plug>(vimshell_int_interrupt)
  nmap <buffer> q         <Plug>(vimshell_int_exit)
  nmap <buffer> cc         <Plug>(vimshell_int_change_line)
  nmap <buffer> dd         <Plug>(vimshell_int_delete_line)
  nmap <buffer> I         <Plug>(vimshell_int_insert_head)
  nmap <buffer> A         <Plug>(vimshell_int_append_end)
  nmap <buffer> i         <Plug>(vimshell_int_insert_enter)
  nmap <buffer> a         <Plug>(vimshell_int_append_enter)
  nmap <buffer> <C-l>     <Plug>(vimshell_int_clear)

  " Insert mode key-mappings.
  imap <buffer> <C-h>     <Plug>(vimshell_int_delete_backward_char)
  imap <buffer> <BS>     <Plug>(vimshell_int_delete_backward_char)
  imap <buffer> <C-a>     <Plug>(vimshell_int_move_head)
  imap <buffer> <C-u>     <Plug>(vimshell_int_delete_backward_line)
  imap <buffer> <C-w>     <Plug>(vimshell_int_delete_backward_word)
  imap <buffer> <C-k>     <Plug>(vimshell_int_delete_forward_line)
  imap <buffer> <C-]>               <C-]><SID>(bs-ctrl-])
  imap <buffer> <CR>      <C-]><Plug>(vimshell_int_execute_line)
  imap <buffer> <C-c>     <Plug>(vimshell_int_interrupt)
  imap <buffer> <C-l>     <Plug>(vimshell_int_history_unite)
  imap <buffer> <C-v>  <Plug>(vimshell_int_send_input)
  inoremap <buffer> <C-n>     <C-n>
  imap <buffer><expr> <TAB>
        \ pumvisible() ? "\<C-n>" :
        \ "\<Plug>(vimshell_int_command_complete)"
endfunction

let s:cur_popup_shell_buf_nr = -1

function! s:MyPopupShellOpen()
  " if s:cur_popup_shell_buf_nr == bufnr("%")
  "   call feedkeys("\<Esc>")
  "   execute "normal q"
  "   let s:cur_popup_shell_buf_nr = -1
  "   return
  " endif
  execute "VimShell -popup -project"
  let s:cur_popup_shell_buf_nr = bufnr('%')
endfunction

command! -nargs=* MyPopupShellOpen
      \ :call s:MyPopupShellOpen()
