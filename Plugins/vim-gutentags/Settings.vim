let g:gutentags_file_list_command = {
    \ 'markers': {
        \ '.git': 'git ls-files',
        \ '.hg': 'hg files',
        \ },
    \ }
" let g:gutentags_trace=0
let g:gutentags_ctags_exclude=["*.json", "*xml", "*css", "*tex", "*node_modules*"]

" gutentags: ctags -f "tags.temp"  --exclude=*.pyc --exclude=*.pyo --exclude=*.dll --exclude=*.so --exclude=*.lib --exclude=*.a --exclude=*.obj --exclude=*.o --exclude=*.d --exclude=*.pdf --exclude=*.djvu --exclude=*.djv --exclude=*.mp3 --exclude=*.ogg --exclude=*.flv --exclude=*.avi --exclude=*.mp4 --exclude=*.mkv --exclude=*.ttf --exclude=*.7z --exclude=*.zip --exclude=*.tar --exclude=*.bz --exclude=*.bz2 --exclude=*.gz --exclude=*.xz --exclude=*.rar*.db --exclude=*.out --exclude=*.log --exclude=*.swp --exclude=*.build*/.git/* --exclude=*/.hg/* --exclude=*//.git//* --exclude=*//.hg//* --exclude=*.png --exclude=*.jpg --exclude=*.jpeg --exclude=*.ico --exclude=*.tiff --exclude=*.gif --exclude=*.svg --exclude=*.json --exclude=*xml --exclude=*css --exclude=*tex --exclude=*node_modules* -L tags.files "."

