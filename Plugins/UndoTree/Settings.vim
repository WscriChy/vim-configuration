let g:undotree_SplitWidth = 45
let g:undotree_DiffpanelHeight = 80

let g:undotree_DiffCommand = "git diff --no-index --minimal"
