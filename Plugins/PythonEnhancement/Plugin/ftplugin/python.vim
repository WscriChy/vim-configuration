if exists("b:did_ftplugin")
  if !exists("b:PythonEnhancementFtPlugin")
    let b:PythonEnhancementFtPlugin = 1
  else
    finish
  endif
endif
let s:keepcpo= &cpo
set cpo&vim

" Redefine runtime values
" setlocal expandtab shiftwidth=2 softtabstop=2 tabstop=8

let &cpo = s:keepcpo
unlet s:keepcpo
