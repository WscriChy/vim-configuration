#!/usr/bin/env python -OO
# encoding: utf-8

from Configurator import *

def setKeyBindings():
  map("<Cr>", "<Cr>",
      Normal, Visual, Select, NoRemap, Silent, Buffer)

hook("FileType qf", setKeyBindings)
