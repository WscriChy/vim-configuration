let g:fanfingtastic_all_inclusive = 0

autocmd VimEnter *
  \ execute "FanfingTasticAlias <C-W> /\\C[[:alnum:]]\\@<![[:alnum:]]\\\\|[^[:upper:]]\\@<=[[:upper:]]\\\\|[[:upper:]][[:lower:]]\\@=/"

autocmd VimEnter *
  \ execute "FanfingTasticAlias <C-E> /\\C[[:alnum:]][[:alnum:]]\\@!\\\\|[[:lower:][:digit:]][[:upper:]]\\@=\\\\|[[:upper:]]\\%([[:upper:]][[:lower:]]\\)\\@=/"

autocmd VimEnter * execute "FanfingTasticAlias <C-F> []{}()"
