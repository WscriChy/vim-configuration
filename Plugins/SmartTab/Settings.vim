
"   set this to true if script used as a filetype plugin
" g:ctab_filetype_maps
" g:ctab_disable_checkalign
"   set this to true to disable re-check of alignment
" disable the filetype specific maps
let g:ctab_enable_default_filetype_maps = 0
" disable the (original) tab mappings
let g:ctab_disable_tab_maps = 0
