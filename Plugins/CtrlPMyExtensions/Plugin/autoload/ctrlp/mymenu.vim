" Load guard
if ( exists('g:loaded_CtrlPMyExtensions') && g:loaded_CtrlPMyExtensions )
	\ || v:version < 700 || &cp
	finish
endif
let g:loaded_CtrlPMyExtensions = 1


" Add this extension's settings to g:ctrlp_ext_vars
"
" Required:
"
" + init: the name of the input function including the brackets and any
"         arguments
"
" + accept: the name of the action function (only the name)
"
" + lname & sname: the long and short names to use for the statusline
"
" + type: the matching type
"   - line : match full line
"   - path : match full line like a file or a directory path
"   - tabs : match until first tab character
"   - tabe : match until last tab character
"
" Optional:
"
" + enter: the name of the function to be called before starting ctrlp
"
" + exit: the name of the function to be called after closing ctrlp
"
" + opts: the name of the option handling function called when initialize
"
" + sort: disable sorting (enabled by default when omitted)
"
" + specinput: enable special inputs '..' and '@cd' (disabled by default)
"
let s:ctrlp_var = {
  \ 'init': 'ctrlp#mymenu#init()',
  \ 'accept': 'ctrlp#mymenu#accept',
  \ 'lname': '',
  \ 'sname': '',
  \ 'type': 'line',
  \ 'enter': 'ctrlp#mymenu#enter()',
  \ 'exit': 'ctrlp#mymenu#exit()',
  \ 'opts': 'ctrlp#mymenu#opts()',
  \ 'sort': 0,
  \ 'specinput': 0,
  \ }
if exists('g:ctrlp_ext_vars') && !empty(g:ctrlp_ext_vars)
  let g:ctrlp_ext_vars = add(g:ctrlp_ext_vars, s:ctrlp_var)
else
  let g:ctrlp_ext_vars = [s:ctrlp_var]
endif

let s:builtins = [
  \ {'sname' : 'cursor-cross', 'lname' : 'set cursorline! | set cursorcolumn!', 'test' : '&cursorline'},
  \ {'sname' : 'spell', 'lname' : 'set spell!', 'test' : '&spell'},
  \ {'sname' : 'number', 'lname' : 'set number!', 'test' : '&number'},
  \ {'sname' : 'scroll-offset', 'lname' : 'set so=20', 'inverse' : 'set so=0', 'test' : '&so'},
  \ ]


" Provide a list of strings to search in
"
" Return: a Vim's List
"
function! ctrlp#mymenu#init()
  let ret = []
  for i in s:builtins
    let prefix = ""
    let value = getwinvar(winnr('#'), i.test)
    if value != 0
      let prefix = "*"
    endif
    let str = printf("%s\t%s\t| %s", prefix, i.sname, i.lname)
    call add(ret, str)
  endfor
  return ret
endfunction


" The action to perform on the selected string
"
" Arguments:
"  a:mode   the mode that has been chosen by pressing <cr> <c-v> <c-t> or <c-x>
"           the values are 'e', 'v', 't' and 'h', respectively
"  a:str    the selected string
"
function! ctrlp#mymenu#accept(mode, str)
  call ctrlp#exit()
  let sname = split(a:str, "\t", 1)[1]
  let n = index(map(copy(s:builtins), 'v:val.sname'), sname)
  if has_key(s:builtins[n], 'inverse')
    let value = getwinvar(winnr(), s:builtins[n].test)
    if value != 0
      silent execute s:builtins[n].inverse
    else
      silent execute s:builtins[n].lname
    endif
    return
  endif
  silent execute s:builtins[n].lname
endfunction


" (optional) Do something before enterting ctrlp
function! ctrlp#mymenu#enter()
endfunction


" (optional) Do something after exiting ctrlp
function! ctrlp#mymenu#exit()
endfunction


" (optional) Set or check for user options specific to this extension
function! ctrlp#mymenu#opts()
endfunction


" Give the extension an ID
let s:id = g:ctrlp_builtins + len(g:ctrlp_ext_vars)

" Allow it to be called later
function! ctrlp#mymenu#id()
  return s:id
endfunction
