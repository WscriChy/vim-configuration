let g:ctrlp_map = ''

let g:ctrlp_match_window = 'top,order:tbb,min:1,max:50,results:30'

let g:ctrlp_switch_buffer = ''

" 1r - open the first file in the current window, and remaining opened as hidden buffers.
let g:ctrlp_open_multiple_files = '1r'
let g:ctrlp_cache_dir = g:trash.'/ctrlp'
let g:ctrlp_max_files = 0
let g:ctrlp_max_depth = 40

let g:ctrlp_follow_symlinks = 0

let g:ctrlp_match_func = {'match' : 'matcher#cmatch' }

let g:ctrlp_extensions = ['tag', 'buffertag', 'quickfix', 'dir', 'rtscript',
                        \ 'undo', 'line', 'changes', 'mymenu']

let g:ctrlp_prompt_mappings = {
  \ 'PrtBS()':              ['<BS>', '<c-]>'],
  \ 'PrtDelete()':          ['<DEL>'],
  \ 'PrtDeleteWord()':      ['<c-w>'],
  \ 'PrtClear()':           ['<c-u>'],
  \ 'PrtSelectMove("j")':   ['<c-j>', '<a-j>'],
  \ 'PrtSelectMove("k")':   ['<c-k>', '<a-k>'],
  \ 'PrtSelectMove("t")':   ['<Home>', '<kHome>'],
  \ 'PrtSelectMove("b")':   ['<End>', '<kEnd>'],
  \ 'PrtSelectMove("u")':   ['<PageUp>', '<kPageUp>'],
  \ 'PrtSelectMove("d")':   ['<PageDown>', '<kPageDown>'],
  \ 'PrtHistory(-1)':       ['<c-n>'],
  \ 'PrtHistory(1)':        ['<c-p>'],
  \ 'AcceptSelection("e")': ['<cr>', '<2-LeftMouse>'],
  \ 'AcceptSelection("h")': ['<c-x>', '<c-cr>', '<c-s>'],
  \ 'AcceptSelection("t")': ['<c-t>'],
  \ 'AcceptSelection("v")': ['<c-\>', '<RightMouse>'],
  \ 'ToggleFocus()':        ['<s-tab>', '<a-u>',],
  \ 'ToggleRegex()':        ['<c-r>'],
  \ 'ToggleByFname()':      ['<c-d>'],
  \ 'ToggleType(1)':        ['<c-l>', '<c-up>'],
  \ 'ToggleType(-1)':       ['<c-h>', '<c-down>'],
  \ 'PrtExpandDir()':       ['<tab>'],
  \ 'PrtInsert("c")':       ['<MiddleMouse>', '<insert>'],
  \ 'PrtInsert()':          ['<c-\>', '<a-p>'],
  \ 'PrtInsert("w")':       ['<a-w>'],
  \ 'PrtCurStart()':        ['<c-a>'],
  \ 'PrtCurEnd()':          ['<c-e>'],
  \ 'PrtCurLeft()':         ['<a-h>', '<left>', '<c-^>'],
  \ 'PrtCurRight()':        ['<a-l>', '<right>'],
  \ 'PrtClearCache()':      ['<F5>'],
  \ 'PrtDeleteEnt()':       ['<F7>'],
  \ 'CreateNewFile()':      ['<c-y>'],
  \ 'MarkToOpen()':         ['<c-v>', '<a-v>'],
  \ 'OpenMulti()':          ['<c-o>', '<a-o>'],
  \ 'PrtExit()':            ['<esc>', '<c-c>', '<c-g>'],
  \ }

nnoremap <silent> <A-w> :<C-U>CtrlPBuffer<Cr>
vnoremap <silent> <A-w> :<C-U>call <SID>runCtrlPInVisualMode(6)<Cr>
inoremap <silent> <A-w> <C-O>:CtrlPBuffer<Cr>

nnoremap <silent> <A-e> :<C-U>MyProjectCtrlP<Cr>
vnoremap <silent> <A-e> :<C-U>call <SID>runCtrlPInVisualMode(5)<Cr>
inoremap <silent> <A-e> <C-O>:MyProjectCtrlP<Cr>

nnoremap <silent> <TAB>   :<C-U>CtrlPBufTag<Cr>
vnoremap <silent> <TAB>   :<C-U>call <SID>runCtrlPInVisualMode(1)<Cr>
nnoremap <silent> <S-TAB> :<C-U>CtrlPTag<Cr>
vnoremap <silent> <S-TAB> :<C-U>call <SID>runCtrlPInVisualMode(2)<Cr>
nnoremap <silent> <Leader>uq      :<C-U>CtrlPQuickfix<Cr>
vnoremap <silent> <Leader>uq      :<C-U>call <SID>runCtrlPInVisualMode(3)<Cr>
nnoremap <silent> <Leader>un       :<C-U>CtrlPUndo<Cr>
nnoremap <silent> <Leader>u`       :<C-U>CtrlPCmdline<Cr>
nnoremap <silent> <Leader>uy       :<C-U>CtrlPYankring<Cr>
nnoremap <silent> <Leader>um       :<C-U>CtrlPMenu<Cr>
nnoremap <silent> <BS>    :<C-U>CtrlPLine <C-R>=bufname('%')<Cr><Cr>
vnoremap <silent> <BS>    :<C-U>call <SID>runCtrlPInVisualMode(4)<Cr>

function! s:runCtrlPInVisualMode(mode)
  try
    let default_input_save = get(g:, 'ctrlp_default_input', '')
    let g:ctrlp_default_input = getreg('*')
    if a:mode == 1
      CtrlPBufTag
    elseif a:mode == 2
      CtrlPTag
    elseif a:mode == 3
      CtrlPQuickfix
    elseif a:mode == 4
      execute "CtrlPLine ".bufname('%')
    elseif a:mode == 5
      MyProjectCtrlP
    elseif a:mode == 6
      CtrlPBuffer
    endif
  finally
    if exists('default_input_save')
      let g:ctrlp_default_input = default_input_save
    endif
  endtry
endfu
