let g:unite_force_overwrite_statusline = 1
let g:unite_no_default_keymappings = 0
let g:unite_source_history_yank_enable = 1
let g:unite_matcher_fuzzy_max_input_length = 100
let g:unite_source_rec_max_cache_files = 100000
" let g:unite_source_file_rec_max_cache_files = 20000
" let g:unite_source_rec_async_command = 'ack -f --nofilter'
" let g:unite_source_rec_async_command =
            " \ 'ag --nocolor --nogroup --skip-vcs-ignores --hidden -g ""'
let g:unite_source_menu_menus = {}

" Deprecated source
" nnoremap <silent> [unite]y :<C-u>Unite history/yank<CR>

nnoremap <silent> <unique> <Plug>MyUniteFileSearch
      \ :<C-U>Unite -start-insert -no-split -no-resize -horizontal -resume line<Cr>
vnoremap <silent> <unique> <Plug>MyUniteFileSearch
      \ :<C-U>Unite -input=<C-R>=escape(@*, ' \')<CR> -start-insert -no-split -no-resize -horizontal -resume line<Cr>

" Default {{{
call unite#filters#matcher_default#use(['matcher_fuzzy'])
call unite#filters#sorter_default#use(['sorter_selecta'])
" call unite#custom#source('buffer,file_rec,line,file_rec/git', 'max_candidates', 0)
" call unite#custom#source('file_rec/async', 'max_candidates', 1000)
call unite#custom#source('buffer,file,file_rec,file_rec/git,line',
  \ 'sorters', ['sorter_selecta'])
call unite#custom#source('file_rec/async',
  \ 'sorters', ['sorter_selecta'])
  " \ 'sorters', ['sorter_nothing'])
call unite#custom#source('file_rec,file_rec/async,file_rec/git',
  \ 'ignore_pattern',
  \ '^\(\..*\|.*\.\(py[odc]\|exe\|dll\|so\|lib\|a\|obj\|o\|d\|pdf\|7z\|zip\|tar.*\|out\|log\|db\|ttf\)\)$')
 call unite#custom#source('file,buffer,file_rec,file_rec/git,line',
  \ 'matchers', 'matcher_fuzzy')
 call unite#custom#source('file_rec/async',
  \ 'matchers', 'matcher_fuzzy')
" Default }}}

nnoremap <silent> <A-s> :<C-u>Unite -no-split -no-resize -wrap -max-multi-lines=2 buffer<Cr>
vnoremap <silent> <A-s> :<C-u>Unite -input=<C-R>=escape(@*, ' \')<CR> -no-split -no-resize -wrap -max-multi-lines=2 buffer<Cr>
inoremap <silent> <A-s> <C-o>:Unite -no-split -no-resize -wrap -max-multi-lines=2 buffer<Cr>
" nnoremap <silent> <A-s> :<C-u>Unite -no-split -no-resize -wrap -max-multi-lines=2  -start-insert buffer<Cr>
" vnoremap <silent> <A-s> :<C-u>Unite -input=<C-R>=escape(@*, ' \')<CR> -no-split -no-resize -wrap -max-multi-lines=2  -start-insert buffer<Cr>
" inoremap <silent> <A-s> <C-o>:Unite -no-split -no-resize -wrap -max-multi-lines=2  -start-insert buffer<Cr>

call unite#custom#profile('source/source', 'context', {
  \ 'buffer_name' : 'Unite Sources',
  \ 'start_insert' : 1,
  \ 'resume' : 1,
  \ 'input' : ''
  \ })


" Buffer Profile {{{
call unite#custom#profile('buffer', 'context', {
\   'prompt' : '>>>',
\ })
" Buffer Profile }}}
" Buffer Converter {{{
let s:converter = {
\   "name" : "BufferConverter",
\}

function! s:converter.filter(candidates, context)
    for candidate in a:candidates
        let path = fnamemodify(bufname(candidate.action__buffer_nr), ':p')
        let filename = fnamemodify(path, ':t')
        let candidate.abbr = "[".filename."] ".path
    endfor
    return a:candidates
endfunction

call unite#define_filter(s:converter)
unlet s:converter
call unite#custom#source('buffer', 'converters', 'BufferConverter')
" Buffer Converter }}}

" nnoremap <silent> <A-e> :<C-u>Unite -no-split -no-resize -wrap -max-multi-lines=2 file_rec/async<Cr>
" vnoremap <silent> <A-e> :<C-u>Unite -input=<C-R>=escape(@*, ' \')<CR> -no-split -no-resize -wrap -max-multi-lines=2 file_rec/async:!<Cr>
" inoremap <silent> <A-e> <C-o>:Unite -no-split -no-resize -wrap -max-multi-lines=2 file_rec/async<Cr>
" nnoremap <silent> <A-d> :<C-u>Unite -no-split -no-resize -wrap -max-multi-lines=2 -start-insert  file_rec/async<Cr>
" vnoremap <silent> <A-d> :<C-u>Unite -input=<C-R>=escape(@*, ' \')<CR> -no-split -no-resize -wrap -max-multi-lines=2 -start-insert  file_rec/async:!<Cr>
" inoremap <silent> <A-d> <C-o>:Unite -no-split -no-resize -wrap -max-multi-lines=2 -start-insert  file_rec/async<Cr>

nnoremap <silent> <A-r> :<C-u>Unite -no-split -no-resize -wrap -max-multi-lines=2 file_rec/git:--cached:--others:--exclude-standard<Cr>
vnoremap <silent> <A-r> :<C-u>Unite -input=<C-R>=escape(@*, ' \')<CR> -no-split -no-resize -wrap -max-multi-lines=2 file_rec/git:--cached:--others:--exclude-standard<Cr>
inoremap <silent> <A-r> <C-o>:Unite -no-split -no-resize -wrap -max-multi-lines=2 file_rec/git:--cached:--others:--exclude-standard<Cr>
nnoremap <silent> <A-f> :<C-u>Unite -no-split -no-resize -wrap -max-multi-lines=2 -start-insert  file_rec/git<Cr>
vnoremap <silent> <A-f> :<C-u>Unite -input=<C-R>=escape(@*, ' \')<CR> -no-split -no-resize -wrap -max-multi-lines=2 -start-insert  file_rec/git<Cr>
inoremap <silent> <A-f> <C-o>:Unite -no-split -no-resize -wrap -max-multi-lines=2 -start-insert  file_rec/git<Cr>

" File_Rec Converter {{{
let s:converter = {
\   "name" : "FileRecConverter",
\}

function! s:converter.filter(candidates, context)
   for candidate in a:candidates
     let path = get(candidate, 'action__path', candidate.word)
     let abbr = fnamemodify(path, ':p:t')
     if has_key(candidate, 'path_only')
       let path = candidate.path_only
     else
       let path = unite#util#substitute_path_separator(
             \ fnamemodify(path, ':~:.'))
     endif
     " if path ==# '.'
     "   let path = ''
     " endif
     let candidate.abbr = "[".abbr."] ".path
   endfor
   return a:candidates
endfunction

call unite#define_filter(s:converter)
unlet s:converter
call unite#custom#source('file_rec,file_rec/async,file_rec/git', 'converters', 'FileRecConverter')
" File_Rec Converter }}}

" File_Rec Hook {{{
" let s:fileRecHook = {
" \   "name" : "FileRecConverter",
" \}
" function! s:fileRecHook.on_syntax(args, context) "{{{
"   syntax match uniteSource__Buffer_Name /[^/ \[\]]\+\s/
"         \ contained containedin=uniteSource__Buffer
"   highlight default link uniteSource__Buffer_Name Function
"   syntax match uniteSource__Buffer_Prefix /\d\+\s\%(\S\+\)\?/
"         \ contained containedin=uniteSource__Buffer
"   highlight default link uniteSource__Buffer_Prefix Constant
"   syntax match uniteSource__Buffer_Info /\[.\{-}\] /
"         \ contained containedin=uniteSource__Buffer
"   highlight default link uniteSource__Buffer_Info PreProc
"   syntax match uniteSource__Buffer_Modified /\[.\{-}+\]/
"         \ contained containedin=uniteSource__Buffer
"   highlight default link uniteSource__Buffer_Modified Statement
"   syntax match uniteSource__Buffer_NoFile /\[nofile\]/
"         \ contained containedin=uniteSource__Buffer
"   highlight default link uniteSource__Buffer_NoFile Function
"   syntax match uniteSource__Buffer_Time /(.\{-}) /
"         \ contained containedin=uniteSource__Buffer
"   highlight default link uniteSource__Buffer_Time Statement
" endfunction"}}}

" call unite#define_filter(s:fileRecHook)
" unlet s:fileRecHook
" call unite#custom#source('file_rec,file_rec/async,file_rec/git', 'converters', 'FileRecConverter')
" File_Rec Hook }}}

" Default
autocmd FileType unite call s:unite_my_settings()
function! s:unite_my_settings()

  if exists("b:unite")
        \ && exists("b:unite.source_names")
        \ && len(b:unite.source_names) > 0
        \ && b:unite.source_names[0] == 'tag'
    nmap <buffer> <Tab>     <Plug>(unite_exit)
    imap <buffer> <Tab>     <Plug>(unite_exit)
  else
    imap <buffer> <Tab>   <Plug>(unite_select_next_line)
  endif

  nmap <buffer> <ESC>      <Plug>(unite_exit)

	imap <buffer> <C-w>     <Plug>(unite_delete_backward_path)
	imap <buffer> <C-u>     <Plug>(unite_delete_backward_path)
	nmap <buffer> <C-q>     <Plug>(unite_exit)
	imap <buffer> <C-q>     <Plug>(unite_exit)
	nmap <buffer> <A-q>     <Plug>(unite_exit)
	imap <buffer> <A-q>     <Plug>(unite_exit)

  imap <buffer><expr> j unite#smart_map('j', '')
  imap <buffer> <S-TAB>   <Plug>(unite_select_previous_line)
  imap <buffer> <C-w>   <Plug>(unite_delete_backward_path)
  imap <buffer> '       <Plug>(unite_quick_match_default_action)
  nmap <buffer> '       <Plug>(unite_quick_match_default_action)
  imap <buffer><expr> x
        \ unite#smart_map('x', "\<Plug>(unite_quick_match_choose_action)")
  nmap <buffer> x       <Plug>(unite_quick_match_choose_action)
  nmap <buffer> <A-z>   <Plug>(unite_toggle_transpose_window)
  imap <buffer> <A-z>   <Plug>(unite_toggle_transpose_window)
  nmap <buffer> <A-p>   <Plug>(unite_toggle_auto_preview)
  nmap <buffer> <p>     <Plug>(unite_toggle_auto_preview)
  imap <buffer> <A-p>   <Plug>(unite_toggle_auto_preview)
  nmap <buffer> <A-r>   <Plug>(unite_narrowing_input_history)
  imap <buffer> <A-r>   <Plug>(unite_narrowing_input_history)
  nnoremap <silent><buffer><expr> l
        \ unite#smart_map('l', unite#do_action('default'))

  " let unite = unite#get_current_unite()

  nnoremap <silent><buffer><expr> f unite#do_action('find_in_files')
  nnoremap <silent><buffer><expr> F unite#do_action('replace_in_files')

  nnoremap <silent><buffer><expr> cd     unite#do_action('lcd')
  nnoremap <buffer><expr> S      unite#mappings#set_current_filters(
        \ empty(unite#mappings#get_current_filters()) ?
        \ ['sorter_reverse'] : [])

  nmap <silent><buffer><expr> <A-s>     unite#do_action('split')
  imap <silent><buffer><expr> <A-s>     unite#do_action('split')
  nmap <silent><buffer><expr> <A-v>     unite#do_action('vsplit')
  imap <silent><buffer><expr> <A-v>     unite#do_action('vsplit')

	nmap <buffer> <C-z>     <Plug>(unite_toggle_transpose_window)
	imap <buffer> <C-z>     <Plug>(unite_toggle_transpose_window)
endfunction

" `find_in_files` action {{{
let find_in_files_action = {
\ 'is_selectable' : 1,
\ }
function! find_in_files_action.func(candidates)
  let pattern = input("/")
  if pattern =~ "^$"
    return
  endif
  let files = "["
  for candidate in a:candidates
    if files != "["
      let files .= ","
    endif
    let files .= "\""
    let file = candidate.word
    let file = substitute(file, "\\", "\\\\", "g")
    let file = substitute(file, '"', '\\"', "g")
    let files .= file
    let files .= "\""
  endfor
  let files .= "]"
  let pattern = substitute(pattern, "\\", "\\\\", "g")
  let pattern = substitute(pattern, '"', '\\"', "g")
  let command = "py import FindAndReplaceInterface; FindAndReplaceInterface.find(\"".pattern."\",".files.")"
  execute command
endfunction
call unite#custom#action('openable', 'find_in_files', find_in_files_action)
unlet find_in_files_action
" `find_in_files` action }}}

" `replace_in_files` action {{{
let replace_in_files_action = {
\ 'is_selectable' : 1,
\ }
function! replace_in_files_action.func(candidates)
  let pattern = input("/")
  if pattern =~ "^$"
    return
  endif
  let files = "["
  for candidate in a:candidates
    if files != "["
      let files .= ","
    endif
    let files .= "\""
    let file = candidate.word
    let file = substitute(file, "\\", "\\\\", "g")
    let file = substitute(file, '"', '\\"', "g")
    let files .= file
    let files .= "\""
  endfor
  let files .= "]"
  let pattern = substitute(pattern, "\\", "\\\\", "g")
  let pattern = substitute(pattern, '"', '\\"', "g")
  let command = "py import FindAndReplaceInterface; FindAndReplaceInterface.replace(\"".pattern."\",".files.")"
  execute command
endfunction
call unite#custom#action('openable', 'replace_in_files', replace_in_files_action)
unlet replace_in_files_action
" `replace_in_files` action }}}

let s:currentDir = expand("<sfile>:h")
execute "source ".s:currentDir."/GitMenu.vim"
