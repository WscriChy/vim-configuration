" let g:syntastic_debug = 1
" If enabled, syntastic will do syntax checks when buffers are first loaded as
" well as on saving >
let g:syntastic_check_on_open = 1
let g:syntastic_mode_map = { 'mode': 'active',
			\ 'active_filetypes': [],
			\ 'passive_filetypes': ['c','cpp','objc','objcpp','python','cs'] }
" let g:syntastic_cpp_check_header = 1
" let g:syntastic_cpp_auto_refresh_includes = 1
" let g:syntastic_python_checkers = ['pyflakes']
let g:syntastic_tex_checkers = ['chktex']
