if exists('loaded_scratch') || &cp
    finish
endif
let loaded_scratch=1

function! s:ScratchBufferOpen(new_win, ...)
    let split_win = a:new_win

    " If the current buffer is modified then open the scratch buffer in a new
    " window
    if !split_win && &modified
        let split_win = 1
    endif

    let name = ''
    if a:0 != 0
      let name = join(a:000, ' ')
    else
      let name = "✵ Scratch ✵"
    endif
    " Check whether the scratch buffer is already created
    let temp_name = fnamemodify(name, ':p')
    while !empty(glob(temp_name))
      let name = name."x"
      let temp_name = temp_name."x"
    endwhile
    let name = escape(name, '"\|\ \')
    let scr_bufnum = bufnr('^'.name.'$')
    if scr_bufnum == -1
        let eventignore = &eventignore
        let &eventignore = 'all'
        if split_win
            exe "new " . name
        else
            exe "edit " . name
        endif
        let &eventignore = eventignore
        call s:ScratchMarkBuffer()
    else
        " Scratch buffer is already created. Check whether it is open
        " in one of the windows
        let scr_winnum = bufwinnr(scr_bufnum)
        if scr_winnum != -1
            " Jump to the window which has the scratch buffer if we are not
            " already in that window
            if winnr() != scr_winnum
                exe scr_winnum . "wincmd w"
            endif
        else
            " Create a new scratch buffer
            if split_win
                exe "split +buffer" . scr_bufnum
            else
                exe "buffer " . scr_bufnum
            endif
        endif
        call s:ScratchMarkBuffer()
    endif
endfunction

" ScratchMarkBuffer
" Mark a buffer as scratch
function! s:ScratchMarkBuffer()
    setlocal buftype=nofile
    setlocal bufhidden=wipe
    setlocal noswapfile
    setlocal buflisted
    setlocal filetype=scratch
endfunction

" Command to edit the scratch buffer in the current window
command! -nargs=? Scratch call s:ScratchBufferOpen(0, <f-args>)
" Command to open the scratch buffer in a new split window
command! -nargs=? Sscratch call s:ScratchBufferOpen(1, <f-args>)

function! s:Invoke(...)
  if a:0 == 0
    return
  endif
  let cmd = join(a:000, ' ')
  redir => lines
  silent execute cmd
  redir END
  if empty(lines)
    echoerr "No output captured"
    return
  endif
  silent put=lines
  if cmd =~ 'set runtimepath'
     silent %substitute/, /\r            /ge
  endif
  " silent %substitute/\(\%^\_s*\|\_s*\%$\)//e
endfunction

function! s:InvokeScratch(...)
  if a:0 == 0
    return
  endif
  let cmd = join(a:000, ' ')
  redir => lines
  silent execute cmd
  redir END
  if empty(lines)
    echoerr "No output captured"
    return
  endif
  silent call s:ScratchBufferOpen(0, cmd)
  silent put=lines
  if cmd =~ 'set runtimepath'
     silent %substitute/, /\r            /ge
  endif
  " silent %substitute/\(\%^\_s*\|\_s*\%$\)//e
endfunction

function! s:InvokeShell(...)
  if a:0 == 0
    return
  endif
  let cmd = join(a:000, ' ')
  silent put=system(cmd)
endfunction

function! s:InvokeShellScratch(...)
  if a:0 == 0
    return
  endif
  let cmd = join(a:000, ' ')
  silent call s:ScratchBufferOpen(0, cmd)
  silent put=system(cmd)
endfunction

function! s:Write(...)
  let name = join(a:000, ' ')
  echom name
  if name == ''
    let name = expand('%:p')
  endif
    setlocal buftype=
    setlocal bufhidden=hide
    setlocal swapfile
    setlocal buflisted
  while !empty(glob(name))
    let name = name."x"
  endwhile
  let name = escape(name, '"\|\ \')
  " let buf_nr = bufnr('^'.name.'$')
  execute "saveas ".name
endfunction

command! -nargs=? Scriptnames call <SID>InvokeScratch('scriptnames', <f-args>)
command! -nargs=? Message call <SID>InvokeScratch('message', <f-args>)
command! -nargs=0 VimRuntimePath call <SID>InvokeScratch('echo $VIMRUNTIME')
command! -nargs=0 RuntimePath call <SID>InvokeScratch('set runtimepath')
command! -nargs=+ InvScratch call <SID>InvokeScratch(<f-args>)
command! -nargs=+ Inv call <SID>Invoke(<f-args>)
command! -nargs=+ ShellInv call <SID>InvokeShell(<f-args>)
command! -nargs=+ ShellInvScratch call <SID>InvokeShellScratch(<f-args>)
command! -nargs=* Write call <SID>Write(<f-args>)
