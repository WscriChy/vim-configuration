#!/usr/bin/env python -OO
#encoding: utf-8

from Configurator import *

def setKeyBindings():
  map("<Leader>a", ":CFileSwitchSwitch<Cr>",
      Normal, Visual, Select, NoRemap, Silent, Buffer)

hook("FileType c,cpp", setKeyBindings)
