if exists('g:MWM#Library#autoload')
  finish
endif

if !has('gui_running')
  finish
endif

if !has('win32') && !has('win32unix')
  finish
endif

let g:MWM#Library#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ==============================================================================
" Public {{{
" ==============================================================================
function! MWM#Library#GetPath()
  if     MWM#Library#Local#Exists()
    return MWM#Library#Local#GetPath()
  elseif MWM#Library#Global#Exists()
    return MWM#Library#Global#GetPath()
  else
    return ''
  endif
endfunction
" ------------------------------------------------------------------------------
function! MWM#Library#GetAbsolutePath()
  if     MWM#Library#Local#Exists()
    return MWM#Library#Local#GetAbsolutePath()
  elseif MWM#Library#Global#Exists()
    return MWM#Library#Global#GetAbsolutePath()
  else
    return ''
  endif
endfunction
" ------------------------------------------------------------------------------
function! MWM#Library#Exists()
  return
  \ MWM#Library#Local#Exists()
  \ ||
  \ MWM#Library#Global#Exists()
endfunction
" ------------------------------------------------------------------------------
function! MWM#Library#Call(function, argument)
  try
    return MWM#Library#Local#Call(a:function, a:argument)
  catch /call/
    throw v:exception
  catch
    let l:exception = v:exception
  endtry
  try
    return MWM#Library#Global#Call(a:function, a:argument)
  catch /call/
    throw v:exception
  catch
    let l:exception .= ' ' . v:exception
  endtry
  throw l:exception
endfunction
" ------------------------------------------------------------------------------
function! MWM#Library#Check()
  if     MWM#Library#Local#Exists()
    "echomsg
    "\ "MWM:"                                                                   .
    "\ ' '                                                                      .
    "\ "Using local library:"                                                   .
    "\ ' '                                                                      .
    "\ '"'                                                                      .
    "\ MWM#Library#Local#GetPath()                                              .
    "\ '"'                                                                      .
    "\ '.'
  elseif MWM#Library#Global#Exists()
    "echomsg
    "\ "MWM:"                                                                   .
    "\ ' '                                                                      .
    "\ "Using global library:"                                                  .
    "\ ' '                                                                      .
    "\ '"'                                                                      .
    "\ MWM#Library#Global#GetPath()                                             .
    "\ '"'                                                                      .
    "\ '.'
  else
    echohl WarningMsg
    echomsg
    \ "MWM: Warning:"                                                          .
    \ ' '                                                                      .
    \ "Cannot find local library:"                                             .
    \ ' '                                                                      .
    \ '"'                                                                      .
    \ MWM#Library#Local#GetPath()                                              .
    \ '"'                                                                      .
    \ '.'                                                                      .
    \ ' '                                                                      .
    \ "Cannot find global library:"                                            .
    \ ' '                                                                      .
    \ '"'                                                                      .
    \ MWM#Library#Global#GetPath()                                             .
    \ '"'                                                                      .
    \ '.'
    echohl None
  endif
endfunction
" ==============================================================================
" }}} Public
" ==============================================================================
" }}} Functions

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
