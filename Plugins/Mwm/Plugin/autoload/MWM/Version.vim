if exists('g:MWM#Version#autoload')
  finish
endif

let g:MWM#Version#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ==============================================================================
" Public {{{
" ==============================================================================
function! MWM#Version#GetMajor()
  return s:major
endfunction
" ------------------------------------------------------------------------------
function! MWM#Version#GetMinor()
  return s:minor
endfunction
" ------------------------------------------------------------------------------
function! MWM#Version#GetPatch()
  return s:patch
endfunction
" ------------------------------------------------------------------------------
function! MWM#Version#ToString()
  return s:major . '.' . s:minor . '.' . s:patch
endfunction
" ==============================================================================
" }}} Public
" ==============================================================================
" }}} Functions

" Defaults {{{
" ==============================================================================
let s:major = 0
let s:minor = 0
let s:patch = 0
" ==============================================================================
" }}} Defaults

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
