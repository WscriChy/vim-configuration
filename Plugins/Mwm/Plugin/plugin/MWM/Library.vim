if exists('g:MWM#Library#plugin')
  finish
endif

if !has('gui_running')
  finish
endif

if !has('win32') && !has('win32unix')
  finish
endif

let g:MWM#Library#plugin = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Hooks {{{
" ==============================================================================
augroup MWM#Library#Check
  autocmd!
  autocmd VimEnter * call MWM#Library#Check()
augroup END
" ==============================================================================
" }}} Hooks

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
