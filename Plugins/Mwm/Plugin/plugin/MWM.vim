if exists('g:MWM#plugin')
  finish
endif

if !has('gui_running')
  finish
endif

if !has('win32') && !has('win32unix')
  finish
endif

let g:MWM#plugin = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Plugs {{{
" ==============================================================================
" Normal {{{
" ==============================================================================
nnoremap <script> <silent> <unique> <Plug>MWM#Fullscreen
\ :call MWM#Fullscreen()<CR>

nnoremap <script> <silent> <unique> <Plug>MWM#ToggleFullscreen
\ :call MWM#ToggleFullscreen()<CR>

nnoremap <script> <silent> <unique> <Plug>MWM#Maximize
\ :call MWM#Maximize()<CR>

nnoremap <script> <silent> <unique> <Plug>MWM#ToggleMaximized
\ :call MWM#ToggleMaximized()<CR>

nnoremap <script> <silent> <unique> <Plug>MWM#Restore
\ :call MWM#Restore()<CR>
" ==============================================================================
" }}} Normal
" ==============================================================================
" }}} Plugs

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ==============================================================================
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ==============================================================================
" }}} Modeline
