#!/usr/bin/env python -OO
#encoding: utf-8

import os
import re
import copy

import vim
from vim import buffers, options, current, command

_vim_bufname = vim.Function('bufname')
_vim_getbufvar = vim.Function('getbufvar')

class BufferToStringHandler:

  def __init__(self, width, max_size):
    self.width = width
    self.string_list = []
    self.string = ""
    self.i = 1
    self.max_size = max_size

  def __call__(self, buf_nr):
    if self.i > self.max_size:
     return False
    fileName = _vim_bufname(buf_nr)
    if fileName == '':
      fileName = ' ♕ '
    else:
      if _vim_getbufvar(buf_nr, '&buftype') == '':
        (head, tail) = os.path.split(fileName)
        fileName = os.path.join(os.path.basename(head), tail)
    entry = "[" + str(self.i) + ":" + fileName + "]"
    if (len(self.string) + len(entry) + 2) < self.width:
      if self.string == "": self.string = entry
      else: self.string += " " + entry
    else:
      if self.string != '':
        self.string_list.append(self.string)
        self.string = ''
      else: self.string_list.append(entry)
    self.i = self.i + 1
    return True

  def getResults(self):
    if self.string != '': self.string_list.append(self.string)
    return self.string_list


class Buffers(object):

  def __init__(self):
    self.list = []
    self.max_size = 9

  def __getitem__(self, ii):
    return self.list[ii]

  def getMinIndex(self, index):
    if index < len(self.list):
      return index
    else:
      return len(self.list) - 1

  def getMostRecent(self):
    if len(self.list) > 1:
      return self.list[1]
    else:
      return self.list[0]

  def append(self, buf_num):
    index = 0
    for k in self.list:
      if k == buf_num:
        break
      index = index + 1
    if index != len(self.list):
      if index == self.getMinIndex(1):
        return False
      elif index == 0:
        self.list[0] = self.list[1]
        self.list[1] = buf_num
        return True
      else:
        temp_data = self.list[0]
        self.list[0] = self.list[1]
        self.list[1] = buf_num
        self.list.pop(index)
        self.list.insert(2, temp_data)
        return True

    if index > self.max_size:
      del self.list[self.max_size - 1:]

    if len(self.list) > 1:
      temp_data = self.list[0]
      self.list[0] = self.list[1]
      self.list[1] = buf_num
      self.list.insert(2, temp_data)
    else:
      self.list.append(buf_num)
    return True

  def remove(self, buf_num):
    index = 0
    for k in self.list:
      if k == buf_num:
        break
      index = index + 1
    if index == len(self.list):
      return False
    if index == 0 and len(self.list) > 2:
      self.list[0] = self.list[2]
      self.list.pop(2)
    elif index == 0:
      self.list.pop(0)
    elif index == 1 and len(self.list) > 2:
      self.list[1] = self.list[0]
      self.list[0] = self.list[2]
      self.list.pop(2)
      self.list.pop(index)
    else:
      self.list.pop(index)
    return True

  def getLine(self, width):
    buffer_to_string_handler = BufferToStringHandler(width, self.max_size)
    for i in self.list:
      if not buffer_to_string_handler(i):
        break
    return buffer_to_string_handler.getResults()
