#!/usr/bin/env python -OO
# encoding: utf-8

from bisect import bisect_left

class Views(object):

  def __init__(self):
    self._list = []

  def __len__(self):
    return len(self._list)

  def __contains__(self, key):
    return key in self._list

  def __getitem__(self, ii):
    return self._list[ii]

  def __delitem__(self, ii):
    del self._list[ii]

  def __setitem__(self, ii, val):
    return self._list[ii]

  def __str__(self):
    return self.__repr__()

  def __repr__(self):
    return """<Views {0}>""".format(str(self._list))

  def __iter__(self):
    for elem in self._list:
      yield elem

  def add(self, key):
    if key not in self._list:
      self._list.append(key)
      sorted(self._list)

  def remove(self, key):
    self._list.remove(key)

  def pop(self, index):
    self._list.pop(index)

  def find(self, key):
    i = bisect_left(self._list, key)
    if i != len(self._list) and self._list[i] == key:
      return self._list[i]
    return None
