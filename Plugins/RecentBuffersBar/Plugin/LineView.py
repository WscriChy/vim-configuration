#!/usr/bin/env python -OO
# encoding: utf-8

from Configurator import *

import sys
import re
import vim
from vim import tabpages, windows, buffers, options, current, command
from Buffers import Buffers

def _printExceptionToVim():
  type_, value_, traceback_ = sys.exc_info()
  vim.command('echomsg "{0}"'.format(
    re.sub("\"",
      "\\\\\"",
      "".join(traceback.format_exception(
        type_,
        value_,
        traceback_)))))

_vimWinNr = vim.Function('winnr')

class WindowState(object):
  def __init__(self, window = None):
    if window is None:
      window = current.window
    self.window = window
    self.previousWindow = _vimWinNr('#')

  def switch(self, window):
    current.window = window

  def restore(self):
    command("{0}wincmd w".format(self.previousWindow))
    command("{0}wincmd w".format(self.window.number))
    # current.window = self.window

class TabpageState(object):
  def __init__(self, tabpage = None):
    if tabpage is None:
      tabpage = current.tabpage
    self.tabpage = tabpage

  def switch(self, tabpage):
    current.tabpage = tabpage
    pass

  def restore(self):
    current.tabpage = self.tabpage
    pass

class LineView(object):
  def __init__(self, tabpage):
    self.buffers = Buffers()
    self.bufferName = '✯ BuffersLine{0} ✯'.format(tabpage.number)
    self.window = None
    self.buffer = None
    self.tabpage = tabpage
    self.vimBufName = vim.Function('bufname')
    self.fixPrevWindow = -1

  def __eq__(self, other):
    if not self.isValid(): return False
    if isinstance(other, LineView):
      return self.tabpage.number == other.tabpage.number
    else:
      return self.tabpage.number == other

  def __ne__(self, other):
    return not self.__eq__(other)

  def __lt__(self, other):
    if not self.isValid(): return False
    if isinstance(other, LineView):
      return self.tabpage.number < other.tabpage.number
    else:
      return self.tabpage.number < other

  def __le__(self, other):
    return self.__eq__(other) or self.__le__(other)

  def __gt__(self, other):
    return not self.__lt__(other)

  def __ge__(self, other):
    return not self.__le__(other)

  def __str__(self):
    return self.__repr__()

  def __repr__(self):
    return "View: tabpage({0}), buffer({1})".format(
        self.tabpage.number, self.buffer.number)

  def isValid(self):
    return self.tabpage.valid

  def release(self):
    if not self.buffer is None:
      if self.buffer.valid:
        command("bdelete {0}".format(self.buffer.number))

  def _isCreated(self):
    if self.window is None: return False
    if self.window in self.tabpage.windows: return True
    return False

  def _createWindow(self, eventignore):
    command("silent! keepjumps keepalt botright split +resize\\ 1"
        "|setlocal\\ winfixheight {bufferName}".format(
      bufferName = re.sub(r' ', r'\\ ', self.bufferName)))

    self.window = current.tabpage.window
    # self.window.options['winfixheight'] = True
    self.window.options['winfixwidth'] = True
    self.window.options['foldcolumn'] = 0
    self.window.options['number'] = False
    self.window.options['wrap'] = False

    # self.window.height = 1

    self.buffer = current.buffer
    self.buffer.options['buflisted'] = False
    self.buffer.options['buftype'] = 'nofile'
    self.buffer.options['bufhidden'] = 'delete'
    self.buffer.options['swapfile'] = False
    self.buffer.options['formatoptions'] = ''
    self.buffer.options['matchpairs'] = ''
    self.buffer.options['readonly'] = False
    self.buffer.options['modifiable'] = False

    options['eventignore'] = eventignore
    vim.command("set filetype=BuffersLine")
    options['eventignore'] = 'all'
    pass

  def releaseAndCreate(self):
    eventignore = options['eventignore']
    options['eventignore'] = 'all'

    tabpageState = TabpageState()
    windowState = WindowState()
    tabpageState.switch(self.tabpage)

    self.release()
    self._createWindow(eventignore)
    self.setLine()
    self.resize()

    tabpageState.restore()
    windowState.restore()
    options['eventignore'] = eventignore

  def create(self):
    if self._isCreated():
      return

    eventignore = options['eventignore']
    options['eventignore'] = 'all'
    tabpageState = TabpageState()
    windowState = WindowState()
    tabpageState.switch(self.tabpage)

    self._createWindow(eventignore)
    self.setLine()
    self.resize()

    tabpageState.restore()
    windowState.restore()
    options['eventignore'] = eventignore

  def resize(self):
    # print("Resize")
    if not self._isCreated():
      return
    n = len(self.buffer)
    if vim.bindeval('winheight({0})'.format(self.window.number)) != n:
      self.window.height = n

  def closeOnNobuffers(self):
    if len(self.buffers.list) == 0:
      command("qall")
      return True
    return False

  def splitOnLastWindow(self):
    # print("Split")
    if not self._isCreated():
      self.create()
    # Check windows length
    if len(self.tabpage.windows) == 1 and self.tabpage.window == self.window:
      pring(len(vim.buffers))
      # if len(vim.buffers) == 0:
      #   return True
      tabpageState = TabpageState()
      tabpageState.switch(self.tabpage)
      self.window.height = options['lines']

      if len(self.buffers.list) == 0:
        command("silent! keepalt topleft new")
        command("silent! VimFiler")
      else:
        recentBufferNumber = self.buffers.getMostRecent()

        eventignore = options['eventignore']
        options['eventignore'] = 'all'
        command("silent! keepalt topleft split {0}".format(
          vim.eval("bufname({0})".format(recentBufferNumber))))
        options['eventignore'] = eventignore

      tabpageState.restore()
      self.resize()
      return True
    return False

  def updatePosition(self):
    # print("UpdatePosition")
    currentBufferName = (vim.eval("bufname('%')"))
    if currentBufferName == '[Command Line]':
        return
    if not self._isCreated():
      self.create()
      return
    windowCount = len(self.tabpage.windows)
    if self.window.number < windowCount:
      self.releaseAndCreate()

  def setLine(self):
    # print("Set")
    if not self._isCreated():
      return
    lines = self.buffers.getLine(self.window.width)
    self.buffer.options['modifiable'] = True
    buf_len = len(self.buffer) - 1
    # if len(lines) < len(self.buffer):
    #   del self.buffer[len(lines):]
    eventignore = options['eventignore']
    options['eventignore'] = 'all'
    i = 0
    for line in lines:
      if buf_len < i:
        self.buffer.append(lines[i:])
        i = len(lines)
        break
      self.buffer[i] = line
      i = i + 1
    del self.buffer[i:]
    self.buffer.options['modifiable'] = False
    options['eventignore'] = eventignore
