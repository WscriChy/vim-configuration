if exists("b:current_syntax")
  call clearmatches()
endif

let s:cpo_save = &cpo
set cpo&vim

syn match BuffersLineOpenning   '\[[0-9]\+:'
syn match BuffersLineClosing    '\]'
syn match BuffersLineDelimiters '[/\\]'

syn match BuffersLineActiveBuffer '\[2:[^]]\+\]' contains=BuffersLineOpenning,BuffersLineClosing

hi! ActiveBuffer guifg=#FF6633 gui=bold

hi! BuffersLineDelimetersColor guifg=#FF6633 gui=bold

hi! def link BuffersLineOpenning BuffersLineDelimetersColor
hi! def link BuffersLineClosing BuffersLineDelimetersColor
hi! def link BuffersLineDelimiters BuffersLineDelimetersColor
hi! def link BuffersLineActiveBuffer ActiveBuffer

let b:current_syntax = "BuffersLine"

let &cpo = s:cpo_save
unlet s:cpo_save
