#!/usr/bin/env python
# encoding: utf-8

from Configurator import *

import vim
from vim import options, current, command, buffers

from LineView import LineView

from Views import Views

class BuffersLine(object):

  def __init__(self):
    self._views = Views()

  def create(self):
    vim.command("py import BuffersLine")
    self.laststatus = options['laststatus']
    options['laststatus'] = 0
    hook("WinEnter *", self._onWinEnter)
    on_buf_cmd = ("execute \"py "
                "BuffersLine.instance._onBuf(\".expand('<abuf>').\")\"")
    vim.command("autocmd BufEnter * {0}".format(on_buf_cmd))
    vim.command("autocmd FileType * {0}".format(on_buf_cmd))
    # hook("BufEnter *", self._onBuf)
    # hook("FileType *", self._onBuf)
    removeCmd = ("execute \"py "
                "BuffersLine.instance._remove(\".expand('<abuf>').\")\"")
    vim.command("autocmd BufDelete * {0}".format(removeCmd))
    hook("VimResized *", self._resize)

  def release(self):
    options['laststatus'] = self.laststatus

  def _getView(self, tabpage):
    deleteViews = []
    tabpageView = None
    for index, view in enumerate(self._views):
      if not view.isValid():
        deleteViews.append(index)
        continue
      if view.tabpage.number == tabpage.number:
        tabpageView = view
    for index in deleteViews:
      self._views.pop(index)
    if tabpageView is None:
      tabpageView = LineView(tabpage)
      self._views.add(tabpageView)
      tabpageView.create()
    return tabpageView

  def _onWinEnter(self):
    tabpage = current.tabpage
    view = self._getView(tabpage)
    view.create()

    if (view.fixPrevWindow != -1):
      eventignore = options['eventignore']
      options['eventignore'] = 'all'
      vim.command("{0}wincmd w".format(view.fixPrevWindow))
      options['eventignore'] = eventignore
      view.fixPrevWindow = -1
      view.resize()

    if tabpage.window.number == view.window.number:
      previous_window = long(vim.eval("winnr('#')"))
      if previous_window == view.window.number or previous_window <= 0:
        if len(tabpage.windows) > 1:
          previous_window = 1
        else:
          min_index = view.buffers.getMinIndex(1)
          if min_index < 0:
            vim.command("silent! keepjumps keepalt topleft new")
          else:
            vim.command("silent! keepjumps keepalt topleft new {}".format(vim.bindeval("bufname({})".format(view.buffers[min_index]))))
          return
      eventignore = options['eventignore']
      options['eventignore'] = 'all'
      vim.command("{0}wincmd w".format(previous_window))
      options['eventignore'] = eventignore
      return

  # Fixes previous window
  def _onQuickFixBufWinLeave(self):
    for tabpage in vim.tabpages:
      cur_win_num = tabpage.window.number
      buf_win_num = -1
      for win in tabpage.windows:
        if (win.buffer.options['buftype'] == 'quickfix'):
          buf_win_num = win.number
          view = self._getView(tabpage)
          view.fixPrevWindow = cur_win_num
          if buf_win_num != cur_win_num:
            eventignore = options['eventignore']
            options['eventignore'] = 'all'
            vim.command("{0}wincmd w".format(buf_win_num))
            options['eventignore'] = eventignore
            break
    return

  def _onBuf(self, buf_nr):
    buffer = vim.buffers[buf_nr]
    # print(buffer.name)
    # for view in self._views:
    tabpage = current.tabpage
    view = self._getView(tabpage)
    self._update(view, buffer)

  def _update(self, view, buffer):

    bufferType = buffer.options['buftype']

    if bufferType == 'quickfix':
      view.updatePosition()
      view.resize()
      vim.command("augroup RecentBuffersLinePluginQuickfixGroup")
      vim.command("au!")
      vim.command(
          "au BufWinLeave <buffer={0}> "
          "execute \"py import BuffersLine;"
          "BuffersLine.instance._onQuickFixBufWinLeave()\"".format(buffer.number))
      vim.command("augroup END")
      return

    bufferFileType = buffer.options['filetype']
    conditions = [
      buffer.options['buflisted'] == True,
      bufferType != 'nofile',
      bufferType != 'quickfix',
      bufferType != 'scratch',
      bufferType != 'help',
      bufferType != '' or buffer.name != '' or bufferFileType != '',
      ]

    for condition in conditions:
      if not condition:
        vim.command("augroup RecentBuffersLinePluginUnknownFileGroup")
        vim.command("au!")
        vim.command(
            "au BufWritePost <buffer={0}> "
            "execute \"py import BuffersLine;"
            "BuffersLine.instance._onBufWritePost({0})\"".format(buffer.number))
        vim.command("augroup END")
        view.resize()
        return

    if view.buffers.append(buffer.number):
      view.setLine()
      view.resize()
    else:
      view.resize()

  def _onBufWritePost(self, buf_nr):
    vim.command("au! RecentBuffersLinePluginUnknownFileGroup BufWritePost <buffer={0}>".format(buf_nr))
    for view in self._views:
      if not view.isValid():
        continue
      tabpage = view.tabpage
      for win in tabpage.windows:
        if win.buffer.number == buf_nr:
          self._update(view, win.buffer)
          break

  def _remove(self, bufferNumber):
    vim.command("au! RecentBuffersLinePluginUnknownFileGroup BufWritePost <buffer={0}>".format(bufferNumber))
    deleteViews = []
    for index, view in enumerate(self._views):
      if not view.isValid():
        deleteViews.append(index)
        continue
      if bufferNumber == view.buffer.number:
        deleteViews.append(index)
        break
      view.buffers.remove(bufferNumber)
      view.splitOnLastWindow()
      view.setLine()
      # view.updatePosition()
      view.resize()
    for index in deleteViews:
      self._views.pop(index)

  def _resize(self):
    for view in self._views:
      view.resize()

  def setBufferToCurrentWindow(self, index):
    tabpage = current.tabpage
    view = self._getView(tabpage)
    if len(view.buffers.list) <= index:
      return
    if current.buffer.number == view.buffer.number:
      view.splitOnLastWindow()
    vim.command("buffer {0}".format(view.buffers.list[index]))

  def quitCurrent(self):
    tabpage = current.tabpage
    view = self._getView(tabpage)
    view.release()

instance = BuffersLine()
