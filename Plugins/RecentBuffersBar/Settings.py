#!/usr/bin/env python
# encoding: utf-8

import sys
import os
import inspect

currentDir = os.path.dirname(os.path.realpath(
  os.path.abspath(inspect.getfile(inspect.currentframe()))))

pluginPath  = os.path.join(currentDir, 'Plugin')
sys.path.insert(0, pluginPath)

import vim
from Configurator import *
from BuffersLine import instance as buffersLine

buffersLine.create()

mappings = {
    "1" : "<A-1>",
    "2" : "<A-2>",
    "3" : "<A-3>",
    "4" : "<A-4>",
    "5" : "<A-5>",
    "6" : "<A-6>",
    "7" : "<A-7>",
    "8" : "<A-8>",
    "9" : "<A-9>",
    }

def mapChangeBufferAction(index, key):
  map(key,
      ":py import BuffersLine;"
      "BuffersLine.instance.setBufferToCurrentWindow({0})<Cr>".format(index),
      Normal, OperatorPending, NoRemap, Silent)
  map(key,
      ":<C-U>py import BuffersLine;"
      "BuffersLine.instance.setBufferToCurrentWindow({0})<Cr>".format(index),
      Visual, Select, NoRemap, Silent)
  map(key,
      "<C-O>:py import BuffersLine;"
      "BuffersLine.instance.setBufferToCurrentWindow({0})<Cr>".format(index),
      Insert, NoRemap, Silent)

for action, key in mappings.iteritems():
  if action == "1":
    mapChangeBufferAction(0, key)
  elif action == "2":
    mapChangeBufferAction(1, key)
  elif action == "3":
    mapChangeBufferAction(2, key)
  elif action == "4":
    mapChangeBufferAction(3, key)
  elif action == "5":
    mapChangeBufferAction(4, key)
  elif action == "6":
    mapChangeBufferAction(5, key)
  elif action == "7":
    mapChangeBufferAction(6, key)
  elif action == "8":
    mapChangeBufferAction(7, key)
  elif action == "9":
    mapChangeBufferAction(8, key)


vim.command("command! -nargs=? RblQuit py "
            "import BuffersLine;BuffersLine.instance.quitCurrent()<Cr>")
