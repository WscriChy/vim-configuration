
" The surrounding with space or no space
" depends on wich brackets you use
" [ means with space
" ] - without space
let g:surround_no_mappings = 1
" delete brackets
nmap <Leader>bd <Plug>Dsurround
" change brackets
nmap <Leader>bb <Plug>Csurround
" surround text by brackets ( used with smt like 'iw' )
nmap <Leader>bf <Plug>Ysurround
nmap <Leader>bF <Plug>YSurround
" surround the text in current line by brackets
nmap <Leader>bg <Plug>Yssurround
nmap <Leader>bG <Plug>YSsurround
" surround the current line by brackets ( add new lines )
nmap <Leader>bh <Plug>YSsurround
" surround the current selection by brackets
xmap <Leader>bb <Plug>VSurround
xmap <Leader>bB <Plug>VgSurround
imap <C-S>      <Plug>Isurround
imap <C-G>s     <Plug>Isurround
imap <C-G>S     <Plug>ISurround
