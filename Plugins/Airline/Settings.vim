let g:airline_powerline_fonts=1
let g:airline#extensions#eclim#enabled = 0
let g:airline#extensions#tabline#enabled = 0
let g:airline#extensions#syntastic#enabled = 1
let g:airline#extensions#tagbar#enabled = 1
let g:airline#extensions#hunks#enabled = 0
let g:airline#extensions#bufferline#enabled = 0
let g:airline#extensions#branch#enabled = 0


let g:airline_section_a = ''
let g:airline_section_y = ''
let g:airline_section_x = ''

let g:airline_section_warning = ''
