if exists('g:Uncrustify_Settings_autoload')
  finish
endif

call Uncrustify#Configuration#Global#SetPath(g:stuff."/Uncrustify/default.uncrustify")

let g:Uncrustify_Settings_autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

augroup Uncrustify_Settings_Map
  autocmd!
  autocmd FileType c,cpp,cs,d,java,objc,objcpp,javascript,javascript.jsx,typescript call <SID>Map()
augroup END

function! s:Map()
  nmap <buffer> <Leader>s <Plug>Uncrustify#FormatBuffer
  vmap <buffer> <Leader>s <Plug>Uncrustify#FormatRange
endfunction

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ------------------------------------------------------------------------------
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ------------------------------------------------------------------------------
" }}} Modeline
