if exists('g:Uncrustify#autoload')
  finish
endif

let g:Uncrustify#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

function! Uncrustify#CreateCommand(executable_path,
\                                  configuration_path,
\                                  language,
\                                  fragmented,
\                                  quiet)
  let l:command =
  \ shellescape(simplify(expand(a:executable_path)))                           .
  \ ' '                                                                        .
  \ '-c'                                                                       .
  \ ' '                                                                        .
  \ shellescape(simplify(expand(a:configuration_path)))                        .
  \ ' '                                                                        .
  \ '-l'                                                                       .
  \ ' '                                                                        .
  \ a:language
  if a:fragmented
    let l:command .=
    \ ' '                                                                      .
    \ '--frag'
  endif
  if a:quiet
    let l:command .=
    \ ' '                                                                      .
    \ '-q'
  else
    let l:command .=
    \ ' '                                                                      .
    \ '-L1-2'
  endif
  return l:command
endfunction

function! Uncrustify#CreateClangFormatterCommand(executable_path,
\                                  style)
  let l:command =
  \ shellescape(simplify(expand(a:executable_path)))                           .
  \ ' '                                                                        .
  \ '-style'                                                                       .
  \ ' '                                                                        .
  \ a:style
  return l:command
endfunction
" ------------------------------------------------------------------------------
" }}} Public

" Private {{{
" ------------------------------------------------------------------------------
function! s:Run(lines, output_buffer, from, to)
  if !Uncrustify#Executable#Exists()
    throw "Uncrustify: Error: Cannot find executable."
  endif
  if !Uncrustify#Configuration#Exists()
    throw "Uncrustify: Error: Cannot find configuration."
  endif
  let l:executable_path    = Uncrustify#Executable#GetPath()
  let l:configuration_path = Uncrustify#Configuration#GetAbsolutePath()
  let l:language           = Uncrustify#Languages#Get(&l:filetype)
  let l:fragmented         = (l:from == 1 && l:to == line('$')) ? 0 : 1
  " ----------------------------------------------------------------------------
  let l:error =
  \ s:Filter(
  \   a:lines,
  \   l:executable_path,
  \   l:configuration_path,
  \   l:language,
  \   l:fragmented,
  \   1
  \ )
  if l:error
    call
    \ s:Filter(
    \   a:lines,
    \   l:executable_path,
    \   l:configuration_path,
    \   l:language,
    \   l:fragmented,
    \   0
    \ )
  endif
  let l:new_lines = getbufline(a:output_buffer, 1, '$')
  if l:error
    throw
    \ "Uncrustify: Error:"                                                     .
    \ ' '                                                                      .
    \ join(l:new_lines, ' | ')
  endif
  return l:new_lines
endfunction

function! s:ClangFormatterFilter(lines,
\                  executable_path,
\                  style)
  silent! noautocmd %delete_
  silent! noautocmd 1put=a:lines
  silent! noautocmd 1delete_
  let &l:shellredir = '1>%s'
  silent! noautocmd execute
  \ '%'                                                                        .
  \ '!'                                                                        .
  \ Uncrustify#CreateClangFormatterCommand(
  \   a:executable_path,
  \   a:style
  \ )
  return v:shell_error
endfunction

function! s:Filter(lines,
\                  executable_path,
\                  configuration_path,
\                  language,
\                  fragmented,
\                  quiet)
  silent! noautocmd %delete_
  silent! noautocmd 1put=a:lines
  silent! noautocmd 1delete_
  if a:quiet
    let &l:shellredir = '1>%s'
  else
    let &l:shellredir = '2>%s'
  endif
  silent! noautocmd execute
  \ '%'                                                                        .
  \ '!'                                                                        .
  \ Uncrustify#CreateCommand(
  \   a:executable_path,
  \   a:configuration_path,
  \   a:language,
  \   a:fragmented,
  \   a:quiet
  \ )
  return v:shell_error
endfunction

function! s:Format(from, to, run_functor)
  let l:input_buffer  = bufnr('%')
  let l:output_buffer = bufnr('Uncrustify', 1)
  silent! noautocmd keepjumps execute "py import vim;views = [];".
  \  "currentWindow = vim.current.window\n".
  \  "for window in vim.windows:\n".
  \  "  if (window.buffer.number == ".l:input_buffer."):\n".
  \  "    vim.command('%swincmd w' % window.number)\n".
  \  "    views.append((window, vim.eval('winsaveview()')))"
  let l:shellredir    = &l:shellredir
  let l:lazyredraw    = &l:lazyredraw
  let l:syntax        = &l:syntax
  try
    let l:from = line(a:from)
    if l:from == 0
      let l:from = a:from
    endif
    let l:to = line(a:to)
    if l:to == 0
      let l:to = a:to
    endif
    let l:count = l:to - l:from + 1
    let l:range = l:from . ',' . l:to
    let l:lines = getbufline(l:input_buffer, from, to)
    let &l:lazyredraw = 1
    silent! noautocmd execute l:output_buffer . 'buffer!'
    let &l:swapfile = 0
    silent! keepjumps noautocmd %delete_
    silent! keepjumps noautocmd 1put=l:lines
    silent! keepjumps noautocmd 1delete_
    let l:new_lines = a:run_functor(l:lines, l:output_buffer, a:from, a:to)
    silent! keepjumps noautocmd execute l:input_buffer . 'buffer!'
    if l:lines != l:new_lines
      let l:lines = l:new_lines
      unlet l:new_lines
      " let l:undo_levels = &l:undolevels
      " let &l:undolevels = -1
      " silent! keepjumps noautocmd execute 'keepjumps let l:view=winsaveview() | keepjumps '.l:to.'put=l:lines | keepjumps '.l:range .'delete_ | winrestview(l:view)'
      silent! keepjumps noautocmd execute 'call setline('.l:from.', l:lines)'
      if len(l:lines) < l:count
        silent! keepjumps noautocmd execute (l:from + len(l:lines)).','.l:to.'delete_'
      endif
      " let &l:undolevels = l:undo_levels
    endif
  catch
    echo v:exception
  finally
    silent! noautocmd execute l:input_buffer  . 'buffer!'
    silent! noautocmd execute l:output_buffer . 'bwipeout!'
    silent! noautocmd keepjumps execute "".
    \  "py \n".
    \  "for window, view in views:\n".
    \  "  vim.command('%swincmd w' % window.number)\n".
    \  "  vim.eval('winrestview(%s)' % view)\n".
    \  "vim.command('%swincmd w' % currentWindow.number)"
    let &l:shellredir = l:shellredir
    let &l:lazyredraw = l:lazyredraw
    let &l:syntax     = l:syntax
  endtry
endfunction

function! s:ClangFormatterRun(lines, output_buffer, from, to)
  if !Uncrustify#Executable#ClangFormatterExists()
    throw "Clang Formatter: Error: Cannot find executable."
  endif
  let l:executable_path    = Uncrustify#Executable#GetClangFormatterPath()
  " let l:style = "llvm"
  " if b:formatter == 2
  let l:style = "file"
  " endif
  " ----------------------------------------------------------------------------
  let l:error =
  \ s:ClangFormatterFilter(
  \   a:lines,
  \   l:executable_path,
  \   l:style
  \ )
  if l:error != 0
    throw "Clang command failed"
  endif
  return getbufline(a:output_buffer, 1, '$')
endfunction

function! s:PrettierFormatterRun(lines, output_buffer, from, to)
  if executable('prettier') <= 0
    throw "Prettier Formatter: Error: Cannot find executable."
  endif
  let &l:shellredir = ">%s 2>&1"
  silent! noautocmd execute '%!'.shellescape(simplify(expand('prettier')))
  let l:new_lines = getbufline(a:output_buffer, 1, '$')
  if v:shell_error != 0
    " silent! keepjumps noautocmd %delete_
    " let &l:shellredir = '>%s 2>&1'
    " silent! noautocmd execute '%!'.shellescape(simplify(expand('prettier')))
    throw "Prettier command failed:\n".join(l:new_lines, "\n")."\n"
  endif
  return l:new_lines
endfunction

function! s:TsfmtRun(lines, output_buffer, from, to)
  if executable('tsfmt') <= 0
    throw "tsfmt: Cannot find executable."
  endif
  let &l:shellredir = ">%s 2>&1"
  silent! noautocmd execute '%!'.shellescape(simplify(expand('tsfmt'))).' --stdin'
  let l:new_lines = getbufline(a:output_buffer, 1, '$')
  if v:shell_error != 0
    throw "tsfmt command failed:\n".join(l:new_lines, "\n")."\n"
  endif
  return l:new_lines
endfunction

function! s:Find()
  if Uncrustify#Executable#Exists()
        \ && Uncrustify#Configuration#Local#Exists()
    let b:formatter = 1
    redraw
  elseif Uncrustify#Executable#ClangFormatterExists()
        \ && Uncrustify#Configuration#Local#ClangFormatterExists()
    let b:formatter = 2
    redraw
  elseif (Uncrustify#Executable#Exists()
        \ && (Uncrustify#Configuration#Global#Exists()
        \ || Uncrustify#Configuration#System#Exists()))
    let b:formatter = 1
    redraw
  elseif Uncrustify#Executable#ClangFormatterExists()
    let b:formatter = 2
    redraw
  else
    echohl WarningMsg
    echomsg "Formatter: Warning: Couldnot find any configuration."
    echohl None
    return 0
  endif
  return 1
endfunction

function! Uncrustify#Format()
  if !exists("b:formatter")
    if &filetype =~ "^\\(slice\\|c\\|cpp\\|cs\\|d\\|java\\|objc\\|objcpp\\)$"
      if !s:Find()
        return
      endif
    elseif &filetype =~ "^\\(proto\\)$"
      let b:formatter = 2
    elseif &filetype =~ "^\\(javascript\\|javascript.jsx\\)$"
      let b:formatter = 3
    elseif &filetype =~ "^\\(typescript\\)$"
      let b:formatter = 4
    else
      return
    endif
  endif
  if b:formatter == 1
    call Uncrustify#FormatBuffer(function(s:Run))
  elseif b:formatter == 2
    call Uncrustify#FormatBuffer(function('s:ClangFormatterRun'))
  elseif b:formatter == 3
    call Uncrustify#FormatBuffer(function('s:PrettierFormatterRun'))
  elseif b:formatter == 4
    call Uncrustify#FormatBuffer(function('s:TsfmtRun'))
  endif
endfunction

function! Uncrustify#FormatRange() range
  if !exists("b:formatter")
    if &filetype =~ "^\\(slice\\|c\\|cpp\\|cs\\|d\\|java\\|objc\\|objcpp\\)$"
      if !s:Find()
        return
      endif
    elseif &filetype =~ "^\\(proto\\)$"
      let b:formatter = 2
    elseif &filetype =~ "^\\(javascript\\|javascript.jsx\\)$"
      let b:formatter = 3
    elseif &filetype =~ "^\\(typescript\\)$"
      let b:formatter = 4
    else
      return
    endif
  endif
  if b:formatter == 1
    call s:Format(a:firstline, a:lastline, function(s:Run))
  elseif b:formatter == 2
    call s:Format(a:firstline, a:lastline, 's:ClangFormatterRun')
  elseif b:formatter == 3
    call s:Format(a:firstline, a:lastline, 's:PrettierFormatterRun')
  elseif b:formatter == 4
    call s:Format(a:firstline, a:lastline, 's:TsfmtRun')
  endif
endfunction

function! Uncrustify#FormatBuffer(run_functor)
  call s:Format(1, '$', a:run_functor)
endfunction

let &cpoptions = s:cpoptions
unlet s:cpoptions

" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
