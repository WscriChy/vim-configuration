if exists('g:Uncrustify#Configuration#autoload')
  finish
endif

let g:Uncrustify#Configuration#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ------------------------------------------------------------------------------
" Public {{{
" ------------------------------------------------------------------------------
function! Uncrustify#Configuration#GetPath()
  if     Uncrustify#Configuration#Local#Exists()
    return Uncrustify#Configuration#Local#GetPath()
  elseif Uncrustify#Configuration#Global#Exists()
    return Uncrustify#Configuration#Global#GetPath()
  elseif Uncrustify#Configuration#System#Exists()
    return Uncrustify#Configuration#System#GetPath()
  else
    return ''
  endif
endfunction

function! Uncrustify#Configuration#GetAbsolutePath()
  if Uncrustify#Configuration#Local#Exists()
    return Uncrustify#Configuration#Local#GetAbsolutePath()
  elseif Uncrustify#Configuration#Global#Exists()
    return Uncrustify#Configuration#Global#GetAbsolutePath()
  elseif Uncrustify#Configuration#System#Exists()
    return Uncrustify#Configuration#System#GetAbsolutePath()
  else
    return ''
  endif
endfunction

function! Uncrustify#Configuration#Exists()
  return
  \ Uncrustify#Configuration#Local#Exists()
  \ ||
  \ Uncrustify#Configuration#Global#Exists()
  \ ||
  \ Uncrustify#Configuration#System#Exists()
endfunction

function! Uncrustify#Configuration#Check()
  if Uncrustify#Configuration#Exists()
    redraw
    echomsg
    \ "Uncrustify: Using configuration:"                                       .
    \ ' '                                                                      .
    \ '"'                                                                      .
    \ Uncrustify#Configuration#GetAbsolutePath()                               .
    \ '"'                                                                      .
    \ '.'
  else
    echohl WarningMsg
    echomsg "Uncrustify: Warning: Cannot find configuration."
    echohl None
  endif
endfunction
" ------------------------------------------------------------------------------
" }}} Public
" ------------------------------------------------------------------------------
" }}} Functions

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ------------------------------------------------------------------------------
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ------------------------------------------------------------------------------
" }}} Modeline
