if exists('g:Uncrustify#Version#autoload')
  finish
endif

let g:Uncrustify#Version#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ------------------------------------------------------------------------------
" Public {{{
" ------------------------------------------------------------------------------
function! Uncrustify#Version#GetMajor()
  return s:major
endfunction

function! Uncrustify#Version#GetMinor()
  return s:minor
endfunction

function! Uncrustify#Version#GetPatch()
  return s:patch
endfunction

function! Uncrustify#Version#ToString()
  return s:major . '.' . s:minor . '.' . s:patch
endfunction
" ------------------------------------------------------------------------------
" }}} Public
" ------------------------------------------------------------------------------
" }}} Functions

" Defaults {{{
" ------------------------------------------------------------------------------
let s:major = 0
let s:minor = 0
let s:patch = 0
" ------------------------------------------------------------------------------
" }}} Defaults

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ------------------------------------------------------------------------------
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ------------------------------------------------------------------------------
" }}} Modeline
