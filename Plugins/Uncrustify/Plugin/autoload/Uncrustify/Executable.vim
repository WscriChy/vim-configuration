if exists('g:Uncrustify#Executable#autoload')
  finish
endif

let g:Uncrustify#Executable#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ------------------------------------------------------------------------------
" Public {{{
" ------------------------------------------------------------------------------
function! Uncrustify#Executable#SetPath(path)
  if type(a:path) != type('')
    throw "Uncrustify: Error: `a:path` should be of type `String`."
  endif
  let s:path = a:path
endfunction

function! Uncrustify#Executable#GetPath()
  return simplify(expand(s:path))
endfunction

function! Uncrustify#Executable#GetClangFormatterPath()
  return simplify(expand("clang-format"))
endfunction

function! Uncrustify#Executable#GetAbsolutePath()
  " TODO: In fact, one has to check the `PATH` environment variable as well.
  return fnamemodify(Uncrustify#Executable#GetPath(), ':p')
endfunction

function! Uncrustify#Executable#GetClangFormatterAbsolutePath()
  return fnamemodify(Uncrustify#Executable#GetClangFormatterPath(), ':p')
endfunction

function! Uncrustify#Executable#IsAbsolute()
  return
  \ Uncrustify#Executable#GetPath()
  \ ==
  \ Uncrustify#Executable#GetAbsolutePath()
endfunction

function! Uncrustify#Executable#Exists()
  return executable(Uncrustify#Executable#GetPath()) > 0
endfunction

function! Uncrustify#Executable#ClangFormatterExists()
  return executable(Uncrustify#Executable#GetClangFormatterPath()) > 0
endfunction

function! Uncrustify#Executable#Check()
  if Uncrustify#Executable#Exists()
    redraw
  else
    echohl WarningMsg
    echomsg "Uncrustify: Warning: Cannot find executable."
    echohl None
  endif
endfunction
" ------------------------------------------------------------------------------
" }}} Public
" ------------------------------------------------------------------------------
" }}} Functions
"

" Defaults {{{
" ------------------------------------------------------------------------------
call Uncrustify#Executable#SetPath('uncrustify')
" ------------------------------------------------------------------------------
" }}} Defaults

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ------------------------------------------------------------------------------
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ------------------------------------------------------------------------------
" }}} Modeline
