if exists('g:Uncrustify#Configuration#System#autoload')
  finish
endif

let g:Uncrustify#Configuration#System#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ------------------------------------------------------------------------------
" Public {{{
" ------------------------------------------------------------------------------
function! Uncrustify#Configuration#System#SetPath(path)
  if type(a:path) != type('')
    throw "Uncrustify: Error: `a:path` should be of type `String`."
  endif
  let s:path = a:path
endfunction

function! Uncrustify#Configuration#System#GetPath()
  return simplify(expand(s:path))
endfunction

function! Uncrustify#Configuration#System#GetAbsolutePath()
  return fnamemodify(Uncrustify#Configuration#System#GetPath(), ':p')
endfunction

function! Uncrustify#Configuration#System#IsAbsolute()
  return
  \ Uncrustify#Configuration#System#GetPath()
  \ ==
  \ Uncrustify#Configuration#System#GetAbsolutePath()
endfunction

function! Uncrustify#Configuration#System#Exists()
  return filereadable(Uncrustify#Configuration#System#GetPath())
endfunction
" ------------------------------------------------------------------------------
" }}} Public
" ------------------------------------------------------------------------------
" }}} Functions

" Defaults {{{
" ------------------------------------------------------------------------------
if has('unix')
  call
  \ Uncrustify#Configuration#System#SetPath(
  \   '/etc/uncrustify/default.uncrustify'
  \ )
else
  call Uncrustify#Configuration#System#SetPath('')
endif
" ------------------------------------------------------------------------------
" }}} Defaults

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ------------------------------------------------------------------------------
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ------------------------------------------------------------------------------
" }}} Modeline
