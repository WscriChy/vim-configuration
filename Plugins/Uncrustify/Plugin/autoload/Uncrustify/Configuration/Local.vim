if exists('g:Uncrustify#Configuration#Local#autoload')
  finish
endif

let g:Uncrustify#Configuration#Local#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ------------------------------------------------------------------------------
" Public {{{
" ------------------------------------------------------------------------------
function! Uncrustify#Configuration#Local#SetName(name)
  if type(a:name) != type('')
    throw "Uncrustify: Error: `a:name` should be of type `String`."
  endif
  let s:name = a:name
endfunction

function! Uncrustify#Configuration#Local#GetName()
  return s:name
endfunction

function! Uncrustify#Configuration#Local#GetPath()
  return findfile(Uncrustify#Configuration#Local#GetName(), '%,.;')
endfunction

function! Uncrustify#Configuration#Local#GetClangFormatterPath()
  return findfile(".clang-format", '%,.;')
endfunction

function! Uncrustify#Configuration#Local#GetAbsolutePath()
  return fnamemodify(Uncrustify#Configuration#Local#GetPath(), ':p')
endfunction

function! Uncrustify#Configuration#Local#GetClangFormatterPathAbsolutePath()
  return fnamemodify(Uncrustify#Configuration#Local#GetClangFormatterPath(), ':p')
endfunction

function! Uncrustify#Configuration#Local#IsAbsolute()
  return
  \ Uncrustify#Configuration#Local#GetPath()
  \ ==
  \ Uncrustify#Configuration#Local#GetAbsolutePath()
endfunction

function! Uncrustify#Configuration#Local#Exists()
  return filereadable(Uncrustify#Configuration#Local#GetPath())
endfunction

function! Uncrustify#Configuration#Local#ClangFormatterExists()
  return filereadable(Uncrustify#Configuration#Local#GetClangFormatterPath())
endfunction
" ------------------------------------------------------------------------------
" }}} Public
" ------------------------------------------------------------------------------
" }}} Functions

" Defaults {{{
" ------------------------------------------------------------------------------
call Uncrustify#Configuration#Local#SetName('.uncrustify')
" ------------------------------------------------------------------------------
" }}} Defaults

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ------------------------------------------------------------------------------
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ------------------------------------------------------------------------------
" }}} Modeline
