if exists('g:Uncrustify#Configuration#Global#autoload')
  finish
endif

let g:Uncrustify#Configuration#Global#autoload = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Functions {{{
" ------------------------------------------------------------------------------
" Public {{{
" ------------------------------------------------------------------------------
function! Uncrustify#Configuration#Global#SetPath(path)
  if type(a:path) != type('')
    throw "Uncrustify: Error: `a:path` should be of type `String`."
  endif
  let s:path = a:path
endfunction

function! Uncrustify#Configuration#Global#GetPath()
  return simplify(expand(s:path))
endfunction

function! Uncrustify#Configuration#Global#GetAbsolutePath()
  return fnamemodify(Uncrustify#Configuration#Global#GetPath(), ':p')
endfunction

function! Uncrustify#Configuration#Global#IsAbsolute()
  return
  \ Uncrustify#Configuration#Global#GetPath()
  \ ==
  \ Uncrustify#Configuration#Global#GetAbsolutePath()
endfunction

function! Uncrustify#Configuration#Global#Exists()
  return filereadable(Uncrustify#Configuration#Global#GetPath())
endfunction
" ------------------------------------------------------------------------------
" }}} Public
" ------------------------------------------------------------------------------
" }}} Functions

" Defaults {{{
" ------------------------------------------------------------------------------
call Uncrustify#Configuration#Global#SetPath('~/.uncrustify/default.uncrustify')
" ------------------------------------------------------------------------------
" }}} Defaults

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ------------------------------------------------------------------------------
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ------------------------------------------------------------------------------
" }}} Modeline
