" Hooks {{{
" ------------------------------------------------------------------------------
autocmd BufNewFile,BufRead *.uncrustify let &l:filetype = 'uncrustify'
" ------------------------------------------------------------------------------
" }}} Hooks

" Modeline {{{
" ------------------------------------------------------------------------------
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ------------------------------------------------------------------------------
" }}} Modeline
