if exists('b:did_ftplugin')
  finish
endif

let b:did_ftplugin = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

let &l:comments      = ':#'
let &l:commentstring = '#%s'

let b:undo_ftplugin =
\ 'setlocal comments<'                                                         .
\ ' '                                                                          .
\ '|'                                                                          .
\ ' '                                                                          .
\ 'setlocal commentstring<'

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ------------------------------------------------------------------------------
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ------------------------------------------------------------------------------
" }}} Modeline
