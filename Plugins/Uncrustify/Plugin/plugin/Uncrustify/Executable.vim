if exists('g:Uncrustify#Executable#plugin')
  finish
endif

let g:Uncrustify#Executable#plugin = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Hooks {{{
" ------------------------------------------------------------------------------
" augroup Uncrustify#Executable#Check
"   autocmd!
"   autocmd FileType c,cpp,cs,d,java,objc,objcpp
"   \ call Uncrustify#Executable#Check()
" augroup END
" ------------------------------------------------------------------------------
" }}} Hooks

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ------------------------------------------------------------------------------
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ------------------------------------------------------------------------------
" }}} Modeline
