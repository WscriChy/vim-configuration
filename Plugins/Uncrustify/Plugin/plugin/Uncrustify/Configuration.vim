if exists('g:Uncrustify#Configuration#plugin')
  finish
endif

let g:Uncrustify#Configuration#plugin = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Hooks {{{
" ------------------------------------------------------------------------------
" augroup Uncrustify#Configuration#Check
"   autocmd!
"   autocmd FileType c,cpp,cs,d,java,objc,objcpp
"   \ call Uncrustify#Configuration#Check()
" augroup END
" ------------------------------------------------------------------------------
" }}} Hooks

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ------------------------------------------------------------------------------
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ------------------------------------------------------------------------------
" }}} Modeline
