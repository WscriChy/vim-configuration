if exists('g:Uncrustify#plugin')
  finish
endif

let g:Uncrustify#plugin = 1

let s:cpoptions = &cpoptions
set cpoptions&vim

" Plugs {{{
" ------------------------------------------------------------------------------
" Normal {{{
" ------------------------------------------------------------------------------
nnoremap <script> <silent> <unique> <Plug>Uncrustify#FormatRange
\ :call Uncrustify#FormatRange()<CR>

nnoremap <script> <silent> <unique> <Plug>Uncrustify#FormatBuffer
\ :call Uncrustify#Format()<CR>
" ------------------------------------------------------------------------------
" }}} Normal

" Visual {{{
" ------------------------------------------------------------------------------
vnoremap <script> <silent> <unique> <Plug>Uncrustify#FormatRange
\ :call Uncrustify#FormatRange()<CR>
" ------------------------------------------------------------------------------
" }}} Visual
" ------------------------------------------------------------------------------
" }}} Plugs

let &cpoptions = s:cpoptions
unlet s:cpoptions

" Modeline {{{
" ------------------------------------------------------------------------------
" vim:ft=vim:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
" ------------------------------------------------------------------------------
" }}} Modeline
