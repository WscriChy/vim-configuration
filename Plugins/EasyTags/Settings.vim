let g:easytags_auto_update = 1
let g:easytags_auto_highlight = 0
let g:easytags_async = 1
let g:easytags_file = g:trash."/tags"
" let g:easytags_events = ['BufWritePost', 'CursorHold']
let g:easytags_events = ['BufWritePost']
