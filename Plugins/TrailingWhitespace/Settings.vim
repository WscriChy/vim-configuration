highlight ExtraWhitespace ctermbg=blue guibg=#5F0E10

command! AutoStripWhitespaceToggle call <SID>AutoStripToggle()

function! s:AutoStripToggle()
  if !exists('b:toggle_auto_strip')
    let b:toggle_auto_strip = 0
  endif
  if b:toggle_auto_strip
    autocmd! TrailingWhitespaceToggleAutorStripPluginGroup * <buffer>
    let b:toggle_auto_strip = 0
  else
    augroup TrailingWhitespaceToggleAutorStripPluginGroup
      autocmd!
      autocmd BufWrite <buffer> StripWhitespace
    augroup end
    let b:toggle_auto_strip = 1
  endif
endfunction
