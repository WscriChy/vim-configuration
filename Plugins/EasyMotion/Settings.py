#!/usr/bin/env python
# encoding: utf-8

import os

import vim

from Configurator import *

set("g:EasyMotion_prompt", "'{n}>>>'")

set("g:EasyMotion_keys",
    "'acdefghijklmnopqrstuvwx'"
    # "'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'"
    )
set("g:EasyMotion_grouping", "1")
set("g:EasyMotion_smartcase", "1")
set("g:EasyMotion_do_mapping", 0)
set("g:EasyMotion_enter_jump_first", 1)
set("g:EasyMotion_space_jump_first", 1)
set("g:EasyMotion_re_anywhere",
    ("'\\v"
     "[:;(){}+-=/\\\\,]'"
     ))

vim.command("hi EasyMotionTarget guifg=#FF0000 gui=bold")
vim.command("hi EasyMotionTarget2First guifg=#FF0000 gui=bold")
vim.command("hi EasyMotionTarget2Second guifg=#AA0000 gui=bold")
