
function! s:OpenVisuallySelectedFiles() range
  echo "firstline ".a:firstline." lastline ".a:lastline
  let line1 = a:firstline
  let line2 = a:lastline
  execute "py OpenFile.openVisuallySelectedFiles(".line1.",".line2.")"
endfunction

command! -range OpenFiles <line1>,<line2>call <SID>OpenVisuallySelectedFiles()
