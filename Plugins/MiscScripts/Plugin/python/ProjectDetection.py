#!/usr/bin/env python
# encoding: utf-8

import os

import vim

import FindCommands

class _AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(_AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self

_project = _AttrDict(type='',path='',tags='')

def detectProject(project_dir = ''):
  if project_dir != '':
    project_dir = validatePath(project_dir)
    if project_dir == '':
      detectDefaultProject()
      return
    setProject(project_dir)
    return

  detectDefaultProject()

def detectDefaultProject():
  if len(vim.buffers) > 0:
    project_dir = vim.eval('expand("%:p:h")')
  else:
    project_dir = getCwd()
  (project_type, project_dir) = findProjectDir(project_dir)
  if project_dir != '':
    setProject(project_dir, project_type)
  else:
    setProject(getCwd())

def validatePath(path):
  path = os.path.abspath(path)
  if not os.path.isdir(path):
    print("Path '{0}' is not a director".format(path))
    return ''
  return path

def findProjectDir(top):
  head = top
  while True:
    for f in os.listdir(head):
      if f == ".git" and os.path.isdir(os.path.join(head, f)):
          return ('git', head)
    (head, tail) = os.path.split(head)
    if tail == "":
      return ('', top)

def setProject(project_dir, project_type = ''):
  cwd = getCwd()
  if project_type == '':
    for f in os.listdir(project_dir):
      if f == ".git" and os.path.isdir(os.path.join(project_dir, f)):
          project_type = 'git'
          break
  if project_type == '':
    setupTagging()
    if len(cwd) < len(project_dir):
      if cwd == project_dir[0:len(cwd)]:
        vim.command('cd {0}'.format(project_dir))
    else:
      if cwd[0:len(project_dir)] != project_dir:
        vim.command('cd {0}'.format(project_dir))
  else:
    setupDynamicTagging(project_dir)
    if len(cwd) < len(project_dir):
      if cwd == project_dir[0:len(cwd)]:
        vim.command('cd {0}'.format(project_dir))
    else:
      if cwd[0:len(project_dir)] != project_dir:
        vim.command('cd {0}'.format(project_dir))
  _project.type = project_type
  _project.path = project_dir
  vim.command("let g:PrjDir='{0}'".format(project_dir))
  setProjectFindCommand()

def setupTagging():
  vim.command("execute \"set tags=\".g:trash.\"'/tags'\"")
  vim.command("let g:easytags_dynamic_files=0")
  _project.tags = ''

def setupDynamicTagging(project_dir):
  tag_file = os.path.join(project_dir, ".tags")
  while not os.path.isfile(tag_file):
    if os.path.isdir(tag_file):
      tag_file = tag_file + "x"
    else:
      with open(tag_file, 'a'): os.utime(tag_file, None)
      break
  vim.command("set tags={0}".format(tag_file))
  vim.command("let g:easytags_dynamic_files=1")
  # if os.stat(tag_file).st_size < 50*1024*1024:
  #   vim.command("let g:easytags_autorecurse=1")
  _project.tags = tag_file

def setupCommands():
  vim.command(
      "command! -nargs=* -complete=file DetectProject"
      " execute \"py import ProjectDetection;"
      "ProjectDetection.detectProject('\".<q-args>.\"')\"")
  vim.command(
      "command! PFindFindCommandSet"
      " execute \"py import ProjectDetection;ProjectDetection.setPlainFindFindCommand()\"")
  vim.command(
      "command! FindFindCommandSet"
      " execute \"py import ProjectDetection;ProjectDetection.setFindFindCommand()\"")
  vim.command(
      "command! PGitFindCommandSet"
      " execute \"py import ProjectDetection;ProjectDetection.setPlainGitFindCommand()\"")
  vim.command(
      "command! GitFindCommandSet"
      " execute \"py import ProjectDetection;ProjectDetection.setGitFindCommand()\"")
  vim.command(
      "command! MyProjectCtrlP"
      " execute \"py import ProjectDetection;ProjectDetection.MyCtrlP()\"")

def MyCtrlP():
  if _project.type == 'git':
    vim.command("CtrlP")
  else:
    vim.command("CtrlP " + _project.path)

def getCwd():
  return vim.eval("getcwd()")

def setProjectFindCommand():
  if _project.type == 'git':
    setGitFindCommand()
    vim.command("let g:ctrlp_working_path_mode = 'ra'")
  else:
    setFindFindCommand()
    vim.command("let g:ctrlp_working_path_mode = '0'")

def setPlainFindFindCommand():
  setCtrlPPlainFindFindCommand()

def setFindFindCommand():
  setCtrlPFindFindCommand()

def setPlainGitFindCommand():
  setCtrlPPlainGitFindCommand()

def setGitFindCommand():
  setCtrlPGitFindCommand()

# CtrlP Find Command {{{
def setCtrlPPlainFindFindCommand():
  _setCtrlPFindFindCommand(FindCommands.getPlainFind('%s'))

def setCtrlPFindFindCommand():
  _setCtrlPFindFindCommand(FindCommands.getFindWithAgIgnorePattern('%s'))

def setCtrlPPlainGitFindCommand():
  _setCtrlPFindFindCommand(FindCommands.getPlainGit('%s'))

def setCtrlPGitFindCommand():
  _setCtrlPFindFindCommand(FindCommands.getGitWithAgIgnorePattern('%s'))

def _setCtrlPFindFindCommand(command):
  vim.command("let g:ctrlp_user_command = '{0}'".format(command))
# CtrlP Find Command }}}

setupCommands()
detectProject()
