#!/usr/bin/env python
# encoding: utf-8

_ignore_directories = (
     ".git|.hg"
    "|.build|.install"
    "|tmp|temp"
    ).split('|')

_ignore_file_extensions = (
   "pyc|pyo|pyd"
  "|exe|dll|so|lib|a|obj|o|d"
  "|pdf|djvu|djv"
  "|mp3|ogg|flv"
  "|avi|mp4|mkv"
  "|ttf"
  "|7z|zip|tar|bz|bz2|gz|xz|rar"
  "|db"
  "|out|log|swp|build"
  "|png|jpg|jpeg|ico|tiff|gif|svg").split('|')

# `find` command generators {{{
def getPlainFind(dr):
  return 'find %s -type f'

def getFindWithIgnoreDirecotires(dr):
  if _ignore_directories == '':
    return 'find %s -type f'
  ignore_extensions = ""
  for dr in _ignore_directories:
    ignore_extensions += ' ! -ipath "*/{0}/*"'.format(dr)
  command = 'find %s {0} -type f '.format(ignore_extensions)
  return command

def getFindWithIgnorePattern(dr):
  if len(_ignore_file_extensions) == 0:
    return getFindWithIgnoreDirecotires(dr)
  ignores = ""
  for dr in _ignore_directories:
    ignores += " ! -path *.{0}".format(dr)
  for ext in _ignore_file_extensions:
    ignores += " ! -iname *.{0}".format(ext)
  command = 'find %s -type f \( {0} \)'.format(ignores)
  return command

def getFindWithAgIgnorePattern(dr):
  if len(_ignore_file_extensions) == 0:
    return getFindWithIgnoreDirecotires(dr)
  command = '{0} | {1}'.format(
      getFindWithIgnoreDirecotires(dr),
      getAgCommandWithIgnorePattern())
  return command
# `find` command generators }}}

# `git` command generator {{{
def getPlainGit(dr):
  return '\\cd %s && git ls-files'

def getGitWithAgIgnorePattern(dr):
  if len(_ignore_file_extensions) == 0:
    return getPlainGit(dr)
  command = '{0} | {1}'.format(
      getPlainGit(dr),
      getAgCommandWithIgnorePattern())
  return command
# `git` command generator }}}


def getAgCommandWithIgnorePattern():
  if len(_ignore_file_extensions) == 0:
    return 'ag --nocolor ".*"'
  ignore_extensions = ""
  i = 0
  for ext in _ignore_file_extensions:
    if i == 0:
      ignore_extensions += "{0}".format(ext)
      i = i + 1
    else:
      ignore_extensions += "|{0}".format(ext)
  command = 'ag -v --nocolor "\.({0})$"'.format(ignore_extensions)
  return command
