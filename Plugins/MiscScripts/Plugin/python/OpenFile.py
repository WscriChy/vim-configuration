#!/usr/bin/env python
# encoding: utf-8

import re

import vim

def openVisuallySelectedFiles(line1, line2):
  cur_buffer = vim.current.buffer
  cur_window = vim.current.window
  do_restore_prev = True
  if (cur_buffer.options['buftype'] != ''
     or cur_buffer.options['bufhidden'] != ''):
     do_restore_prev = False
     vim.command("silent! keepjumps keepalt new")
  if line1 == line2:
    vim.command("edit {0}".format(escaptePath(cur_buffer[line1-1])))
  else:
    for path in cur_buffer[line1-1:line2-1]:
      vim.command("bad {0}".format(escaptePath(path)))
    vim.command("edit {0}".format(escaptePath(cur_buffer[line2-1])))
  if do_restore_prev:
    vim.command("{0}b".format(cur_buffer.number))
  else:
    vim.command("{0}wincmd w".format(cur_window.number))


def escaptePath(path):
  path = re.sub(r'\\', r'\\\\', path)
  path = re.sub(r' ', r'\\ ', path)
  return path
