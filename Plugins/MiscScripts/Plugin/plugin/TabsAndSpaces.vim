function s:GetSpacesOrTabsCommand(isTabs, width)
  if a:isTabs
    return s:GetTabsCommand(a:width)
  else
    return s:GetSpacesCommand(a:width)
  endif
endfunction

function s:GetSpacesCommand(width)
  return "setlocal expandtab shiftwidth=".a:width." softtabstop=".a:width." tabstop=".a:width
endfunction

function s:GetTabsCommand(width)
  return "setlocal smarttab noexpandtab shiftwidth=".a:width." softtabstop=0 tabstop=".a:width
endfunction

function! s:SetSpacesOrTabs(fileTypes, isTabs, width)
  for type in a:fileTypes
    if &ft == type
      execute s:GetSpacesOrTabsCommand(a:isTabs, a:width)
      break
    endif
  endfor
  let typesString = a:fileTypes[0]
  let shortTypesString = a:fileTypes[0]
  for type in a:fileTypes[1:]
    let typesString .= ",".type
    let shortTypesString .= type
  endfor
  execute "augroup MiscScriptsTabsAndSpacesGroupFor".shortTypesString
    au!
    execute "au FileType ".typesString." ".s:GetSpacesOrTabsCommand(a:isTabs, a:width)
  augroup END
endfunction

command! -nargs=1 CSetSpaces
      \ call s:SetSpacesOrTabs(['c', 'cpp', 'slice'], 0, <args>)
command! -nargs=1 CSetTabs
      \ call s:SetSpacesOrTabs(['c', 'cpp', 'slice'], 1, <args>)

command! -nargs=1 PythonSetSpaces
      \ call s:SetSpacesOrTabs(['python'], 0, <args>)
command! -nargs=1 PythonSetTabs
      \ call s:SetSpacesOrTabs(['python'], 1, <args>)
