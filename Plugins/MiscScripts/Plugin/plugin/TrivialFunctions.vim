function! s:QuickfixToggle()
    let cur_win_nr = winnr()
	  for i in range(1, tabpagewinnr(tabpagenr(), '$'))
	    let buf_nr =  winbufnr(i)
	    if buf_nr == -1
	      continue
      endif
      if getbufvar(buf_nr, "&buftype") == "quickfix"
        cclose
        return
      endif
    endfor
    copen
    execute "silent! ".cur_win_nr."wincmd w"
endfunction

command! -nargs=* QuickfixToggle
      \ :call s:QuickfixToggle()
