hi! MyOperator     guifg=#CC33FF gui=bold
hi! Namespace      guifg=#00BFFF
hi! iCursor        guifg=#000000 guibg=#F8F8F0
hi! MatchParen     cterm=reverse ctermfg=NONE ctermbg=NONE gui=reverse guibg=NONE guifg=NONE
DoMatchParen

hi! TabLineFill    guifg=#1B1D1E guibg=#1B1D1E
hi! TabLine        guibg=#1B1D1E guifg=#808080 gui=none
" hi TabLineSel ctermfg=Red ctermbg=Yellow:if &ro==0 && &bl==1 && &bt==''|silent! w|endif
