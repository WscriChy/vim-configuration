let s:script_folder_path = escape( expand( '<sfile>:p:h' ), '\' ).'/Plugin'

python << ImportEOF
import sys, os, vim
sys.path.insert(0, os.path.abspath(os.path.join(vim.eval('s:script_folder_path' ), 'python')))
import FindCommands
import ProjectDetection
import OpenFile
ImportEOF
" sys.path.pop(0)

execute "source ".s:script_folder_path."/vim/OpenFile.vim"

function! SynGroup()
    let l:s = synID(line('.'), col('.'), 1)
    echo synIDattr(l:s, 'name') . ' -> ' . synIDattr(synIDtrans(l:s), 'name')
endfun
