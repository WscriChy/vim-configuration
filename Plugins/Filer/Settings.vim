let g:vimfiler_as_default_explorer = 1
let g:vimfiler_no_default_key_mappings = 1
let g:vimfiler_min_cache_files = 100
let g:vimfiler_max_directories_history = 1000000
let g:vimfiler_safe_mode_by_default = 0

let g:vimfiler_tree_indentation = 4
let g:vimfiler_tree_leaf_icon = '|'
let g:vimfiler_tree_opened_icon = '▾'
let g:vimfiler_tree_closed_icon = '▸'
let g:vimfiler_file_icon = '-'
let g:vimfiler_marked_file_icon = '*'

" Windows.
" let g:vimfiler_quick_look_command = 'maComfort.exe -ql'
" Linux.
let g:vimfiler_quick_look_command = 'gloobus-preview'

let g:vimfiler_ignore_pattern =
 \ '^\(\..*\|.*\.\(py[odc]\|exe\|dll\|so\|lib\|a\|obj\|o\|d\|pdf\|7z\|zip\|tar.*\|bz\|gz\|xz\|out\|log\|db\)\)$'

"autocmd VimEnter * if !argc() | VimFiler | endif
autocmd FileType vimfiler call s:vimFilerMySettings()

function! s:vimFilerMySettings()
  setlocal nobuflisted
  nmap <silent><buffer> ` <Plug>(vimfiler_hide)
  nmap <silent><buffer> * <Plug>(vimfiler_toggle_mark_all_lines)
  nmap <silent><buffer> & <Plug>(vimfiler_mark_similar_lines)
  nmap <silent><buffer> <C-d> <Plug>(vimfiler_clear_mark_all_lines)
  vmap <silent><buffer> V <Plug>(vimfiler_toggle_mark_selected_lines)
  nmap <silent><buffer> v <Plug>(vimfiler_mark_current_line)
  nmap <silent><buffer> u <Plug>(vimfiler_clear_mark_all_lines)

  " nmap <silent><buffer> R <Plug>(vimfiler_switch_to_drive)
  nmap <silent><buffer> ~ <Plug>(vimfiler_switch_to_home_directory)
  " nmap <silent><buffer> \ <Plug>(vimfiler_switch_to_root_directory)
  nmap <silent><buffer> & <Plug>(vimfiler_switch_to_project_directory)
  nmap <silent><buffer> <C-j> <Plug>(vimfiler_switch_to_history_directory)

  nmap <silent><buffer> <C-c> <Plug>(vimfiler_clipboard_copy_file)
  nmap <silent><buffer> <C-x> <Plug>(vimfiler_clipboard_move_file)
  nmap <silent><buffer> <C-v> <Plug>(vimfiler_clipboard_paste)

  nmap <silent><buffer> s <Plug>(vimfiler_select_sort_type)
  nmap <silent><buffer> S <Plug>(vimfiler_open_file_in_another_vimfiler)

  nmap <silent><buffer> cd <Plug>(vimfiler_cd_vim_current_dir)

  nmap <silent><buffer> A <Plug>(vimfiler_popup_shell)
  nmap <silent><buffer> B <Plug>(vimfiler_edit_binary_file)
  nmap <silent><buffer> ge <Plug>(vimfiler_execute_external_filer)
  nmap <silent><buffer> <RightMouse> <Plug>(vimfiler_execute_external_filer)
  nmap <silent><buffer> ! <Plug>(vimfiler_execute_shell_command)
  nmap <silent><buffer> <C-v> <Plug>(vimfiler_switch_vim_buffer_mode)
  nmap <silent><buffer> <A-v> <Plug>(vimfiler_split_edit_file)
  nmap <silent><buffer><expr> e vimfiler#smart_cursor_map(
    \ "\<Plug>(vimfiler_execute_system_associated)",
    \ "\<Plug>(vimfiler_execute_external_filer)")
  nmap <silent><buffer> gf <Plug>(vimfiler_find)
  nmap <silent><buffer> gr <Plug>(vimfiler_grep)
  nmap <silent><buffer> gd <Plug>(vimfiler_cd_input_directory)
  nmap <silent><buffer> gs <Plug>(vimfiler_toggle_safe_mode)
  nmap <silent><buffer> gS <Plug>(vimfiler_toggle_simple_mode)
  nmap <silent><buffer> g<C-g> <Plug>(vimfiler_toggle_maximize_window)
  nmap <silent><buffer> P <Plug>(vimfiler_popd)
  nmap <silent><buffer> p <Plug>(vimfiler_preview_file)
  " nmap <silent><buffer> M <Plug>(vimfiler_set_current_mask)
  " nmap <silent><buffer> u <Plug>(vimfiler_switch_to_parent_directory)
  " nmap <silent><buffer> U <Plug>(vimfiler_execute)
  " nmap <silent><buffer> Y <Plug>(vimfiler_pushd)
  nmap <silent><buffer> <C-g> <Plug>(vimfiler_print_filename)
  nmap <silent><buffer> <2-LeftMouse> <Plug>(vimfiler_double_click)
  nmap <silent><buffer> <BS> <Plug>(vimfiler_switch_to_parent_directory)

  nmap <silent><buffer> O <Plug>(vimfiler_choose_action)

  nmap <silent><buffer> l <Plug>(vimfiler_smart_l)
  nmap <silent><buffer> h <Plug>(vimfiler_smart_h)
  nmap <silent><buffer> j <Plug>(vimfiler_loop_cursor_down)
  nmap <silent><buffer> k <Plug>(vimfiler_loop_cursor_up)
  nmap <silent><buffer> gj <Plug>(vimfiler_jump_last_child)
  nmap <silent><buffer> gk <Plug>(vimfiler_jump_first_child)
  nmap <silent><buffer> J gkko
  nmap <silent><buffer> Y gkko
  nmap <silent><buffer> K <Plug>(vimfiler_expand_tree_recursive)

  nmap <silent><buffer><expr> <Cr> vimfiler#smart_cursor_map(
    \ "\<Plug>(vimfiler_expand_tree)",
    \ "\<Plug>(vimfiler_edit_file)")
  nmap <silent><buffer><expr> o vimfiler#smart_cursor_map(
    \ "\<Plug>(vimfiler_expand_tree)",
    \ "\<Plug>(vimfiler_edit_file)")

  nmap <silent><buffer> R <Plug>(vimfiler_rename_file)
  nmap <silent><buffer> C <Plug>(vimfiler_copy_file)
  nmap <silent><buffer> D <Plug>(vimfiler_delete_file)
  nmap <silent><buffer> M <Plug>(vimfiler_move_file)

  nmap <silent><buffer> gn <Plug>(vimfiler_new_file)
  nmap <silent><buffer> gN <Plug>(vimfiler_make_directory)
  nmap <silent><buffer> gd <Plug>(vimfiler_switch_to_drive)

  nmap <silent><buffer> zh <Plug>(vimfiler_toggle_visible_ignore_files)
  nmap <silent><buffer> zs <Plug>(vimfiler_sync_with_current_vimfiler)
  nmap <silent><buffer> zy <Plug>(vimfiler_yank_full_path)

  nmap <silent><buffer><expr> f vimfiler#do_action('find_in_files')
  nmap <silent><buffer><expr> F vimfiler#do_action('replace_in_files')

  nmap <silent><buffer> q <Plug>(vimfiler_hide)
  nmap <silent><buffer> Q <Plug>(vimfiler_exit)
  " nmap <silent><buffer> ? <Plug>(vimfiler_help)
endfunction

call vimfiler#custom#profile('default', 'context', {
      \ 'columns' : ""
      \ })
