#!/usr/bin/env python
# encoding: utf-8

import os
import sys
import vim
import re
import inspect
import platform
import traceback

currentDir = os.path.dirname(os.path.realpath(
    os.path.abspath(inspect.getfile(inspect.currentframe()))))

sys.path.append(os.path.normpath(os.path.join(currentDir, "../cpp/bin")))

import FindAndReplace

def _printExceptionToVim():
  type_, value_, traceback_ = sys.exc_info()
  vim.command('echomsg "{0}"'.format(
    re.sub("\"",
      "\\\\\"",
      "".join(traceback.format_exception(
        type_,
        value_,
        traceback_)))))

def _replaceBufferContent(bufferNumber, bufferContent):
  try:
    vim.command(str(bufferNumber) + "buffer")
    eventignore = vim.options['eventignore']
    vim.options['eventignore'] = 'all'
    buffer = vim.buffers[bufferNumber]
    del buffer[:]
    # vim.command("silent! %delete")
    # vim.command('silent! noautocmd 1put={0}'.format(bufferContent))
    buffer.append(bufferContent)
    del buffer[0]
    # vim.command("silent! 1delete_")
    vim.options['eventignore'] = eventignore

    # if len(buffer) > 0:
    #   del buffer[len(buffer) - 1]
  except:
    _printExceptionToVim()

FindAndReplace.setIgnorePattern(
    "^.*\.(?:py[odc]|exe|dll|so|lib|a|obj|o|d"
    "|pdf|djvu|djv"
    "|mp3|ogg|flv"
    "|avi|mp4|mkv"
    "|ttf"
    "|7z|zip|tar|bz|bz2|gz|xz|rar"
    "|db"
    "|out|log|swp|build"
    "|git|hg"
    "|png|jpg|jpeg|ico|tiff|gif|svg)$"
    )

def _getBuffer(filePath):
  bufnr = vim.bindeval("bufnr('^{0}$')".format(filePath))
  return bufnr

def _openFileInVim(filePath, lnum=-1):
  bufnr = -1
  try:
    #eventignore = vim.options['eventignore']
    #vim.options['eventignore'] = 'all'
    vim.command("set shortmess+=A")
    vim.command("bad " + filePath)
    vim.command("set shortmess-=A")
    #vim.options['eventignore'] = eventignore
    # bufnr = int(vim.eval("bufnr('^{0}$')".format(filePath)))
    bufnr = _getBuffer(filePath)
  except vim.error as e:
    #logging.exception('Got exception from vim open command')
    _printExceptionToVim()
    return -1
  return bufnr

def find(command, paths):
  filesResults = FindAndReplace.find(
      command,
      paths
      );
  if filesResults is None:
    vim.command('echomsg "{0}"'.format(FindAndReplace.getLastError()))
    return
  resultEntities = []
  for filePath, entities in filesResults.iteritems():
    bufferNumber = _getBuffer(filePath)
    if bufferNumber < 0:
      bufferNumber = _openFileInVim(filePath)
    for entity in entities:
      entity["bufnr"] = bufferNumber
      resultEntities.append(entity)
  vim.Function('setqflist')(resultEntities, 'r')
  vim.command("copen")

def replace(command, paths):
  filesResults = FindAndReplace.replace(
      command,
      paths
      );
  if filesResults is None:
    vim.command('echomsg "{0}"'.format(FindAndReplace.getLastError()))
    return
  resultEntities = []

  cur_buffer = vim.current.buffer
  cur_window = vim.current.window
  do_restore_prev = True
  if (cur_buffer.options['buftype'] != ''
     or cur_buffer.options['bufhidden'] != ''):
     do_restore_prev = False
     vim.command("silent! keepjumps keepalt new")

  for filePath, results in filesResults.iteritems():
    bufferNumber = _getBuffer(filePath)
    if bufferNumber < 0:
      bufferNumber = _openFileInVim(filePath)
    for entity in results[0]:
      entity["bufnr"] = bufferNumber
      resultEntities.append(entity)
    _replaceBufferContent(bufferNumber, results[1])

  if do_restore_prev:
    vim.command("{0}b".format(cur_buffer.number))
  else:
    vim.command("wincmd q")

  vim.Function('setqflist')(resultEntities, 'r')
  vim.command("copen")

def undoReplace():
  filesResults = FindAndReplace.undoReplace();
  if filesResults is None:
    vim.command('echomsg "{0}"'.format(FindAndReplace.getLastError()))
    return
  resultEntities = []

  cur_buffer = vim.current.buffer
  cur_window = vim.current.window
  do_restore_prev = True
  if (cur_buffer.options['buftype'] != ''
     or cur_buffer.options['bufhidden'] != ''):
     do_restore_prev = False
     vim.command("silent! keepjumps keepalt new")

  for filePath, fileContent in filesResults.iteritems():
    bufferNumber = _getBuffer(filePath)
    if bufferNumber < 0:
      bufferNumber = _openFileInVim(filePath)
    _replaceBufferContent(bufferNumber, fileContent)

  if do_restore_prev:
    vim.command("{0}b".format(cur_buffer.number))
  else:
    vim.command("wincmd q")
