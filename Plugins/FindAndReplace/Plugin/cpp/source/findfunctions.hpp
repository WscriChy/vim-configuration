#ifndef FindAndfind_findfunctions_hpp
#define FindAndfind_findfunctions_hpp

#include "details.hpp"

namespace FindAndReplace {
namespace Find {
struct Position {
  Position(std::size_t lineNumber_,
           std::size_t columnNumber_,
           std::string occurrence_) :

    lineNumber(lineNumber_),
    columnNumber(columnNumber_),
    occurrence(occurrence_) {}

  std::size_t lineNumber;
  std::size_t columnNumber;
  std::string occurrence;
};

struct FileResult {
  FileResult(std::string const& path_)
    : path(path_) {}

  std::string           path;
  std::vector<Position> positions;
};

inline int
parseArguments(PyObject*                             args,
               std::regex&                           pattern,
               std::regex&                           pathFilter,
               std::vector<boost::filesystem::path>& pathes) {
  namespace fs = boost::filesystem;
  char*     command;
  PyObject* pyPathes;

  if (!PyArg_ParseTuple(args, "sO",
                        &command,
                        &pyPathes)) {
    LastError::setError("Was not able to parse Python arguments");

    return -1;
  }

  auto result = parsePythonList(pyPathes, pathes);

  if (result < 0) {
    return result;
  }

  static std::regex const commandRegEx(
    "((?:(?:(?:\\\\\\\\)*\\\\/)|(?:[^\\\\/])|(?:[\\\\]+[^/]))*)"
    "(?:/(?:"
    "((?:(?:(?:\\\\\\\\)*\\\\/)|(?:[^\\\\/])|(?:[\\\\]+[^/]))*)"
    "(?:/(?:"
    "(.*)"
    ")?)?)?)?",
    std::regex_constants::optimize);

  std::cmatch commandMatch;

  if (!std::regex_match(command, commandMatch, commandRegEx)) {
    LastError::setError("Failed to match the input command to the pattern");

    return -1;
  }

  if (commandMatch.size() >= 3 &&
      !commandMatch[2].str().empty()) {
    try {
      pattern = compileRegExpWithFlags(
        eliminateSlash(commandMatch[1].str()),
        eliminateSlash(commandMatch[2].str()));
    } catch (std::regex_error const& e) {
      LastError::setError(
        "Failed to parse the find regular expression with flags");

      return -1;
    }
  } else {
    try {
      pattern = eliminateSlash(commandMatch[1].str());
    } catch (std::regex_error const& e) {
      LastError::setError("Failed to parse the find regular expression");

      return -1;
    }
  }

  if (commandMatch.size() == 4 && commandMatch[3].length() > 0) {
    try {
      pathFilter = commandMatch[3].str();
    } catch (std::regex_error const& e) {
      LastError::setError(
        "Failed to parse the file path filter regular expression");

      return -1;
    }
  } else {
    pathFilter = ".*";
  }

  return 0;
}

inline void
process(std::regex const&              pattern,
        std::mutex&                    globalMutex,
        std::vector<FileResult*>&      globalResults,
        boost::filesystem::path const& filePath) {
  auto file = File(filePath);

  if (!file.read()) {
    return;
  }

  static std::regex const newLinePattern("\n");

  auto beginIterator = std::cregex_iterator(file.data,
                                            file.data + file.size,
                                            pattern);
  auto endIterator = std::cregex_iterator();
  auto it          = beginIterator;

  if (it == endIterator) {
    return;
  }

  auto newLineBeginIterator = std::cregex_iterator(file.data,
                                                   file.data + file.size,
                                                   newLinePattern);
  auto newLineEndIterator = std::cregex_iterator();
  auto newLineIt          = newLineBeginIterator;

  std::size_t lineNumber      = 1;
  std::size_t newLineStart    = 0;
  std::size_t newLinePosition = 0;

  if (newLineIt == newLineEndIterator) {
    newLineStart = std::numeric_limits<std::size_t>::max();
  } else {
    newLineStart = (*newLineIt).position();
  }

  auto result = new FileResult(filePath.string());

  while (true) {
    std::cmatch const& match = *it;

    while (newLineStart < match.position()) {
      ++lineNumber;
      ++newLineIt;
      newLinePosition = newLineStart + 1;

      if (newLineIt == newLineEndIterator) {
        newLineStart = std::numeric_limits<std::size_t>::max();
      } else {
        newLineStart = (*newLineIt).position();
      }
    }

    result->positions.emplace_back(
      lineNumber,
      1 + match.position() - newLinePosition,
      match.str());

    ++it;

    if (it == endIterator) {
      break;
    }
  }

  std::unique_lock<std::mutex> lock(globalMutex);
  globalResults.push_back(result);
}
}
}

#endif
