#ifndef FindAndReplace_ReplaceProcessing_hpp
#define FindAndReplace_ReplaceProcessing_hpp

#include <regex>
#include <string>

namespace FindAndReplace {
class ReplaceProcessing {
public:
public:

  ReplaceProcessing(std::regex const&              pattern,
        std::string const&             replacement)
    : _pattern(pattern),
    _replacement(replacement) {
    }

  ~ReplaceProcessing() {}

  process(

private:
  std::regex const& _pattern;
  std::string const& _replacement;
};
}

#endif
