#ifndef FindAndReplace_ignorepatternfunctions_hpp
#define FindAndReplace_ignorepatternfunctions_hpp

#include "LastError.hpp"

#include <Python.h>

#include <regex>

namespace FindAndReplace {
extern std::string ignorePatternString;
extern std::regex  ignorePattern;

inline int
parseArguments(PyObject* args) {
  char* ignorePatternChars;

  if (!PyArg_ParseTuple(args, "s",
                        &ignorePatternChars)) {
    LastError::setError("Was not able to parse Python arguments");

    return -1;
  }

  try {
    ignorePattern = ignorePatternChars;
  } catch (std::regex_error const& e) {
    LastError::setError("Failed to parse the ignore regular expression");

    return -1;
  }

  ignorePatternString = ignorePatternChars;

  return 0;
}
}

#endif
