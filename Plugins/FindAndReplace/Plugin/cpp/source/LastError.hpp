#ifndef FindAndReplace_LastError_hpp
#define FindAndReplace_LastError_hpp

#include <string>

namespace FindAndReplace {
class LastError {
public:
  static std::string
  getError() {
    return get()._error;
  }

  static void
  setError(std::string const& error) {
    get()._error = error;
  }

private:
  LastError() {}

  LastError(LastError const& other)
    : _error(other._error) {}

  LastError&
  operator=(LastError const& other) {
    _error = other._error;

    return *this;
  }

  static LastError&
  get() {
    static LastError instance;

    return instance;
  }

  std::string _error;
};
}

#endif
