#ifndef FindAndReplace_TaskPool_hpp
#define FindAndReplace_TaskPool_hpp

#include <boost/filesystem.hpp>

#include <condition_variable>
#include <functional>
#include <mutex>
#include <queue>
#include <thread>
#include <vector>

#include <iostream>

namespace FindAndReplace {
class TaskArguments {
public:
  typedef boost::filesystem::path Path;

public:
  TaskArguments() : isValid(false) {}

  TaskArguments(Path const& path_)
    : path(path_),
      isValid(true) {}

  TaskArguments(TaskArguments const& other)
    : path(other.path),
      isValid(other.isValid) {}

  ~TaskArguments() {}

  TaskArguments&
  operator=(TaskArguments const& other) {
    path    = other.path;
    isValid = other.isValid;

    return *this;
  }

  Path path;
  bool isValid;
};

class TaskPool {
public:
  typedef std::thread                       Thread;
  typedef std::vector<Thread>               Threads;
  typedef boost::filesystem::path           Path;
  typedef std::function<void (Path const&)> Task;

public:
  TaskPool()
    : _concurentThreadsSupported(std::thread::hardware_concurrency()) {
    if (_concurentThreadsSupported == 0) {
      _concurentThreadsSupported = 2;
    }
    // std::cout << "Number of threads is "
    // << _concurentThreadsSupported << std::endl;

    for (int i = 0; i < _concurentThreadsSupported; ++i) {
      _threads.emplace_back(runInThread, this);
    }
  }

  TaskPool(TaskPool const&) = delete;

  ~TaskPool() {}

  TaskPool&
  operator=(TaskPool const&) = delete;

  void
  task(Task const& task) {
    _task = task;
  }

  void
  addTask(Path const& path) {
    std::unique_lock<std::mutex> lock(_mutex);
    _queue.push(TaskArguments(path));
    lock.unlock();
    _condition.notify_one();
  }

  void
  join() {
    std::unique_lock<std::mutex> lock(_mutex);

    for (int i = 0; i < _threads.size(); ++i) {
      _queue.push(TaskArguments());
      _condition.notify_one();
    }
    lock.unlock();

    for (int i = 0; i < _threads.size(); ++i) {
      _threads[i].join();
    }
  }

  void
  finish() {
    std::unique_lock<std::mutex> lock(_mutex);

    for (int i = 0; i < _queue.size(); ++i) {
      _queue.pop();
    }

    for (int i = 0; i < _threads.size(); ++i) {
      _queue.push(TaskArguments());
      _condition.notify_one();
    }
    lock.unlock();

    for (int i = 0; i < _threads.size(); ++i) {
      _threads[i].join();
    }
  }

private:
  TaskArguments
  getTask() {
    std::unique_lock<std::mutex> lock(_mutex);

    while (_queue.empty()) {
      _condition.wait(lock);
    }

    TaskArguments arguments = _queue.front();
    _queue.pop();

    lock.unlock();

    return arguments;
  }

  static void
  runInThread(TaskPool* pool) {
    while (true) {
      auto arguments = pool->getTask();

      if (!arguments.isValid) {
        break;
      }
      pool->_task(arguments.path);
    }
  }

  unsigned                  _concurentThreadsSupported;
  std::mutex                _mutex;
  std::condition_variable   _condition;
  std::queue<TaskArguments> _queue;
  Task                      _task;
  Threads                   _threads;
};
}

#endif
