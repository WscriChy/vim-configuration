#ifndef FindAndReplace_replacefunctions_hpp
#define FindAndReplace_replacefunctions_hpp

#include "details.hpp"

#include "ignorepatternfunctions.hpp"

#include <boost/locale/formatting.hpp>

#include <iterator>

namespace FindAndReplace {
namespace Replace {
extern std::vector<std::unique_ptr<File>> files;

struct Position {
  Position(std::size_t lineNumber_,
           std::size_t columnNumber_,
           std::string replacement_) :

    lineNumber(lineNumber_),
    columnNumber(columnNumber_),
    replacement(replacement_) {}

  std::size_t lineNumber;
  std::size_t columnNumber;
  std::string replacement;
};

struct FileResult {
  FileResult(std::string const& path_)
    : path(path_) {}

  std::string              path;
  std::vector<Position>    positions;
  std::vector<std::string> fileContent;
};

inline int
parseArguments(PyObject*                             args,
               std::regex&                           pattern,
               std::string&                          replacement,
               std::regex&                           pathFilter,
               std::vector<boost::filesystem::path>& pathes) {
  namespace fs = boost::filesystem;
  using boost::locale::format;

  char*     command;
  PyObject* pyPathes;

  if (!PyArg_ParseTuple(args, "sO",
                        &command,
                        &pyPathes)) {
    LastError::setError("Was not able to parse Python arguments");

    return -1;
  }

  auto result = parsePythonList(pyPathes, pathes);

  if (result < 0) {
    return result;
  }

  static std::regex const commandRegEx(
    "((?:(?:(?:\\\\\\\\)*\\\\/)|(?:[^\\\\/])|(?:[\\\\]+[^/]))*)"
    "/"
    "((?:(?:(?:\\\\\\\\)*\\\\/)|(?:[^\\\\/])|(?:[\\\\]+[^/]))*)"
    "(?:/(?:"
    "((?:(?:(?:\\\\\\\\)*\\\\/)|(?:[^\\\\/])|(?:[\\\\]+[^/]))*)"
    "(?:/(?:"
    "(.*)"
    ")?)?)?)?",
    std::regex_constants::optimize);

  std::cmatch commandMatch;

  if (!std::regex_match(command, commandMatch, commandRegEx)) {
    LastError::setError("Failed to match the input command to the pattern");

    return -1;
  }

  replacement = eliminateSlash(commandMatch[2].str());

  if (commandMatch.size() >= 4 && commandMatch[3].length() > 0) {
    try {
      pattern = compileRegExpWithFlags(
        eliminateSlash(commandMatch[1].str()),
        eliminateSlash(commandMatch[3].str()));
    } catch (std::regex_error const& e) {
      LastError::setError(
        "Failed to parse the find regular expression with flags");

      return -1;
    }
  } else {
    try {
      pattern = eliminateSlash(commandMatch[1].str());
    } catch (std::regex_error const& e) {
      LastError::setError("Failed to parse the find regular expression");

      return -1;
    }
  }

  if (commandMatch.size() == 5 && commandMatch[4].length() > 0) {
    auto path_filter_str = commandMatch[4].str();
    try {
      pathFilter = path_filter_str;
    } catch (std::regex_error const& e) {
      LastError::setError(
        (format(
           "Failed to parse the file path filter regular expression "
           "'{1}' with error: {2}") % path_filter_str % e.code()).str());

      return -1;
    }
  } else {
    pathFilter = ".*";
  }

  return 0;
}

inline void
handleNewLines(std::string const&        piece,
               std::size_t&              newLineNumber,
               std::string&              suffix,
               std::vector<std::string>& whole) {
  static std::regex const newLinePattern("(?:\r\n|\n)");

  auto it = std::sregex_iterator(piece.begin(),
                                 piece.end(),
                                 newLinePattern);
  auto endIterator = std::sregex_iterator();

  int tempNewLineNumber = 0;

  if (it == endIterator) {
    suffix += piece;

    return;
  }

  while (true) {
    ++tempNewLineNumber;
    std::smatch match = *it;
    std::string prefix(match.prefix());

    if (tempNewLineNumber == 1) {
      if (!suffix.empty()) {
        whole.push_back(suffix + prefix);
      } else {
        whole.push_back(prefix);
      }
    } else {
      whole.push_back(prefix);
    }
    ++it;

    if (it == endIterator) {
      suffix = match.suffix();
      break;
    }
  }

  newLineNumber += tempNewLineNumber;
}

inline void
process(std::regex const&              pattern,
        std::string const&             replacement,
        std::mutex&                    globalMutex,
        std::vector<FileResult*>&      globalResults,
        boost::filesystem::path const& filePath) {
  auto file = std::unique_ptr<File>(new File(filePath));

  if (!file->read()) {
    return;
  }

  auto it = std::cregex_iterator(file->data,
                                 file->data + file->size,
                                 pattern);
  auto endIterator = std::cregex_iterator();

  if (it == endIterator) {
    return;
  }

  auto result = new FileResult(filePath.string());

  std::size_t lineNumber = 1;
  std::string suffix;

  while (true) {
    std::cmatch match = *it;

    handleNewLines(match.prefix(),
                   lineNumber,
                   suffix,
                   result->fileContent);

    std::string replacementString(match.format(replacement));

    result->positions.emplace_back(
      lineNumber,
      suffix.size() + 1,
      replacementString);

    handleNewLines(replacementString,
                   lineNumber,
                   suffix,
                   result->fileContent);

    ++it;

    if (it == endIterator) {
      handleNewLines(match.suffix(),
                     lineNumber,
                     suffix,
                     result->fileContent);

      if (!suffix.empty()) {
        result->fileContent.push_back(suffix);
      }
      break;
    }
  }

  files.emplace_back(std::move(file));

  std::unique_lock<std::mutex> lock(globalMutex);
  globalResults.push_back(result);
}

inline PyObject*
undo() {
  PyObject* pyResults = PyDict_New();

  for (auto const& file : files) {
    PyObject* pyNewFileContent = PyList_New(0);

    static std::regex const nLinePattern("(?:(?!\r)\n|\r\n)",
                                         std::regex_constants::optimize);

    auto nLineIterator = std::cregex_iterator(
      file->data,
      file->data + file->size,
      nLinePattern);
    auto nLineEndIterator = std::cregex_iterator();

    if (nLineIterator == nLineEndIterator) {
      PyList_Append(
        pyNewFileContent,
        PyString_FromString(file->data));
    } else {
      while (true) {
        std::cmatch const& match = *nLineIterator;
        auto last_match_suffix_str = match.suffix().str();

        PyList_Append(pyNewFileContent,
                      PyString_FromString(match.prefix().str().c_str()));

        if (++nLineIterator == nLineEndIterator) {
          if (!last_match_suffix_str.empty()) {
            PyList_Append(pyNewFileContent,
                          PyString_FromString(last_match_suffix_str.c_str()));
          }
          break;
        }
      }
    }
    PyDict_SetItemString(
      pyResults,
      file->path.string().c_str(),
      pyNewFileContent);
  }

  return pyResults;
}
}
}

#endif
