#ifndef FindAndReplace_details_hpp
#define FindAndReplace_details_hpp

#include "LastError.hpp"
#include "TaskPool.hpp"
#include "ignorepatternfunctions.hpp"

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/locale.hpp>

#include <Python.h>

#include <iostream>
#include <limits>
#include <map>
#include <regex>
#include <string>

#include <iostream>

namespace FindAndReplace {
struct File {
  File(boost::filesystem::path const& path_) : path(path_),
    data(nullptr), size(0) {}

  File(boost::filesystem::path const& path_,
       char*                          data_,
       std::size_t const&             size_) : path(path_),
    data(data_),
    size(size_) {}
  ~File() {
    delete data;
  }

  bool
  read() {
    namespace fs = boost::filesystem;

    fs::ifstream fileStream(path, std::ios_base::binary);

    if (!fileStream) {
      LastError::setError("Failed to open file " + path.string());

      return false;
    }

    std::filebuf* fileBuffer = fileStream.rdbuf();

    size = fileBuffer->pubseekoff(0,
                                  fileStream.end,
                                  fileStream.in);

    if (size == 0) {
      return false;
    }

    fileBuffer->pubseekpos(0, fileStream.in);

    data = new char[size];

    fileBuffer->sgetn(data, size);

    return true;
  }

  boost::filesystem::path path;
  char*                   data;
  std::size_t             size;
};

inline std::string
eliminateSlash(std::string const& str) {
  static std::regex const eliminateSlashRegEx("\\\\/",
                                              std::regex_constants::optimize);

  return std::regex_replace(str, eliminateSlashRegEx, "/");
}

inline std::regex
compileRegExpWithFlags(
  std::string const& reg_exp_str,
  std::string const& flags_str) {
  using FlagType = typename std::regex::flag_type;
  FlagType flags;

  bool was_grammar_assigned = false;
  // Value  Effect(s)
  // icase  Character matching should be performed without regard to case.
  // nosubs   When performing matches, all marked sub-expressions (expr) are
  // treated as non-marking sub-expressions (?:expr). No matches are stored in
  // the supplied std::regex_match structure and mark_count() is zero
  // optimize   Instructs the regular expression engine to make matching faster,
  // with the potential cost of making construction slower. For example, this
  // might mean converting a non-deterministic FSA to a deterministic FSA.
  // collate  Character ranges of the form "[a-b]" will be locale sensitive.
  // ECMAScript   Use the Modified ECMAScript regular expression grammar
  // basic  Use the basic POSIX regular expression grammar (grammar
  // documentation).
  // extended   Use the extended POSIX regular expression grammar (grammar
  // documentation).
  // awk  Use the regular expression grammar used by the awk utility in POSIX
  // (grammar documentation)
  // grep   Use the regular expression grammar used by the grep utility in
  // POSIX. This is effectively the same as the basic option with the addition
  // of newline '\n' as an alternation separator.
  // egrep  Use the regular expression grammar used by the grep utility, with
  // the -E option, in POSIX. This is effectively the same as the extended
  // option with the addition of newline '\n' as an alternation separator in
  // addtion to '|'.
  std::map<char, FlagType> flag_map = {
    { 'a', std::regex_constants::awk      },
    { 'b', std::regex_constants::basic    },
    { 'c', std::regex_constants::collate  },
    { 'e', std::regex_constants::extended },
    { 'g', std::regex_constants::grep     },
    { 'i', std::regex_constants::icase    },
    { 'n', std::regex_constants::nosubs   },
    { 'r', std::regex_constants::egrep    }
  };

  for (auto it = flags_str.begin(); it != flags_str.end(); ++it) {
    auto find_it = flag_map.find(*it);

    if (find_it != flag_map.end()) {
      flags |= find_it->second;

      for (auto const& ch : { 'a', 'b', 'e', 'g', 'r' }) {
        if (find_it->first == ch) {
          was_grammar_assigned = true;
        }
      }
    }
  }

  if (!was_grammar_assigned) {
    flags |= std::regex_constants::ECMAScript;
  }

  flags |= std::regex_constants::optimize;

  return std::regex(reg_exp_str, flags);
}

inline int
parsePythonList(PyObject*                             pyList,
                std::vector<boost::filesystem::path>& pathes) {
  PyObject* iter = PyObject_GetIter(pyList);

  if (!iter) {
    LastError::setError("Was not able to obtain iterator of the argument list");

    return -1;
  }

  while (true) {
    PyObject* next = PyIter_Next(iter);

    if (!next) {
      break;
    }

    if (PyString_Check(next)) {
      pathes.emplace_back(PyString_AsString(next));
      pathes.back().make_preferred();
    }
  }

  return 0;
}

inline int
createRegExFromChars(char const* string,
                     std::regex& regex) {
  try {
    regex = string;
  } catch (std::regex_error const& e) {
    LastError::setError("Failed to parse the regular expression");

    return -1;
  }

  return 0;
}

inline int
checkFilePath(std::regex const&  pathFilter,
              std::string const& filePath) {
  if (std::regex_match(filePath, pathFilter)) {
    return 1;
  }

  return 0;
}

inline void
runTasks(TaskPool&                                   pool,
         std::vector<boost::filesystem::path> const& pathes,
         std::regex const&                           pathFilter) {
  namespace fs = boost::filesystem;

  for (auto const& currentPath : pathes) {
    auto fileType =  fs::status(currentPath).type();

    if (fileType == fs::file_type::file_not_found
        || fileType == fs::file_type::status_error
        || fileType == fs::file_type::status_unknown
        || fileType == fs::file_type::type_unknown) {
      continue;
      //
    }

    if (fileType == fs::file_type::directory_file) {
      auto it  = fs::recursive_directory_iterator(currentPath);
      auto end = fs::recursive_directory_iterator();

      for (; it != end; ++it) {
        fileType = it->status().type();

        if (fileType != fs::file_type::directory_file) {
          if (checkFilePath(pathFilter, it->path().string()) != 0
              && checkFilePath(ignorePattern, it->path().string()) != 1) {
            pool.addTask(it->path());
          }
        }
      }
    } else {
      if (checkFilePath(pathFilter, currentPath.string()) != 0
          && checkFilePath(ignorePattern, currentPath.string()) != 1) {
        pool.addTask(currentPath);
      }
    }
  }
  pool.join();
}
}

#endif
