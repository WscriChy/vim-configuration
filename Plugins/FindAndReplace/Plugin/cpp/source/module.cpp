#include "LastError.hpp"
#include "TaskPool.hpp"
#include "findfunctions.hpp"
#include "ignorepatternfunctions.hpp"
#include "replacefunctions.hpp"

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/locale.hpp>

#include <Python.h>

#include <iostream>
#include <limits>
#include <mutex>
#include <regex>
#include <string>

#ifdef __cplusplus
extern "C" {
#endif
static PyObject*
setIgnorePattern(PyObject* self, PyObject* args) {
  int result = FindAndReplace::parseArguments(
    args);
  Py_RETURN_NONE;
}

static PyObject*
getIgnorePattern(PyObject* self, PyObject* args) {
  return PyString_FromString(FindAndReplace::ignorePatternString.c_str());
}

static PyObject*
find(PyObject* self, PyObject* args) {
  namespace fs = boost::filesystem;
  std::locale::global(boost::locale::generator().generate("en_US.UTF-8"));
  fs::path::imbue(std::locale());

  std::regex                           pattern;
  std::regex                           pathFilter;
  std::vector<boost::filesystem::path> pathes;

  int result = FindAndReplace::Find::parseArguments(
    args,
    pattern,
    pathFilter,
    pathes);

  if (result < 0) {
    Py_RETURN_NONE;
  }

  FindAndReplace::TaskPool pool;

  std::mutex                                     globalMutex;
  std::vector<FindAndReplace::Find::FileResult*> results;

  pool.task(
    FindAndReplace::TaskPool::Task(
      [&] (fs::path const& filePath) {
        FindAndReplace::Find::process(
          pattern,
          globalMutex,
          results,
          filePath);
      }));

  runTasks(pool,
           pathes,
           pathFilter);

  PyObject* pyResults = PyDict_New();

  for (auto const& result : results) {
    PyObject* pyEntities = PyList_New(0);

    for (auto const& position : result->positions) {
      PyObject* entity = PyDict_New();
      PyDict_SetItemString(
        entity,
        "lnum",
        PyInt_FromSize_t(position.lineNumber));
      PyDict_SetItemString(
        entity,
        "col",
        PyInt_FromSize_t(position.columnNumber));
      PyDict_SetItemString(
        entity,
        "text",
        PyString_FromString(position.occurrence.c_str()));
      PyList_Append(pyEntities, entity);
    }

    PyDict_SetItemString(
      pyResults,
      result->path.c_str(),
      pyEntities);

    delete result;
  }

  return pyResults;
}

static PyObject*
replace(PyObject* self, PyObject* args) {
  namespace fs = boost::filesystem;
  std::locale::global(boost::locale::generator().generate("en_US.UTF-8"));
  fs::path::imbue(std::locale());

  std::regex                           pattern;
  std::string                          replacement;
  std::regex                           pathFilter;
  std::vector<boost::filesystem::path> pathes;

  int result = FindAndReplace::Replace::parseArguments(
    args,
    pattern,
    replacement,
    pathFilter,
    pathes);

  if (result < 0) {
    Py_RETURN_NONE;
  }

  FindAndReplace::TaskPool pool;

  std::mutex                                        globalMutex;
  std::vector<FindAndReplace::Replace::FileResult*> results;

  pool.task(
    FindAndReplace::TaskPool::Task(
      [&] (fs::path const& filePath) {
        FindAndReplace::Replace::process(
          pattern,
          replacement,
          globalMutex,
          results,
          filePath);
      }));

  FindAndReplace::Replace::files.clear();

  runTasks(pool,
           pathes,
           pathFilter);

  PyObject* pyResults = PyDict_New();

  for (auto const& result : results) {
    PyObject* pyEntities = PyList_New(0);

    for (auto const& position : result->positions) {
      PyObject* entity = PyDict_New();
      PyDict_SetItemString(
        entity,
        "lnum",
        PyInt_FromSize_t(position.lineNumber));
      PyDict_SetItemString(
        entity,
        "col",
        PyInt_FromSize_t(position.columnNumber));
      PyDict_SetItemString(
        entity,
        "text",
        PyString_FromString(position.replacement.c_str()));
      PyList_Append(pyEntities, entity);
    }

    PyObject* pyNewFileContent = PyList_New(0);

    for (auto const& line : result->fileContent) {
      PyList_Append(
        pyNewFileContent,
        PyString_FromString(line.c_str()));
    }

    PyObject* pyResult = PyList_New(0);
    PyList_Append(pyResult, pyEntities);
    PyList_Append(pyResult, pyNewFileContent);
    PyDict_SetItemString(
      pyResults,
      result->path.c_str(),
      pyResult);

    delete result;
  }

  return pyResults;
}

static PyObject*
undoReplace(PyObject* self, PyObject* args) {
  return FindAndReplace::Replace::undo();
}

static PyObject*
getLastError(PyObject* self, PyObject* args) {
  PyObject* error = PyString_FromString(
    FindAndReplace::LastError::getError().c_str());

  return error;
}

static PyMethodDef FindAndReplaceMethods[] =
{
  { "setIgnorePattern",
    setIgnorePattern,
    METH_VARARGS,
    "Set ignore pattern for find and replace operations" },
  { "getIgnorePattern",
    getIgnorePattern,
    METH_VARARGS,
    "Return ignore pattern for find and replace operations" },
  { "find",
    find,
    METH_VARARGS,
    "Find the pattern in files" },
  { "replace",
    replace,
    METH_VARARGS,
    "Replace the search pattern in files" },
  { "undoReplace",
    undoReplace,
    METH_VARARGS,
    "Undo the last replacement operation" },
  { "getLastError",
    (PyCFunction)getLastError,
    METH_NOARGS,
    "Returen the last error" },
  { NULL,              NULL,0, NULL }
};

PyMODINIT_FUNC
initFindAndReplace(void) {
  (void)Py_InitModule("FindAndReplace", FindAndReplaceMethods);
}

#ifdef __cplusplus
}
#endif
