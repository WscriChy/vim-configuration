#! usr/bin/env python
#  encoding: utf-8

import os
import sys
import inspect

currentDir = os.path.dirname(os.path.realpath(
    os.path.abspath(inspect.getfile(inspect.currentframe()))))

sys.path.append(os.path.join(currentDir, "../.build/Release"))

import FindAndReplace

FindAndReplace.setIgnorePattern(
    "[^.]*\.(?:py[odc]|exe|dll|so|lib|a|obj|o|d"
    "|pdf|djvu|djv"
    "|mp3|ogg"
    "|avi|mp4|mkv"
    "|7z|zip|tar.*|bz|gz|xz|out|log|rar|db"
    "|png|jpg|jpeg|ico|tiff|gif|svg)"
    )

entities = FindAndReplace.replace(
    "s/wow/",
    ["D:/vim/VimRC.vim"]
    );
print("Replace")
print("===================================")
if entities is None:
  print(FindAndReplace.getLastError())
else:
  for key, value in entities.iteritems():
    print(key)
    #print(value[0])
    print(value[1])
    #result = ''
    #for piece in value[1]:
    #  result += piece + "\n"
    #print(result)
print("===================================")
entities = FindAndReplace.replace(
    "s/wow/",
    ["D:/vim/VimRC.vim"]
    );
for key, value in entities.iteritems():
   #print(key)
   #print(value[0])
   print(value[1])

entities = FindAndReplace.find(
    "something",
    ["D:/vim"]
    );
print("Find")
print("===================================")
if entities is None:
  print(FindAndReplace.getLastError())
#else:
  #for key in entities:
  #  print(key)
print("===================================")

