#!/usr/bin/env python
#encoding: utf-8

from Configurator import *

set("g:ycm_auto_trigger", 1)
set("g:ycm_allow_changing_updatetime", 0)
set("g:ycm_min_num_of_chars_for_completion", 1)
set("g:ycm_complete_in_strings", 1)
set("g:ycm_complete_in_comments", 1)
# By default, YCM's filepath completion will interpret relative paths like '../'
# as being relative to the folder of the file of the currently active buffer.
# Setting this option will force YCM to always interpret relative paths as being
# relative to Vim's current working directory.
set("g:ycm_filepath_completion_use_working_dir", 0)
# When this option is set to '1' YCM will ask once per '.ycm_extra_conf.py' file
# if it is safe to be loaded. This is to prevent execution of malicious code from
# a '.ycm_extra_conf.py' file you didn't write.
set("g:ycm_confirm_extra_conf", 0)
set("g:ycm_use_ultisnips_completer", 1)
set("g:ycm_disable_for_files_larger_than_kb", 10000)
set("g:ycm_server_log_level", "'critical'")
set("g:ycm_filetype_blacklist", {
  'BuffersLine' : 1,
  'tagbar' : 1,
  'svn' : 1,
  'unite' : 1,
  'nerdtree' : 1
})

set("g:ycm_filetype_whitelist", {
  '*': 1
})

def setKeyBindings():
  map("<F7>",
      ":<C-U>YcmDiags<Cr>",
      Normal, Visual, Select, NoRemap, Silent, Buffer)
  map("<F7>",
      "<C-O>:YcmDiags<Cr>",
      Insert, NoRemap, Silent, Buffer)
  map("<F8>",
      ":<C-U>YcmForceCompileAndDiagnostics<Cr>",
      Normal, Visual, Select, NoRemap, Silent, Buffer)
  map("<F8>",
      "<C-O>:YcmForceCompileAndDiagnostics<Cr>",
      Insert, NoRemap, Silent, Buffer)
  map("<Leader>f",
      ":<C-U>YcmShowDetailedDiagnostic<Cr>",
      Normal, NoRemap, Silent, Buffer)
  map("<Leader>c",
      ":YcmCompleter GoTo<Cr>",
      Normal, NoRemap, Silent, Buffer)
  map("<Leader>d",
      ":YcmCompleter GoToDefinition<Cr>",
      Normal, NoRemap, Silent, Buffer)

hook("FileType c,cpp,objc,objcpp,python,cs,typescript,javascript,javascript.jsx", setKeyBindings)
