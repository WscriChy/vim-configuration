" Normally, YCM searches for a .ycm_extra_conf.py file for compilation flags
" (see the User Guide for more details on how this works).
" This option specifies a fallback path to a config file which is used
" if no .ycm_extra_conf.py is found.
" You can place such a global file anywhere in your filesystem.
let g:ycm_global_ycm_extra_conf = g:stuff."/.ycm_extra_conf.py"
" When this option is set to '1', YCM's identifier completer will also collect
" identifiers from strings and comments. Otherwise, the text in comments and
" strings will be ignored.
let g:ycm_collect_identifiers_from_comments_and_strings = 1
" This option controls the character-based triggers for the various semantic
" completion engines. The option holds a dictionary of key-values, where the
" keys are Vim's filetype strings delimited by commas and values are lists of
" strings, where the strings are the triggers.
" let g:ycm_semantic_triggers =  {
" 			\   'c' : ['->', '.', '::'],
" 			\   'cpp' : ['->', '.', '::'],
" 			\   'objc' : ['->', '.'],
" 			\   'ocaml' : ['.', '#'],
" 			\   'cpp,objcpp' : ['->', '.', '::'],
" 			\   'perl' : ['->'],
" 			\   'php' : ['->', '::'],
" 			\   'cs,java,javascript,d,vim,ruby,python,perl6,scala,vb,elixir,go' : ['.'],
" 			\   'lua' : ['.', ':'],
" 			\   'erlang' : [':'],
"  			\ }

let g:ycm_key_invoke_completion = '<C-Space>'
let g:ycm_key_list_select_completion = ['<Down>']
let g:ycm_key_list_previous_completion = ['<S-TAB>', '<Up>']
let g:ycm_add_preview_to_completeopt = 0
let g:ycm_show_diagnostics_ui = 1
let g:ycm_enable_diagnostic_highlighting = 1
let g:ycm_seed_identifiers_with_syntax = 1
let g:ycm_collect_identifiers_from_tags_files = 1

if !exists('g:tab_function')
  exe 'inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"'
else
  exe 'inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : '.g:tab_function
endif
