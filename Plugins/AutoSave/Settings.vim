command! AutoSaveToggle call <SID>AutoSaveToggle()

function! s:AutoSaveToggle()
  if g:auto_save >= 1
    if exists("b:auto_save")
      let b:auto_save = !b:auto_save
    else
      let b:auto_save = 0
    endif
  endif
endfunction

command! AutoSaveDisableToggle call <SID>AutoSaveDisableToggle()

function! s:AutoSaveDisableToggle()
  if exists("g:auto_save_disable")
    let g:auto_save_disable = !g:auto_save_disable
  else
    let g:auto_save_disable = 1
  endif
endfunction
