#!/usr/bin/env python
# encoding: utf-8

from Configurator import *

set("g:auto_save", 1)
set("g:auto_save_in_insert_mode", 1)
set("g:auto_save_no_updatetime", 0)
set("g:auto_save_silent", 1)
