if exists("g:auto_save_loaded")
  finish
else
  let g:auto_save_loaded = 1
endif

let s:save_cpo = &cpo
set cpo&vim

if !exists("g:auto_save")
  let g:auto_save = 0
endif

if !exists("g:auto_save_in_insert_mode")
  let g:auto_save_in_insert_mode = 1
endif

if !exists("g:auto_save_silent")
  let g:auto_save_silent = 0
endif

augroup auto_save
  autocmd!

  au FocusLost * call AutoSave()
  au WinLeave  * call AutoSave()
augroup END


let g:auto_save_disable = 0

function! AutoSave()
  if mode() !=# 'n'
    return
  endif
  if &ro!=0 || &bl!=1 || &bt!=''
    return
  endif
  if exists("b:auto_save")
    if !b:auto_save
      return
    endif
  endif
  if g:auto_save_disable | return | endif
  keepalt keepjump silent! call feedkeys(":if &ro==0 && &bl==1 && &bt==''|silent! w|endif\<CR>:\<C-U>\<CR>")
endfunction

let &cpo = s:save_cpo
unlet s:save_cpo
