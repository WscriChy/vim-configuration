#!/usr/bin/env python -OO
#encoding: utf-8

from Configurator import *

def setup():
  #command("TagbarOpen")
  map("<F9>",
      ":<C-U>TagbarOpenAutoClose<Cr>",
      Normal, Visual, Select, NoRemap, Silent, Buffer)
  map("<F9>",
      "<C-O>:TagbarOpenAutoClose<Cr>",
      Insert, NoRemap, Silent, Buffer)

setGlobal("tagbar_sort", 0)
setGlobal("tagbar_show_linenumbers", 0)

hook("FileType c,cpp,objc,objcpp,python,cs,vim,tex,slice,php,javascript", setup)


# Need to remove '--extra=' from argument string in autoload/tagbar.vim
