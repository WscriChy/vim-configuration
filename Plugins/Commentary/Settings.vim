autocmd FileType apache setlocal commentstring=#\ %s
autocmd FileType cmake  setlocal commentstring=#\ %s
autocmd FileType c,cpp  setlocal commentstring=//\ %s
