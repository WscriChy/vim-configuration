" cabbrev e <c-r>=(getcmdtype()==':' && getcmdpos()==1 ? 'E' : 'e')<CR>

augroup AltCmdSettingsExecuteCommandsGroup
  au!
  au VimEnter * Alias q qall
  au VimEnter * Alias wq wqall
augroup END
