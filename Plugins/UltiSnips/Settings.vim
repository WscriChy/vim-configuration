let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"
let g:UltiSnipsListSnippets="<c-h>"

" let g:UltiSnipsSnippetDirectories=["UltiSnips"]
let g:UltiSnipsSnippetDirectories = [g:plugins."/UltiSnipsSnippets/Plugin/UltiSnips"]

let g:UltiSnipsSnippetsDir = g:plugins."/UltiSnipsSnippets/Plugin/UltiSnips"

" let g:UltiSnipsExpandTrigger       = "<c-tab>"
" let g:UltiSnipsListSnippets        = "<c-e>"
" let g:UltiSnipsJumpForwardTrigger  = "<tab>"
" let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"


" " Enable tabbing through list of results
" function! g:UltiSnips_Complete()
"     call UltiSnips#ExpandSnippet()
"     if g:ulti_expand_res == 0
"         if pumvisible()
"             return "\<C-n>"
"         else
"             call UltiSnips#JumpForwards()
"             if g:ulti_jump_forwards_res == 0
"                return "\<TAB>"
"             endif
"         endif
"     endif
"     return ""
" endfunction

" au InsertEnter * exec "inoremap <silent> " . g:UltiSnipsExpandTrigger . " <C-R>=g:UltiSnips_Complete()<cr>"

" " Expand snippet or return
" let g:ulti_expand_res = 0
" function! Ulti_ExpandOrEnter()
"     call UltiSnips#ExpandSnippet()
"     if g:ulti_expand_res
"         return ''
"     else
"         return "\<return>"
" endfunction

" " Set <space> as primary trigger
" inoremap <silent> <return> <C-R>=Ulti_ExpandOrEnter()<CR>
