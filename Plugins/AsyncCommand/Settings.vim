command! -nargs=* Exe call s:AsyncExe(<q-args>)
" command! -nargs=* Run call s:AsyncRun(<q-args>)
command! -nargs=+ -complete=file -complete=shellcmd -bang Shell
      \ call asynccommand#run(<q-args>, asynchandler#split(<bang>0))

let s:sed_control_character_filter = "sed -r \"s/\\x1B\\[([0-9]{1,2}(;[0-9]{1,2})*)?[m|K]//g\""
" let s:sed_test_trim = "sed -r 's/^\\/usr\\/home/\\/home/g'"
let s:sed_test_trim = "sed -r 's/^..\\/..\\/..\\///g'"

" function! asynchandler#split()
"     function env.get(temp_file_name) dict
"         if self.is_const_preview
"             exec "pedit " . a:temp_file_name
"             wincmd P
"             setlocal nomodifiable
"             silent! nnoremap <unique> <buffer> q :bdelete<CR>
"         else
"             exec "split " . a:temp_file_name
"         endif
"         silent! wincmd p
"     endfunction
"     return asynccommand#tab_restore(env)
" endfunction

function! s:AsyncExe(target)
    let make_cmd = g:ExeCmd." 2>&1 | ".s:sed_control_character_filter." | ".s:sed_test_trim
    " echom make_cmd
    let title = 'Make [%d]: '
    if a:target == ''
        let title .= "(default)"
    else
        let title .= a:target
    endif
    call asynccommand#run(make_cmd, asynchandler#quickfix(&errorformat, title))
endfunction

function! s:AsyncRun(...)
  let args = ""
  for item in a:000
    args .= " ".item
  endfor
  call asynccommand#run(g:RunCmd.args, s:AsyncRunHandler())
endfunction

function! s:AsyncRunHandler()
  let env = {
        \ 'cmd': "ls -la",
        \}
  function env.get(temp_file_name) dict
    echomsg string(self)
    let buffer_name = self.cmd
    let buffer_name = "Run [".buffer_name."]"
    let is_current = s:ScratchBufferOpen(0, buffer_name)
    exec "read ".a:temp_file_name
    if !is_current
      " silent! wincmd p
    endif
  endfunction
  return asynccommand#tab_restore(env)
endfunction

function! s:ScratchBufferOpen(new_win, ...)
    let split_win = a:new_win

    " If the current buffer is modified then open the scratch buffer in a new
    " window
    if !split_win && &modified
        let split_win = 1
    endif

    let l:name = "Scratch Buffer"
    if a:0 != 0
      let l:name = a:1
    endif
    " Check whether the scratch buffer is already created
    let buffer_result = s:IsOnCurrentTab(l:name)
    if buffer_result.status == 0
        if split_win
          exe "new " . l:name
        else
          exe "edit " . l:name
        endif
    else
        " Scratch buffer is already created. Check whether it is open
        " in one of the windows
        let scr_winnum = bufwinnr(buffer_result.number)
        if scr_winnum != -1
            " Jump to the window which has the scratch buffer if we are not
            " already in that window
            if winnr() != scr_winnum
                exe scr_winnum . "wincmd w"
                return 0
            endif
            return 1
        else
            if split_win
                exe "split +buffer" . buffer_result.number
            else
                exe "buffer " . buffer_result.number
            endif
        endif
    endif
    call s:ScratchMarkBuffer()
    return 0
endfunction

function! s:ScratchMarkBuffer()
    setlocal buftype=nofile
    setlocal bufhidden=hide
    setlocal noswapfile
    setlocal buflisted
    setlocal filetype=Scratch
endfunction

function! s:IsOnCurrentTab(buf_name)
  " Try to determine whether file is open in any tab.
  " Return number of tab it's open in
  let buffername = bufname(a:buf_name)
  if buffername == ""
      return {"num": -1, "status": 0}
  endif
  let buf_num = bufnr(buffername)

  " tabdo will loop through pages and leave you on the last one;
  " this is to make sure we don't leave the current page
  let currenttab = tabpagenr()
  let tab_arr = []
  tabdo let tab_arr += tabpagebuflist()

  " return to current page
  exec "tabnext ".currenttab

  for tnum in tab_arr
      if tnum == buf_num
        return {"num": buf_num, "status": 1}
      endif
  endfor

  return {"num": buf_num, "status": 0}
endfunction

function! g:GuessExeCmd()
  return g:GuessCExeCmd()
endfunction

function! g:GuessCExeCmd()
  let result = &makeprg
  let cmakelists = findfile("CMakeLists.txt", '.;')
  let configure = findfile("configure", '.;')
  let makefile = findfile("Makefile", '%,.;')
  let build = findfile("build", '%,.;')
  let compile = findfile("compile", '%,.;')

  if (empty(compile))
    if (empty(build))
      if (empty(cmakelists))
        if (empty(configure))
          if (empty(makefile))
          else
            let result .= " -f ".fnamemodify(makefile, ':p')
          endif
        else
          let makefile = fnamemodify(configure, ':p:h')."/Makefile"
          let result .= " -f ".makefile
        endif
      else
        let makefile = fnamemodify(cmakelists, ':p:h')."/Makefile"
        let result .= " -f ".makefile
      endif
    else
      let result = fnamemodify(build, ':p')
    endif
  else
    if (empty(build))
      let result = fnamemodify(compile, ':p')
    else
      let build = fnamemodify(build, ':p:h')
      let compile = fnamemodify(compile, ':p:h')
      if (len(build) > len(compile))
        let result = build."/build"
      else
        let result = compile."/compile"
      endif
    endif
  endif

  return result
endfunction

let g:ExeCmd = g:GuessExeCmd()

let &makeprg = g:ExeCmd
let g:RunCmd = "ls -la"
