#!/usr/bin/env python
# encoding: utf-8

from Configurator import *

import vim

set("g:indent_guides_enable_on_vim_startup", 1)
set("g:indent_guides_default_mapping", 0)
set("g:indent_guides_color_change_percent", 4)
set("g:indent_guides_guide_size", 0)
