function! g:VisualSearch_search(cmd)
  let old_reg = getreg('"')
  let old_regtype = getregtype('"')
  normal! gvy
  if @" =~? '^[0-9a-z,_]*$'
    let @/ = @"
  else
    let pat = escape(@", a:cmd.'\')
    let pat = substitute(pat, '^\_s\+', '\\s\\+', '')
    let pat = substitute(pat, '\_s\+$', '\\s\\*', '')
    let pat = substitute(pat, '\_s\+', '\\_s\\+', 'g')
    "let @/ = '\V'.pat
    let @/ = '\V'.pat
  endif
  normal! gV
  call setreg('"', old_reg, old_regtype)
endfunction

"vnoremap <silent> # :<C-U>call <SID>VSetSearch('?')<CR>?<C-R>/<CR>
"vmap <kMultiply> *
