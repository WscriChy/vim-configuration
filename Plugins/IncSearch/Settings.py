#!/usr/bin/env python
# encoding: utf-8

from Configurator import *

import vim

set("g:incsearch#magic", "'\v'")
set("g:incsearch#auto_nohlsearch", 1)
set("g:incsearch#separate_highlight", 1)
set("g:incsearch#consistent_n_direction", 1)
