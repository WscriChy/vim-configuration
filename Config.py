#!/usr/bin/env python
# encoding: utf-8

import sys
import os
from os.path import join
import inspect
import platform
_platform = platform.system()

currentDir = os.path.dirname(os.path.realpath(
  os.path.abspath(inspect.getfile(inspect.currentframe()))))

configuratorPath = join(currentDir, 'Configurator')
sys.path.insert(0, configuratorPath)

from Configurator import *
from utils import normalize

sourcePythonScript(
  "FindAndReplaceInterface",
  os.path.join(
    currentDir,
    "Plugins/FindAndReplace/Plugin/python/FindAndReplaceInterface.py"))

sourcePythonScript(
  "Replace",
  os.path.join(
    currentDir,
    "Plugins/MultiMode/Plugin/Replace.py"))

def sourcePythonScriptFromCurrentDir(name):
  scriptPath = os.path.join(currentDir, "{0}.py".format(name))
  sourcePythonScript(
    "VimConfigurationCore{0}".format(name),
    scriptPath)

plugins = [
  "Abolish",
  "AlterCmd",
  "Asterisk",
  "AsyncCommand",
  "AutoSave",
  "BBye",
  "CEnhancement",
  "CFileSwitch",
  "Commentary",
  # "CsApprox",
  "CtrlP",
  "CtrlPCMatcher",
  "CtrlPExtensions",
  "CtrlPMyExtensions",
  "EasyMotion",
  # "EasyTags",
  "Filer",
  "FindAndReplace",
  # "FixTerminalKeys",
  "Fugitive",
  "vim-gutentags",
  "IncSearch",
  # "IndentGuides",
  # "vim-javascript",
  "yajs.vim",
  "JavascriptEnch",
  "vim-jsx",
  "Mark",
  "Misc",
  "MiscScripts",
  "MultiMode",
  # "Mwm",
  # "NerdTree",
  # "NeoComplCache",
  "Proc",
  "PythonEnhancement",
  "QuickFixEnchancment",
  "RainbowParantheses",
  "RecentBuffersBar",
  "Repeat",
  "Scratch",
  "Surround",
  "TagBar",
  "TextWrapping",
  "TrailingWhitespace",
  "typescript-vim",
  # "UltiSnipsSnippets",
  # "UltiSnips",
  "Uncrustify",
  # "UndoTree",
  "Unite",
  "Ycm"
  ]

# if _platform == "Linux":
#   plugins.remove("Mwm")
pluginRoot = "Plugins"
pluginSourceDirectory = "Plugin"

pathsManager.currentDir(currentDir)

for plugin in plugins:
  pluginSourcePath = join(pluginRoot, plugin, pluginSourceDirectory)
  pathsManager.add(pluginSourcePath)

#if _platform == "Windows":
#  pathsManager.runtime(normalize(currentDir, "bin", "Runtime"))

pathsManager.add("RuntimeUpgrades")
pathsManager.loadPaths()

sourcePythonScriptFromCurrentDir("Settings")

commander.currentDir(currentDir)
source("CommonSettings.vim")
source("Keybindings.vim")
source("Hooks.vim")
sourcePythonScriptFromCurrentDir("Keybindings")
source("Plugins/Settings.vim")
source("RuntimeUpgrades/Settings.vim")

for plugin in plugins:
  pluginRootPath = join(pathsManager.currentDir(), pluginRoot, plugin)
  settingsNameList = ["Settings"]
  for settingsName in settingsNameList:
    sourceVimSettings(pluginRootPath, settingsName)
    sourcePythonSettings(
        pluginRootPath,
        "VimConfigurationPlugin{0}{1}".format(plugin, settingsName),
        settingsName)
