﻿" ============================================================
" Basic Vim
" ============================================================
set nocompatible
filetype off
filetype plugin indent on
syntax on
" let c_minlines=50
" syntax sync minlines=20

" ============================================================
" Colors View Sounds
" ============================================================
" Use visual bell instead of beeping. The terminal code to display the visual bell is 
" given with 't_vb'.  When no beep or flash is wanted, use:
" set noerrorbells novisualbell t_vb=
" Highlight current line
set nocursorline
set nocursorcolumn
set guicursor=n-c-v:block-Cursor-blinkwait1000-blinkon0-blinkoff250,i-ci:ver30-iCursor-blinkwait30000-blinkon200-blinkoff150
" See tabs and spaces
scriptencoding utf-8

set background=dark
" set background=light
" colorscheme hybrid
" colorscheme solarized
colorscheme papercolor

set nonumber
set listchars=tab:☠\ 
set nolist
set scrolloff=20

set guioptions-=m  "remove menu bar
set guioptions-=T  "remove toolbar
set guioptions-=r  "remove right-hand scroll bar
set guioptions-=L  "remove left-hand scroll bar
set guioptions-=e

" ============================================================
" Files & File System Settings
" ============================================================
"set shellslash
" Use both Unix and DOS file formats, but favor the Unix one for new files.
set fileformats=unix,dos
set encoding=utf-8
set fileencodings=utf-8,ucs-bom,latin1,iso-8859-5

" ============================================================
" Indentation
" ============================================================
" set smartindent
set tabstop=2
set shiftwidth=2
set expandtab
"Perform automatic indentation when starting the new line.
set autoindent
"Copy whatever characters were used for indenting on the existing line to the
"new line.
set copyindent

" ============================================================
" Search & Replace
" ============================================================
set wrapscan
" ...dynamically as they are typed.
set incsearch
" Ignore case when searching
set ignorecase
" When searching try to be smart about cases
set smartcase
" Highlight search results
set hlsearch
" For regular expressions turn magic on
set magic
set grepprg=grep\ -nH\ --color

" ============================================================
" Swap History Backup ...
" ============================================================
set backup
execute "set backupdir=".g:trash."/backup"
execute "set directory=".g:trash."/swap"
set history=10000         " remember more commands and search history
set undolevels=10000      " use many muchos levels of undo
" Saves all open buffers in the background, instead of closing them and re-opening on demand
set hidden
" Demands confirmation before closing unsaved buffers
set confirm
execute "set viminfo='500,<10000,s10000,n".PathToString(g:trash)."/.viminfo"

" ============================================================
" Command Line
" ============================================================
"Enhance completion in Command-Line Mode.
set wildmenu
set wildmode=list:longest,full
set showcmd
set noshowmode

" Time out on mapping after
set ttimeoutlen=900
" Time out on key codes
set timeoutlen=700
if !has('gui_running')
"    augroup FastEscape
"        autocmd!
"        au InsertEnter * set timeoutlen=0
"        au InsertLeave * set timeoutlen=0
"    augroup END
endif

set backspace=2 " make backspace work like most other apps
set backspace=indent,eol,start

let g:statusline_max_path = 50
fun! StatusLineGetPath() "{{{
  if &buftype == 'nofile'
    return ''
  endif
  let p = expand('%:.:h') "relative to current path, and head path only
  let p = substitute(p,'\','/','g')
  let p = substitute(p, '^\V' . $HOME, '~', '')
  let width = winwidth(0)
  if len(p) > width
    let p = simplify(p)
    let p = pathshorten(p)
  endif
  return p.'/'
endfunction "}}}

set tags=./tags

" ============================================================
" Status Line
" ============================================================
function! IsSearchReplaceMode()
	if exists('g:searchReplaceModeState')
		if g:searchReplaceModeState | return "[SR]" | endif
	endif
	return ""
endfunction
function! GetWindowNumber()
  let l:window_number = winnr()
  return l:window_number
endfunction
" always show statusbar
set laststatus=2
set statusline=
set statusline+=%-2.2{GetWindowNumber()}
set statusline+=%h%#StatuslineFlag#%m%r%w " flags
set statusline+=%#StatuslinePath#\%-0.50{StatusLineGetPath()}%0* " path
set statusline+=%#StatuslineFileName#%t\ " file name
set statusline+=%-10.50{gutentags#statusline()}
set statusline+=%=
set statusline+=%-14.(%l,%c-%V%)             "position
set statusline+=%P                             "position percentage

" let g:default_compeltion = "Ycm"
" function! MyToggleCompeltion()
"   if (g:default_compeltion =~ "Neo")
"     echo "Ycm is on"
"     let g:default_compeltion = "Ycm"
"     exec "NeoComplCacheDisable"
"     call youcompleteme#Enable()
"     " set completefunc='youcompleteme#Complete'
"   elseif (g:default_compeltion =~ "Ycm")
"     echo "Neo is on"
"     let g:default_compeltion = "Neo"
"     exec "NeoComplCacheEnable"
"     " set completefunc='neocomplcache#complete#manual_complete'
"     " set omnifunc='neocomplcache#complete#manual_complete'
"   endif
" endfunction
" map <Space>[c :<C-U>call MyToggleCompeltion()<Cr>
