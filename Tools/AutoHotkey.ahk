; classname = ""
; keystate = ""
; StartVim() {
; 	vimTitle		= \[No Name\] - GVIM
; 	vimTitleFull = %vimTitle%\d
; 	SetTitleMatchMode,RegEx
; 	WinGetTitle, title, A
; 	position := RegExMatch(title, vimTitleFull)
; 	Run D:\vim\gvim.exe, E:\Documents, Max
; 	if (position = 0) {
; 		WinWaitActive %vimTitleFull%,,1
; 	} else{
; 		StringTrimLeft, excludeTitle, title, 16
; 		excludeTitle=%vimTitle%%excludeTitle%
; 		WinWaitActive %vimTitleFull%,,1,
; 	}
; 	if ErrorLevel {
; 		MsgBox, WinWait timed out.
; 		return
; 	} else
; 		WinMaximize
; 	return
; }

$!k::
WinGetClass, classname, A
if (classname = "Vim") {
  SendInput !{k}
} else {
  SendInput {Up}
}
return
$!j::
WinGetClass, classname, A
if (classname = "Vim") {
  SendInput !{j}
} else {
  SendInput {Down}
}
return
$!h::
WinGetClass, classname, A
if (classname = "Vim") {
  SendInput !{h}
} else {
  SendInput {Left}
}
return
$!l::
WinGetClass, classname, A
if (classname = "Vim") {
  SendInput !{l}
} else {
  SendInput {Right}
}
return

Capslock::Escape
!f::
  SendInput {Enter}
Return
!Capslock::SendInput {Enter}
SC056 Down::SendInput {Shift Down}
SC056 Up::SendInput {Shift Up}
$`;::
GetKeyState, shiftState, Shift
GetKeyState, specialKeyState, SC056
GetKeyState, altState,   Alt
if (shiftState = D || specialKeyState = D) {
  SendInput {:}
} else if (altState = D) {
  SendInput {`;}
} else {
  SendInput {Backspace}
}
return

; if (classname = "Vim"
;   || classname = "PX_WINDOW_CLASS" ;   || classname = "Emacs"
;   || classname = "QWidget"
;   || classname = "Console_2_Main")
; {
; 	SetCapsLockState, Off
; 		Send, {ESC}
; }
;else
;{
;	GetKeyState, keystate, CapsLock, T
;		if (keystate = "D")
;			SetCapsLockState, Off
;		else
;			SetCapsLockState, On
;				return
;}
;return

;$F1::
;WinGetClass, classname, A
;if (classname = "Vim")
;{
;	Send, {F1}
;	return
;}
;else
;{
;	StartVim()
;}
;return

;$#F1::
;StartVim()
;return


;F12::
;WinGetClass, classname, A
;MsgBox, "%classname%"
;return

; Note: From now on whenever you run AutoHotkey directly, this script
; will be loaded.  So feel free to customize it to suit your needs.

; Please read the QUICK-START TUTORIAL near the top of the help file.
; It explains how to perform common automation tasks such as sending
; keystrokes and mouse clicks.  It also explains more about hotkeys.

